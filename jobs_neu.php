<?php
$pageTitle = "Jobs";
$metaKeywords = "JObs";

include("pages_neu/fakeheader.inc.php");
include("_templates/jobs-list.inc.php");

$activeJobs = [];

foreach ($jobs as $job) {
   if (!$job["deaktiviert"]) {
      $activeJobs[] = $job;
   }
} ?>

    <section class="uk-container uk-container-center uk-margin-large-top uk-padding-vertical-remove">
        <h1 class="uk-heading-large uk-text-center"><?php echo $pageTitle ?></h1>
        </div>
    </section>


    <section
            class="uk-container uk-container-center white-bg uk-margin-large-top uk-padding-vertical-remove has-shadow">
        <div class="uk-panel uk-panel-space">
            <p class="uk-text-large">Software ist unser Handwerk: Wir unterstützen verarbeitende Betriebe des
                Baugewerbes und deren Lieferanten bei der Digitalisierung. Wir wollen mit allen Beteiligten mehr
                erreichen: Produktivität, Umsatz und Freude bei der Arbeit. Die Nachfrage ist sensationell - deswegen
                brauchen wir deine Unterstützung.</p>
        </div>
    </section>
    <!--Auswahlzeile-->

    <section id="products" class="uk-container uk-container-center white-bg uk-margin-large-top has-shadow">
        <div class="uk-panel uk-text-center uk-text-left-small">
            <h2>Alle Jobangebote im Überblick.</h2>
            <h4>Nichts dabei? Bewirb Dich gerne initiativ!</h4>
        </div>
        <div class="uk-panel uk-margin-large-top uk-margin-left uk-margin-right">
            <ul data-uk-tab data-uk-switcher="{connect:'#tabbed-content', animation: 'uk-animation-fade'}"
                class="uk-tab uk-tab-grid uk-margin-top">
               <?php foreach ($activeJobs as $job) {
                  if (!$job["deaktiviert"]) { ?>
                      <li class="uk-width-1-<?php echo count($activeJobs) ?>"><a
                                  href="#"><?php echo $job["kurztitel"]; ?></a></li>
                  <?php }
               } ?>
            </ul>
            <ul id="tabbed-content" class="uk-switcher uk-width-medium-4-5 uk-push-1-10 uk-margin-large-top">
               <?php foreach ($activeJobs as $job) {
                  if (!$job["deaktiviert"]) { ?>
                      <li>
                          <div class="uk-panel ">

                              <div class="uk-container uk-container-center uk-margin-arge-top uk-padding-remove">
                                  <div class="uk-panel uk-text-center"><img src="<?php echo $job['image']; ?>" alt="<?php echo $job['slogan']; ?>">
                                      <h1 class="uk-heading-large uk-margin-large-top"><?php echo $job['titel']; ?></h1>
                                  </div>
                              </div>

                              <p class="uk-text-large"><strong>Software ist unser Handwerk:</strong>
                              </p>
                             <?php
                             if(!empty($job["einleitung"])) {?>
                                 <p class="uk-text-large">
                                    <?php echo $job["einleitung"];?>
                                 </p>
                             <?php }
                             ?>
                              <h3 class="text-ida-red"># Was wir tun</h3>
                              <ul>
                                 <?php
                                 foreach ($job["wirsind"] as $text) {
                                    echo "<li class=\"uk-text-large\">" . $text . "</li>";
                                 }
                                 ?>
                              </ul>
                              <h3 class="text-ida-red"># Wen wir suchen</h3>
                              <ul>
                                 <?php
                                 foreach ($job["suchen"] as $text) {
                                    echo "<li class=\"uk-text-large\">" . $text . "</li>";
                                 }
                                 ?>
                              </ul>
                              <h3 class="text-ida-red"># Was wir bieten</h3>
                              <ul>
                                 <?php
                                 foreach ($job["bieten"] as $text) {
                                    echo "<li class=\"uk-text-large\">" . $text . "</li>";
                                 }
                                 ?>
                              </ul>
                              <br />
                              <p class="uk-text-large"><strong>Wir haben dein Interesse geweckt?</strong> Dann werde Teil unseres Teams! Melde
                                  dich einfach und unkompliziert per Mail - gerne mit einem kurzen Lebenslauf - und wir vereinbaren kurzfristig
                                  einen Termin zum Kennenlernen.
                              </p>
                              <br />
                          </div>
                      </li>
                  <?php }
               } ?>

            </ul>
        </div>
    </section>




<?php include("_templates/kontakt.inc.php"); ?>
<?php include("_templates/footer.inc.php");
