<?php
$pageTitle = "IDA - Die Software für das Handwerk";
$metaKeywords = "Geschäftsprozesse, digitale Transformation";
include("../_templates/header.inc.php");
?>

<section class="uk-container uk-container-center uk-margin-large-top uk-padding-vertical-remove">
    <div class="uk-panel uk-space uk-text-center">
        <h1 class="uk-heading-large"><?php echo $pageTitle; ?></h1>
    </div>
</section>

<!-- Projekte Box -->
<section class="uk-container uk-container-center uk-margin-large-top uk-padding-remove ">
    <div class="uk-grid uk-grid-collapse sec has-shadow white-bg ">
        <div class="uk-width-large-1-2 uk-position- uk-panel uk-panel-box">
            <div style="padding-bottom: 0px!important;"
                 class="uk-panel uk-panel-box  uk-flex uk-flex-middle uk-flex-center uk-padding-remove">
                <div class="uk-panel-space">
                    <h1>Ergänzen</h1>
                    <p> Die <strong>I</strong>ntegrierte <strong>D</strong>atenbank <strong>A</strong>nwendung für das
                            Handwerk (IDA) ist eine ganzheitliche, modular aufgebaute Software zur Steuerung
                        und Verarbeitung der Unternehmensprozesse in Handwerksbetrieben. Die Module sind beliebig
                        miteinander kombinierbar und jeweils auch einzeln lauffähig.</p>
                    </p>
                </div>
            </div>

        </div>
        <div class="uk-width-large-1-2">
            <div class="uk-panel uk-panel-box uk-panel-box-primary uk-flex uk-flex-middle uk-flex-center uk-padding-remove">
                <div class="uk-position-relative uk-visible-large">
                    <img src="/produkte/Grafiken/blades.jpeg" alt="IDALABS Porojekte">
                </div>

            </div>
        </div>
    </div>
</section>

<!-- IDA Ökosystem-->
<section id="Haus" class="uk-container uk-container-center uk-margin-large-top uk-padding-remove ">
    <div class="uk-grid uk-grid-collapse sec has-shadow white-bg ">

        <div class="uk-width-large-3-4 uk-panel uk-panel-box">
            <div class="uk-panel-space">
                <h2 class="text-ida-red">IDA Ökosystem </h2>
                <p>IDA soll es dem Unternehmer ermöglichen, eine digitale Transformation seiner Tätigkeiten</p>
                <ul class="uk-list-space">
                    <li>von bürokratischen Routineaufgaben zu Steuerungsaufgaben</li>
                    <li>von Verwaltungsaufgaben zu fakturierbarer Arbeit</li>
                    <li>von manueller Bearbeitung zur Automatisierung</li>
                    <li>und von Bauchentscheidungen zu Business Intelligence</li>
                </ul>
                <p>entwickeln zu können, und dadurch Transparenz, Effizienz und Entscheidungskompetenz zu gewinnen
                    und Zeit und Kosten zu sparen, was sich letztendlich in einem Wettbewerbsvorteil gegenüber
                    Mitbewerbern auswirken wird.</p>
            </div>
        </div>
        <div class="uk-width-large-1-4 uk-visible-large uk-panel uk-panel-box ">
            <div class="uk-panel-space">
                 <img src="//www.idalabs.de/assets/img/ida.png" width="170">
            </div>
        </div>
        <div class="uk-width-large-4-4 uk-panel uk-panel-box">
            <div class="uk-panel-space uk-padding-top-remove">

                <div class="uk-panel uk-panel-box uk-padding-top-remove uk-align-center">
                    <img src="/produkte/Grafiken/uebersicht_ergaenzen.png" alt="IDALABS Ida Module" id="img" onclick="swipe(id)">
                </div>

            </div>
        </div>


    </div>
</section>

<!-- IDA Module  -->
<section id="Lean" class="uk-container uk-container-center uk-margin-large-top uk-padding-remove ">
    <div class="uk-grid uk-grid-collapse sec has-shadow white-bg ">
        <div class="uk-width-large-3-4 uk-panel uk-panel-box">
            <div class="uk-panel-space">
                <h2 class="text-ida-red">IDA MODULE </h2>
                <p> Die einzelnen Module mit einer Übersicht über alle Funktionen und entsprechenden Preisen sind hier zu finden.  </p>

                <a href="/produkte/preis.php" class="uk-button uk-button-danger uk-margin-top  ">Module & Preise </a>
            </div>

        </div>
        <div class="uk-width-large-1-4 uk-visible-large uk-panel uk-panel-box">
            <div class="uk-panel-space">
                <img src="/assets/img/ida.png" alt="IDALABS Logo">

            </div>

        </div>
    </div>
</section>




<?php include("../_templates/kontakt.inc.php"); ?>
<?php include("../_templates/footer.inc.php");
