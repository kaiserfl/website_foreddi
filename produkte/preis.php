<?php include("../_templates/header.inc.php"); ?>





<?php

$servername = "10.0.0.6";
$username = "ida_website";
$password = "*************";
$db = "ida_mandanten";




try {
   $conn = new mysqli($servername, $username, $password, $db) or die(mysqli_connect_error());
} catch (Exception $e) {
   echo $e->getMessage();
}

mysqli_set_charset($conn, 'utf8mb4');

?>


<!-- Überschrift -->
<section class="uk-container uk-container-center uk-margin-large-top uk-padding-vertical-remove">
    <div class="uk-panel uk-space uk-text-center">
        <h1 class="uk-heading-large">Modul Preise</h1>
    </div>
</section>

<!-- Erklär Box -->
<section class="uk-container uk-container-center uk-margin-large-top uk-padding-remove ">
    <div class="uk-grid uk-grid-collapse sec has-shadow white-bg ">
        <div class="uk-width-large-1-2 uk-position- uk-panel uk-panel-box">
            <div style="padding-bottom: 0px!important;"
                 class="uk-panel uk-panel-box  uk-flex uk-flex-middle uk-flex-center uk-padding-remove">
                <div class="uk-panel-space">
                    <h1>Faires Preismodell</h1>
                    <p> Die Module stellen das gesamte Funktionsspektrum der IDA dar. Sie können in verschiedenen
                        Ausprägungen, Kombinationen aber auch einzeln gebucht werden.
                        IDA setzt sich aus 14 optimal ineinander greifenden Modulen zusammen, von denen 10 die
                        Kernfunktionalität für das Geschäftsprozessessmanagement beinhalten und 4 Module
                        Groupwarefunktionalität (Mail, Kalender, Aufgaben und Dokumente) als Querschnittsfähigkeit
                        bereitstellen.
                        Die Software wächst mit dem Handwerksbetrieb und wird je nach Digitalisierungsgrad angepasst. Du
                        zahlst nur was du wirklich brauchst.
                    </p>
                </div>
            </div>

        </div>
        <div class="uk-width-large-1-2">
            <div class="uk-panel uk-panel-box uk-panel-box-primary uk-flex uk-flex-middle uk-flex-center uk-padding-remove">
                <div class="uk-position-relative">
                    <img src="/produkte/Grafiken/werkzeug.jpg" alt="IDALABS Projekte">
                </div>

            </div>
        </div>
    </div>
</section>

<!-- Mandant  -->
<section class="uk-container uk-container-center uk-margin-large-top uk-padding-remove  ">
    <div class="uk-panel uk-panel-box uk-panel-space has-shadow white-bg uk-overflow-container">

        <table class="uk-table ">
            <tbody class="uk-accordion" data-uk-accordion="{showfirst : false}">

            <tr>
                <th style="padding: 12px">


                    <div class="uk-grid uk-flex uk-flex-left uk-flex-middle">
                        <img class="uk-grid-width-1-4" src="/produkte/Grafiken/idalabslogo.png " width="90px"
                             alt="IDALABS MaWi">
                        <h2 style="margin-left: unset"
                            class=" uk-grid-width-1-4 uk-align-center  uk-margin-small uk-heading-medium uk-text-bold ">
                            Mandant</h2>
                        <a class="uk-button uk-button-primary uk-margin-remove uk-flex uk-accordion-title  uk-margin-large-top">Funktionen
                            & Preise</a>
                    </div>


                    <div class=" uk-accordion-content">

                        <!--Infobox -->
                       <?php $query = mysqli_query($conn, "SELECT * FROM module where modul_id = 23 ")
                       or die (mysqli_error($conn));

                       while ($row = mysqli_fetch_array($query)) {
                          $kurztext = $row['kurztext'];
                       }


                       echo "  
                            <div style='padding-left: 30px; padding-right: 30px' class='uk-panel uk-space'>
                                <div style='padding-left: 0px;  padding-right: 0px' class='uk-panel uk-panel-box uk-flex uk-flex-middle uk-flex-center uk-panel-space '>
                                    <div style='padding-left: 0px;  padding-right: 0px' class='uk-panel uk-panel-space  uk-text-normal '>
                                        <p style='font-weight: normal'>  $kurztext</p>
                                    </div>
                                </div>
                            </div>";

                       ?>


                        <!--Steuerung von Nutzern und Zugriffen  -->
                        <div class=" uk-panel-space">
                            <table class="uk-table uk-table-striped  ">
                                <thead>
                                <tr>
                                    <th class=" uk-width-large-1-5 uk-padding-remove "><h3 class="text-ida-red">
                                            Steuerung von Nutzern und Zugriffen</h3></th>

                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary   ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php
                                $query = mysqli_query($conn, "SELECT * FROM features where modul_id = 23 and featuregruppe = 'Steuerung von Nutzern und Zugriffen'")
                                or die (mysqli_error($conn));


                                while ($row = mysqli_fetch_array($query)) {
                                   if ($row['ab_plan'] == 3) {

                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-remove'></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 2) {
                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 1) {
                                      $state1 = "<div class='uk-icon-check-square '></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }


                                   echo


                                   "<tr>
                                        <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'> <h5 class='uk-margin-remove'> {$row['feature']} </h5> </td>
                                      
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state1</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state2</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state3</div></td>
                                        
                                      
                                       </tr>\n";

                                }

                                ?>
                                <tr>
                                    <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'><h5
                                                class='uk-margin-remove'> Anzahl Nutzer </h5></td>

                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>1</div>
                                    </td>
                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>25</div>
                                    </td>
                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>unbegrenzt</div>
                                    </td>


                                </tr>
                                </tbody>


                                <!--vorletzte und letzte Zeile  -->
                                <tfoot>


                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove"></h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                </tr>

                                </tfoot>


                                </tbody>
                            </table>
                        </div>

                        <!--Generelle Software Features  -->
                        <div class=" uk-panel-space">
                            <table class="uk-table uk-table-striped  ">
                                <thead>
                                <tr>
                                    <th class=" uk-width-large-1-5 uk-padding-remove "><h3 class="text-ida-red">
                                            Generelle Software Features</h3></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary   ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php
                                $query = mysqli_query($conn, "SELECT * FROM features where modul_id = 23 and featuregruppe = 'Generelle Software Features'")
                                or die (mysqli_error($conn));


                                while ($row = mysqli_fetch_array($query)) {
                                   if ($row['ab_plan'] == 3) {

                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-remove'></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 2) {
                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 1) {
                                      $state1 = "<div class='uk-icon-check-square '></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }


                                   echo


                                   "<tr>
                                        <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'> <h5 class='uk-margin-remove'> {$row['feature']} </h5> </td>
                                      
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state1</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state2</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state3</div></td>
                                        
                                      
                                       </tr>\n";

                                }

                                ?>


                                <!--Letzte Zeile  -->
                                <tfoot>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove"></h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                </tr>

                                </tfoot>


                                </tbody>
                            </table>
                        </div>

                        <!--Infrastruktur -->
                        <div class=" uk-panel-space">
                            <table class="uk-table uk-table-striped  ">
                                <thead>
                                <tr>
                                    <th class=" uk-width-large-1-5 uk-padding-remove "><h3 class="text-ida-red">
                                            Infrastruktur</h3></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary   ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php
                                $query = mysqli_query($conn, "SELECT * FROM features where modul_id = 23 and featuregruppe = 'Infrastruktur'")
                                or die (mysqli_error($conn));


                                while ($row = mysqli_fetch_array($query)) {
                                   if ($row['ab_plan'] == 3) {

                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-remove'></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 2) {
                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 1) {
                                      $state1 = "<div class='uk-icon-check-square '></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }


                                   echo


                                   "<tr>
                                        <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'> <h5 class='uk-margin-remove'> {$row['feature']} </h5> </td>
                                      
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state1</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state2</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state3</div></td>
                                        
                                      
                                       </tr>\n";

                                }

                                ?>

                                <tr>
                                    <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'><h5
                                                class='uk-margin-remove'> Backupaufbewahrung </h5></td>

                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>3 Tage</div>
                                    </td>
                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>10 Tage</div>
                                    </td>
                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '> 30 tage</div>
                                    </td>
                                </tr>

                                <tr>
                                    <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'><h5
                                                class='uk-margin-remove'> Speicherplatz </h5></td>

                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>10 GB</div>
                                    </td>
                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>50 GB</div>
                                    </td>
                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '> unbegrenzt</div>
                                    </td>
                                </tr>

                                </tbody>


                                <!--Letzte Zeile  -->
                                <tfoot>




                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove"></h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                </tr>

                                </tfoot>


                                </tbody>
                            </table>
                        </div>


                        <!-- Preise -->
                        <div class=" uk-panel-space">
                            <h3 class="uk-padding-small">Preise</h3>
                            <table class="uk-table ">
                                <thead>
                                <tr>
                                    <th class="uk-width-large-2-5 "></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary  ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>

                                <tbody>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove">Preise für den Mandanten
                                            pro Monat</h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom"> 145,00 €</div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom"> 195,00 €</div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom"> 245,00 €</div>
                                    </td>
                                </tr>

                                </tbody>


                                </thead>
                            </table>
                        </div>


                </th>
            </tr>


            </tbody>
        </table>
    </div>
</section>

<!-- Mawi  -->
<section class="uk-container uk-container-center uk-margin-large-top uk-padding-remove  ">
    <div class="uk-panel uk-panel-box uk-panel-space has-shadow white-bg uk-overflow-container">

        <table class="uk-table ">
            <tbody class="uk-accordion" data-uk-accordion="{showfirst : false}">


            <tr>
                <th style="padding: 12px">


                    <div class="uk-grid uk-flex uk-flex-left uk-flex-middle">
                        <img class="uk-grid-width-1-4" src="/produkte/Grafiken/ERP.png " width="90px"
                             alt="IDALABS MaWi">
                        <h2 style="margin-left: unset"
                            class=" uk-grid-width-1-4 uk-align-center  uk-margin-small uk-heading-medium uk-text-bold ">
                            Warenwirtschaft - MaWi</h2>
                        <a class="uk-button uk-button-primary uk-margin-remove uk-flex uk-accordion-title  uk-margin-large-top">Funktionen
                            & Preise</a>
                    </div>

                    <div class=" uk-accordion-content">

                        <!--Infobox -->
                       <?php $query = mysqli_query($conn, "SELECT * FROM module where modul_id = 1 ")
                       or die (mysqli_error($conn));

                       while ($row = mysqli_fetch_array($query)) {
                          $kurztext = $row['kurztext'];
                       }


                       echo "  
                            <div style='padding-left: 30px; padding-right: 30px' class='uk-panel uk-space'>
                                <div style='padding-left: 0px;  padding-right: 0px' class='uk-panel uk-panel-box uk-flex uk-flex-middle uk-flex-center uk-panel-space '>
                                    <div style='padding-left: 0px;  padding-right: 0px' class='uk-panel uk-panel-space  uk-text-normal '>
                                        <p style='font-weight: normal'>  $kurztext</p>
                                    </div>
                                </div>
                            </div>";

                       ?>




                        <!--LVS-->
                        <div id="LVS" class=" uk-panel-space">
                            <table class="uk-table uk-table-striped  ">
                                <thead>
                                <tr>
                                    <th class=" uk-width-large-1-5 uk-padding-remove "><h3 class="text-ida-red">
                                            Lagerverwaltungssystem LVS</h3></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary   ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php
                                $query = mysqli_query($conn, "SELECT * FROM features where modul_id = 1 and featuregruppe = 'Lagerverwaltungssystem LVS'")
                                or die (mysqli_error($conn));


                                while ($row = mysqli_fetch_array($query)) {
                                   if ($row['ab_plan'] == 3) {

                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-remove'></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 2) {
                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 1) {
                                      $state1 = "<div class='uk-icon-check-square '></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }


                                   echo


                                   "<tr>
                                        <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'> <h5 class='uk-margin-remove'> {$row['feature']} </h5> </td>
                                      
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state1</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state2</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state3</div></td>
                                        
                                      
                                       </tr>\n";

                                }

                                ?>


                                <!--Letzte Zeile  -->
                                <tfoot>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove"></h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                </tr>

                                </tfoot>


                                </tbody>
                            </table>
                        </div>

                        <!--Bestellprozess-->
                        <div class=" uk-panel-space">
                            <table class="uk-table uk-table-striped  ">
                                <thead>
                                <tr>
                                    <th class=" uk-width-large-1-5 uk-padding-remove "><h3 class="text-ida-red">
                                            Bestellprozess</h3></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary   ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php
                                $query = mysqli_query($conn, "SELECT * FROM features where modul_id = 1 and featuregruppe = 'Bestellprozess'")
                                or die (mysqli_error($conn));


                                while ($row = mysqli_fetch_array($query)) {
                                   if ($row['ab_plan'] == 3) {

                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-remove'></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 2) {
                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 1) {
                                      $state1 = "<div class='uk-icon-check-square '></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }


                                   echo


                                   "<tr>
                                        <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'> <h5 class='uk-margin-remove'> {$row['feature']} </h5> </td>
                                      
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state1</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state2</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state3</div></td>
                                        
                                      
                                       </tr>\n";

                                }

                                ?>


                                <!--Letzte Zeile  -->
                                <tfoot>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove"></h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                </tr>

                                </tfoot>


                                </tbody>
                            </table>
                        </div>

                        <!--Artikelstammdaten-->
                        <div class=" uk-panel-space">
                            <table class="uk-table uk-table-striped  ">
                                <thead>
                                <tr>
                                    <th class=" uk-width-large-1-5 uk-padding-remove "><h3 class="text-ida-red">
                                            Artikelstammdaten</h3></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary   ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php
                                $query = mysqli_query($conn, "SELECT * FROM features where modul_id = 1 and featuregruppe = 'Artikelstammdaten'")
                                or die (mysqli_error($conn));


                                while ($row = mysqli_fetch_array($query)) {
                                   if ($row['ab_plan'] == 3) {

                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-remove'></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 2) {
                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 1) {
                                      $state1 = "<div class='uk-icon-check-square '></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }


                                   echo


                                   "<tr>
                                        <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'> <h5 class='uk-margin-remove'> {$row['feature']} </h5> </td>
                                      
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state1</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state2</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state3</div></td>
                                        
                                      
                                       </tr>\n";

                                }

                                ?>


                                <!--Letzte Zeile  -->
                                <tfoot>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove"></h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                </tr>

                                </tfoot>


                                </tbody>
                            </table>
                        </div>

                        <!--Inventur-->
                        <div class=" uk-panel-space">
                            <table class="uk-table uk-table-striped  ">
                                <thead>
                                <tr>
                                    <th class=" uk-width-large-1-5 uk-padding-remove "><h3 class="text-ida-red">
                                            Inventur</h3></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary   ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php
                                $query = mysqli_query($conn, "SELECT * FROM features where modul_id = 1 and featuregruppe = 'Inventur'")
                                or die (mysqli_error($conn));


                                while ($row = mysqli_fetch_array($query)) {
                                   if ($row['ab_plan'] == 3) {

                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-remove'></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 2) {
                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 1) {
                                      $state1 = "<div class='uk-icon-check-square '></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }


                                   echo


                                   "<tr>
                                        <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'> <h5 class='uk-margin-remove'> {$row['feature']} </h5> </td>
                                      
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state1</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state2</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state3</div></td>
                                        
                                      
                                       </tr>\n";

                                }

                                ?>


                                <!--Letzte Zeile  -->
                                <tfoot>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove"></h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                </tr>

                                </tfoot>


                                </tbody>
                            </table>
                        </div>

                        <!--Lieferantenverwaltung-->
                        <div class=" uk-panel-space">
                            <table class="uk-table uk-table-striped  ">
                                <thead>
                                <tr>
                                    <th class=" uk-width-large-1-5 uk-padding-remove "><h3 class="text-ida-red">
                                            Lieferantenverwaltung</h3></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary   ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php
                                $query = mysqli_query($conn, "SELECT * FROM features where modul_id = 1 and featuregruppe = 'Lieferantenverwaltung'")
                                ;


                                while ($row = mysqli_fetch_array($query)) {
                                   if ($row['ab_plan'] == 3) {

                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-remove'></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 2) {
                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 1) {
                                      $state1 = "<div class='uk-icon-check-square '></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }


                                   echo


                                   "<tr>
                                        <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'> <h5 class='uk-margin-remove'> {$row['feature']} </h5> </td>
                                      
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state1</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state2</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state3</div></td>
                                        
                                      
                                       </tr>\n";

                                }

                                ?>


                                <!--Letzte Zeile  -->
                                <tfoot>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove"></h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                </tr>

                                </tfoot>


                                </tbody>
                            </table>
                        </div>

                        <!--Integration-->
                        <div class=" uk-panel-space">
                            <table class="uk-table uk-table-striped  ">
                                <thead>
                                <tr>
                                    <th class=" uk-width-large-1-5 uk-padding-remove "><h3 class="text-ida-red">
                                            Integrationen</h3></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary   ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php
                                $query = mysqli_query($conn, "SELECT * FROM features where modul_id = 1 and featuregruppe = 'Integrationen'")
                                or die (mysqli_error($conn));


                                while ($row = mysqli_fetch_array($query)) {
                                   if ($row['ab_plan'] == 3) {

                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-remove'></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 2) {
                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 1) {
                                      $state1 = "<div class='uk-icon-check-square '></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }


                                   echo


                                   "<tr>
                                        <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'> <h5 class='uk-margin-remove'> {$row['feature']} </h5> </td>
                                      
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state1</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state2</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state3</div></td>
                                        
                                      
                                       </tr>\n";

                                }

                                ?>

                                <tr>
                                    <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'><h5
                                                class='uk-margin-remove'> Digitale Lieferantenanbindungen</h5></td>

                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>1</div>
                                    </td>
                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>2</div>
                                    </td>
                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '> unbegrenzt</div>
                                    </td>
                                </tr>

                                <tr>
                                    <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'><h5
                                                class='uk-margin-remove'> Lieferanten für Stammdatenupdates </h5></td>

                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>3</div>
                                    </td>
                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>7</div>
                                    </td>
                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '> unbegrenzt</div>
                                    </td>
                                </tr>


                                <!--Letzte Zeile  -->
                                <tfoot>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove"></h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                </tr>

                                </tfoot>


                                </tbody>
                            </table>
                        </div>


                        <!-- Preise -->
                        <div class=" uk-panel-space">
                            <h3 class="uk-padding-small">Preise</h3>
                            <table class="uk-table ">
                                <thead>
                                <tr>
                                    <th class="uk-width-large-2-5 "></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary  ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>

                                <tbody>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove">Preise für das
                                            gesamte
                                            Mawi Modul pro Monat</h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom"> 95,00 €</div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom"> 145,00 €</div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom"> 195,00 €</div>
                                    </td>
                                </tr>

                                </tbody>


                                </thead>
                            </table>
                        </div>


                </th>
            </tr>


            </tbody>
        </table>
    </div>
</section>

<!-- Ava  -->
<section class="uk-container uk-container-center uk-margin-large-top uk-padding-remove  ">
    <div class="uk-panel uk-panel-box uk-panel-space has-shadow white-bg uk-overflow-container">

        <table class="uk-table ">
            <tbody class="uk-accordion" data-uk-accordion="{showfirst : false}">


            <tr>
                <th style="padding: 12px">


                    <div class="uk-grid uk-flex uk-flex-left uk-flex-middle">
                        <img class="uk-grid-width-1-4" src="/produkte/Grafiken/ava.png" width="90px"
                             alt="IDALABS MaWi">
                        <h2 style="margin-left: unset"
                            class=" uk-grid-width-1-4 uk-align-center  uk-margin-small uk-heading-medium uk-text-bold ">
                            Leistungs- und Vergabemanagement - AVA</h2>
                        <a class="uk-button uk-button-primary uk-margin-remove uk-flex uk-accordion-title  uk-margin-large-top">Funktionen
                            & Preise</a>
                    </div>

                    <div class=" uk-accordion-content">

                        <!--Infobox -->
                       <?php $query = mysqli_query($conn, "SELECT * FROM module where modul_id = 18 ")
                       ;

                       while ($row = mysqli_fetch_array($query)) {
                          $kurztext = $row['kurztext'];
                       }


                       echo "  
                            <div style='padding-left: 30px; padding-right: 30px' class='uk-panel uk-space'>
                                <div style='padding-left: 0px;  padding-right: 0px' class='uk-panel uk-panel-box uk-flex uk-flex-middle uk-flex-center uk-panel-space '>
                                    <div style='padding-left: 0px;  padding-right: 0px' class='uk-panel uk-panel-space  uk-text-normal '>
                                        <p style='font-weight: normal'>  $kurztext</p>
                                    </div>
                                </div>
                            </div>";

                       ?>


                        <!--Kalkulation-->
                        <div class=" uk-panel-space">
                            <table class="uk-table uk-table-striped  ">
                                <thead>
                                <tr>
                                    <th class=" uk-width-large-1-5 uk-padding-remove "><h3 class="text-ida-red">
                                            Kalkulation</h3></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary   ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php
                                $query = mysqli_query($conn, "SELECT * FROM features where modul_id = 18 and featuregruppe = 'Kalkulation'")
                                ;


                                while ($row = mysqli_fetch_array($query)) {
                                   if ($row['ab_plan'] == 3) {

                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-remove'></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 2) {
                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 1) {
                                      $state1 = "<div class='uk-icon-check-square '></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }


                                   echo


                                   "<tr>
                                        <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'> <h5 class='uk-margin-remove'> {$row['feature']} </h5> </td>
                                      
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state1</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state2</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state3</div></td>
                                        
                                      
                                       </tr>\n";

                                }

                                ?>


                                <!--Letzte Zeile  -->
                                <tfoot>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove"></h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                </tr>

                                </tfoot>


                                </tbody>
                            </table>
                        </div>

                        <!--Leistungsportfolio-->
                        <div class=" uk-panel-space">
                            <table class="uk-table uk-table-striped  ">
                                <thead>
                                <tr>
                                    <th class=" uk-width-large-1-5 uk-padding-remove "><h3 class="text-ida-red">
                                            Leistungsportfolio</h3></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary   ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php
                                $query = mysqli_query($conn, "SELECT * FROM features where modul_id = 18 and featuregruppe = 'Leistungsportfolio'")
                                ;


                                while ($row = mysqli_fetch_array($query)) {
                                   if ($row['ab_plan'] == 3) {

                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-remove'></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 2) {
                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 1) {
                                      $state1 = "<div class='uk-icon-check-square '></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }


                                   echo


                                   "<tr>
                                        <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'> <h5 class='uk-margin-remove'> {$row['feature']} </h5> </td>
                                      
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state1</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state2</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state3</div></td>
                                        
                                      
                                       </tr>\n";

                                }

                                ?>


                                <!--Letzte Zeile  -->
                                <tfoot>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove"></h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                </tr>

                                </tfoot>


                                </tbody>
                            </table>
                        </div>

                        <!--Fremdleistungen und Nachunternehmer-->
                        <div class=" uk-panel-space">
                            <table class="uk-table uk-table-striped  ">
                                <thead>
                                <tr>
                                    <th class=" uk-width-large-1-5 uk-padding-remove "><h3 class="text-ida-red">
                                            Fremdleistungen und Nachunternehmer</h3></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary   ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php
                                $query = mysqli_query($conn, "SELECT * FROM features where modul_id = 18 and featuregruppe = 'Fremdleistungen und Nachunternehmer'")
                                ;


                                while ($row = mysqli_fetch_array($query)) {
                                   if ($row['ab_plan'] == 3) {

                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-remove'></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 2) {
                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 1) {
                                      $state1 = "<div class='uk-icon-check-square '></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }


                                   echo


                                   "<tr>
                                        <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'> <h5 class='uk-margin-remove'> {$row['feature']} </h5> </td>
                                      
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state1</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state2</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state3</div></td>
                                        
                                      
                                       </tr>\n";

                                }

                                ?>


                                <!--Letzte Zeile  -->
                                <tfoot>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove"></h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                </tr>

                                </tfoot>


                                </tbody>
                            </table>
                        </div>

                        <!--Leistungsverzeichnis-->
                        <div class=" uk-panel-space">
                            <table class="uk-table uk-table-striped  ">
                                <thead>
                                <tr>
                                    <th class=" uk-width-large-1-5 uk-padding-remove "><h3 class="text-ida-red">
                                            Inventur</h3></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary   ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php
                                $query = mysqli_query($conn, "SELECT * FROM features where modul_id = 18 and featuregruppe = 'Leistungsverzeichnis'")
                                ;


                                while ($row = mysqli_fetch_array($query)) {
                                   if ($row['ab_plan'] == 3) {

                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-remove'></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 2) {
                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 1) {
                                      $state1 = "<div class='uk-icon-check-square '></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }


                                   echo


                                   "<tr>
                                        <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'> <h5 class='uk-margin-remove'> {$row['feature']} </h5> </td>
                                      
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state1</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state2</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state3</div></td>
                                        
                                      
                                       </tr>\n";

                                }

                                ?>


                                <!--Letzte Zeile  -->
                                <tfoot>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove"></h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                </tr>

                                </tfoot>


                                </tbody>
                            </table>
                        </div>

                        <!--Abrechnung-->
                        <div class=" uk-panel-space">
                            <table class="uk-table uk-table-striped  ">
                                <thead>
                                <tr>
                                    <th class=" uk-width-large-1-5 uk-padding-remove "><h3 class="text-ida-red">
                                            Abrechnung</h3></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary   ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php
                                $query = mysqli_query($conn, "SELECT * FROM features where modul_id = 18 and featuregruppe = 'Abrechnung'")
                                ;


                                while ($row = mysqli_fetch_array($query)) {
                                   if ($row['ab_plan'] == 3) {

                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-remove'></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 2) {
                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 1) {
                                      $state1 = "<div class='uk-icon-check-square '></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }


                                   echo


                                   "<tr>
                                        <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'> <h5 class='uk-margin-remove'> {$row['feature']} </h5> </td>
                                      
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state1</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state2</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state3</div></td>
                                        
                                      
                                       </tr>\n";

                                }

                                ?>


                                <!--Letzte Zeile  -->
                                <tfoot>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove"></h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                </tr>

                                </tfoot>


                                </tbody>
                            </table>
                        </div>


                        <!-- Preise -->
                        <div class=" uk-panel-space">
                            <h3 class="uk-padding-small">Preise</h3>
                            <table class="uk-table ">
                                <thead>
                                <tr>
                                    <th class="uk-width-large-2-5 "></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary  ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>

                                <tbody>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove">Preise für das
                                            gesamte
                                            AVA Modul pro Monat</h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom"> 95,00 €</div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom"> 145,00 €</div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom"> 195,00 €</div>
                                    </td>
                                </tr>

                                </tbody>


                                </thead>
                            </table>
                        </div>


                </th>
            </tr>


            </tbody>
        </table>
    </div>
</section>

<!-- MES  -->
<section class="uk-container uk-container-center uk-margin-large-top uk-padding-remove  ">
    <div class="uk-panel uk-panel-box uk-panel-space has-shadow white-bg uk-overflow-container">

        <table class="uk-table ">
            <tbody class="uk-accordion" data-uk-accordion="{showfirst : false}">


            <tr>
                <th style="padding: 12px">


                    <div class="uk-grid uk-flex uk-flex-left uk-flex-middle">
                        <img class="uk-grid-width-1-4" src="/produkte/Grafiken/MES.png" width="90px"
                             alt="IDALABS MaWi">
                        <h2 style="margin-left: unset"
                            class=" uk-grid-width-1-4 uk-align-center  uk-margin-small uk-heading-medium uk-text-bold ">
                            Projektsteuerung - PLM</h2>
                        <a class="uk-button uk-button-primary uk-margin-remove uk-flex uk-accordion-title  uk-margin-large-top">Funktionen
                            & Preise</a>
                    </div>

                    <div class=" uk-accordion-content">

                        <!--Infobox -->
                       <?php $query = mysqli_query($conn, "SELECT * FROM module where modul_id = 2 ")
                       ;

                       while ($row = mysqli_fetch_array($query)) {
                          $kurztext = $row['kurztext'];
                       }


                       echo "  
                            <div style='padding-left: 30px; padding-right: 30px' class='uk-panel uk-space'>
                                <div style='padding-left: 0px;  padding-right: 0px' class='uk-panel uk-panel-box uk-flex uk-flex-middle uk-flex-center uk-panel-space '>
                                    <div style='padding-left: 0px;  padding-right: 0px' class='uk-panel uk-panel-space  uk-text-normal '>
                                        <p style='font-weight: normal'>  $kurztext</p>
                                    </div>
                                </div>
                            </div>";

                       ?>


                        <!--Auftragskategorisierung und -dokumentation-->
                        <div class=" uk-panel-space">
                            <table class="uk-table uk-table-striped  ">
                                <thead>
                                <tr>
                                    <th class=" uk-width-large-1-5 uk-padding-remove "><h3 class="text-ida-red">
                                            Auftragskategorisierung und -dokumentation</h3></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary   ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php
                                $query = mysqli_query($conn, "SELECT * FROM features where modul_id = 2 and featuregruppe = 'Auftragskategorisierung und -dokumentation'")
                                ;


                                while ($row = mysqli_fetch_array($query)) {
                                   if ($row['ab_plan'] == 3) {

                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-remove'></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 2) {
                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 1) {
                                      $state1 = "<div class='uk-icon-check-square '></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }


                                   echo


                                   "<tr>
                                        <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'> <h5 class='uk-margin-remove'> {$row['feature']} </h5> </td>
                                      
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state1</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state2</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state3</div></td>
                                        
                                      
                                       </tr>\n";

                                }

                                ?>


                                <!--Letzte Zeile  -->
                                <tfoot>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove"></h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                </tr>

                                </tfoot>


                                </tbody>
                            </table>
                        </div>

                        <!--Projektmanagement und -planung-->
                        <div class=" uk-panel-space">
                            <table class="uk-table uk-table-striped  ">
                                <thead>
                                <tr>
                                    <th class=" uk-width-large-1-5 uk-padding-remove "><h3 class="text-ida-red">
                                            Projektmanagement und -planung</h3></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary   ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php
                                $query = mysqli_query($conn, "SELECT * FROM features where modul_id = 2 and featuregruppe = 'Projektmanagement und -planung'")
                                ;


                                while ($row = mysqli_fetch_array($query)) {
                                   if ($row['ab_plan'] == 3) {

                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-remove'></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 2) {
                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 1) {
                                      $state1 = "<div class='uk-icon-check-square '></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }


                                   echo


                                   "<tr>
                                        <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'> <h5 class='uk-margin-remove'> {$row['feature']} </h5> </td>
                                      
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state1</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state2</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state3</div></td>
                                        
                                      
                                       </tr>\n";

                                }

                                ?>


                                <!--Letzte Zeile  -->
                                <tfoot>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove"></h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                </tr>

                                </tfoot>


                                </tbody>
                            </table>
                        </div>

                        <!--Integrationen mit anderen Modulen-->
                        <div class=" uk-panel-space">
                            <table class="uk-table uk-table-striped  ">
                                <thead>
                                <tr>
                                    <th class=" uk-width-large-1-5 uk-padding-remove "><h3 class="text-ida-red">
                                            Integrationen mit anderen Modulen</h3></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary   ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php
                                $query = mysqli_query($conn, "SELECT * FROM features where modul_id = 2 and featuregruppe = 'Integrationen mit anderen Modulen'")
                                ;


                                while ($row = mysqli_fetch_array($query)) {
                                   if ($row['ab_plan'] == 3) {

                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-remove'></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 2) {
                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 1) {
                                      $state1 = "<div class='uk-icon-check-square '></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }


                                   echo


                                   "<tr>
                                        <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'> <h5 class='uk-margin-remove'> {$row['feature']} </h5> </td>
                                      
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state1</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state2</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state3</div></td>
                                        
                                      
                                       </tr>\n";

                                }

                                ?>


                                <!--Letzte Zeile  -->
                                <tfoot>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove"></h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                </tr>

                                </tfoot>


                                </tbody>
                            </table>
                        </div>


                        <!-- Preise -->
                        <div class=" uk-panel-space">
                            <h3 class="uk-padding-small">Preise</h3>
                            <table class="uk-table ">
                                <thead>
                                <tr>
                                    <th class="uk-width-large-2-5 "></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary  ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>

                                <tbody>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove">Preise für das
                                            PLM Modul pro Monat</h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom"> 95,00 €</div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom"> 145,00 €</div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom"> 195,00 €</div>
                                    </td>
                                </tr>

                                </tbody>


                                </thead>
                            </table>
                        </div>


                </th>
            </tr>


            </tbody>
        </table>
    </div>
</section>

<!-- EAM  -->
<section class="uk-container uk-container-center uk-margin-large-top uk-padding-remove  ">
    <div class="uk-panel uk-panel-box uk-panel-space has-shadow white-bg uk-overflow-container">

        <table class="uk-table ">
            <tbody class="uk-accordion" data-uk-accordion="{showfirst : false}">


            <tr>
                <th style="padding: 12px">


                    <div class="uk-grid uk-flex uk-flex-left uk-flex-middle">
                        <img class="uk-grid-width-1-4" src="/produkte/Grafiken/EAM.png" width="90px"
                             alt="IDALABS MaWi">
                        <h2 style="margin-left: unset"
                            class=" uk-grid-width-1-4 uk-align-center  uk-margin-small uk-heading-medium uk-text-bold ">
                            Anlagen und Inventar - EAM</h2>
                        <a class="uk-button uk-button-primary uk-margin-remove uk-flex uk-accordion-title  uk-margin-large-top">Funktionen
                            & Preise</a>
                    </div>

                    <div class=" uk-accordion-content">

                        <!--Infobox -->
                       <?php $query = mysqli_query($conn, "SELECT * FROM module where modul_id = 9 ")
                       ;

                       while ($row = mysqli_fetch_array($query)) {
                          $kurztext = $row['kurztext'];
                       }


                       echo "  
                            <div style='padding-left: 30px; padding-right: 30px' class='uk-panel uk-space'>
                                <div style='padding-left: 0px;  padding-right: 0px' class='uk-panel uk-panel-box uk-flex uk-flex-middle uk-flex-center uk-panel-space '>
                                    <div style='padding-left: 0px;  padding-right: 0px' class='uk-panel uk-panel-space  uk-text-normal '>
                                        <p style='font-weight: normal'>  $kurztext</p>
                                    </div>
                                </div>
                            </div>";

                       ?>


                        <!--Ohne Unterkategorie-->
                        <div class=" uk-panel-space">
                            <table class="uk-table uk-table-striped  ">
                                <thead>
                                <tr>
                                    <th class=" uk-width-large-1-5 uk-padding-remove "><h3 class="text-ida-red">
                                        </h3></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary   ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php
                                $query = mysqli_query($conn, "SELECT * FROM features where modul_id = 9 ")
                                ;


                                while ($row = mysqli_fetch_array($query)) {
                                   if ($row['ab_plan'] == 3) {

                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-remove'></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 2) {
                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 1) {
                                      $state1 = "<div class='uk-icon-check-square '></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }


                                   echo


                                   "<tr>
                                        <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'> <h5 class='uk-margin-remove'> {$row['feature']} </h5> </td>
                                      
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state1</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state2</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state3</div></td>
                                        
                                      
                                       </tr>\n";

                                }

                                ?>


                                <!--Letzte Zeile  -->
                                <tfoot>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove"></h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                </tr>

                                </tfoot>


                                </tbody>
                            </table>
                        </div>


                        <!-- Preise -->
                        <div class=" uk-panel-space">
                            <h3 class="uk-padding-small">Preise</h3>
                            <table class="uk-table ">
                                <thead>
                                <tr>
                                    <th class="uk-width-large-2-5 "></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary  ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>

                                <tbody>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove">Preise für das
                                            EAM Modul pro Monat</h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom"> 25,00 €</div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom"> 45,00 €</div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom"> 65,00 €</div>
                                    </td>
                                </tr>

                                </tbody>


                                </thead>
                            </table>
                        </div>


                </th>
            </tr>


            </tbody>
        </table>
    </div>
</section>

<!-- CMS  -->
<section class="uk-container uk-container-center uk-margin-large-top uk-padding-remove  ">
    <div class="uk-panel uk-panel-box uk-panel-space has-shadow white-bg uk-overflow-container">

        <table class="uk-table ">
            <tbody class="uk-accordion" data-uk-accordion="{showfirst : false}">


            <tr>
                <th style="padding: 12px">


                    <div class="uk-grid uk-flex uk-flex-left uk-flex-middle">
                        <img class="uk-grid-width-1-4" src="/produkte/Grafiken/CMS.png" width="90px"
                             alt="IDALABS MaWi">
                        <h2 style="margin-left: unset"
                            class=" uk-grid-width-1-4 uk-align-center  uk-margin-small uk-heading-medium uk-text-bold ">
                            Intranet und Dashboards - CMS</h2>
                        <a class="uk-button uk-button-primary uk-margin-remove uk-flex uk-accordion-title  uk-margin-large-top">Funktionen
                            & Preise</a>
                    </div>

                    <div class=" uk-accordion-content">

                        <!--Infobox -->
                       <?php $query = mysqli_query($conn, "SELECT * FROM module where modul_id = 13 ")
                       ;

                       while ($row = mysqli_fetch_array($query)) {
                          $kurztext = $row['kurztext'];
                       }


                       echo "  
                            <div style='padding-left: 30px; padding-right: 30px' class='uk-panel uk-space'>
                                <div style='padding-left: 0px;  padding-right: 0px' class='uk-panel uk-panel-box uk-flex uk-flex-middle uk-flex-center uk-panel-space '>
                                    <div style='padding-left: 0px;  padding-right: 0px' class='uk-panel uk-panel-space  uk-text-normal '>
                                        <p style='font-weight: normal'>  $kurztext</p>
                                    </div>
                                </div>
                            </div>";

                       ?>


                        <!--Ohne Unterkategorie-->
                        <div class=" uk-panel-space">
                            <table class="uk-table uk-table-striped  ">
                                <thead>
                                <tr>
                                    <th class=" uk-width-large-1-5 uk-padding-remove "><h3 class="text-ida-red">
                                        </h3></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary   ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php
                                $query = mysqli_query($conn, "SELECT * FROM features where modul_id = 13 ")
                                ;


                                while ($row = mysqli_fetch_array($query)) {
                                   if ($row['ab_plan'] == 3) {

                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-remove'></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 2) {
                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 1) {
                                      $state1 = "<div class='uk-icon-check-square '></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }


                                   echo


                                   "<tr>
                                        <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'> <h5 class='uk-margin-remove'> {$row['feature']} </h5> </td>
                                      
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state1</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state2</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state3</div></td>
                                        
                                      
                                       </tr>\n";

                                }

                                ?>


                                <!--Letzte Zeile  -->
                                <tfoot>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove"></h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                </tr>

                                </tfoot>


                                </tbody>
                            </table>
                        </div>


                        <!-- Preise -->
                        <div class=" uk-panel-space">
                            <h3 class="uk-padding-small">Preise</h3>
                            <table class="uk-table ">
                                <thead>
                                <tr>
                                    <th class="uk-width-large-2-5 "></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary  ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>

                                <tbody>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove">Preise für das
                                            CMS Modul pro Monat</h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom"> 45,00 €</div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom"> 75,00 €</div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom"> 95,00 €</div>
                                    </td>
                                </tr>

                                </tbody>


                                </thead>
                            </table>
                        </div>


                </th>
            </tr>


            </tbody>
        </table>
    </div>
</section>

<!-- TTS  -->
<section class="uk-container uk-container-center uk-margin-large-top uk-padding-remove  ">
    <div class="uk-panel uk-panel-box uk-panel-space has-shadow white-bg uk-overflow-container">

        <table class="uk-table ">
            <tbody class="uk-accordion" data-uk-accordion="{showfirst : false}">


            <tr>
                <th style="padding: 12px">


                    <div class="uk-grid uk-flex uk-flex-left uk-flex-middle">
                        <img class="uk-grid-width-1-4" src="/produkte/Grafiken/TTS.png" width="90px"
                             alt="IDALABS MaWi">
                        <h2 style="margin-left: unset"
                            class=" uk-grid-width-1-4 uk-align-center  uk-margin-small uk-heading-medium uk-text-bold ">
                            Aufgaben - TTS</h2>
                        <a class="uk-button uk-button-primary uk-margin-remove uk-flex uk-accordion-title  uk-margin-large-top">Funktionen
                            & Preise</a>
                    </div>

                    <div class=" uk-accordion-content">

                        <!--Infobox -->
                       <?php $query = mysqli_query($conn, "SELECT * FROM module where modul_id = 14 ")
                       ;

                       while ($row = mysqli_fetch_array($query)) {
                          $kurztext = $row['kurztext'];
                       }


                       echo "  
                            <div style='padding-left: 30px; padding-right: 30px' class='uk-panel uk-space'>
                                <div style='padding-left: 0px;  padding-right: 0px' class='uk-panel uk-panel-box uk-flex uk-flex-middle uk-flex-center uk-panel-space '>
                                    <div style='padding-left: 0px;  padding-right: 0px' class='uk-panel uk-panel-space  uk-text-normal '>
                                        <p style='font-weight: normal'>  $kurztext</p>
                                    </div>
                                </div>
                            </div>";

                       ?>


                        <!--Ohne Unterkategorie-->
                        <div class=" uk-panel-space">
                            <table class="uk-table uk-table-striped  ">
                                <thead>
                                <tr>
                                    <th class=" uk-width-large-1-5 uk-padding-remove "><h3 class="text-ida-red">
                                        </h3></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary   ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php
                                $query = mysqli_query($conn, "SELECT * FROM features where modul_id = 14 ")
                                ;


                                while ($row = mysqli_fetch_array($query)) {
                                   if ($row['ab_plan'] == 3) {

                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-remove'></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 2) {
                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 1) {
                                      $state1 = "<div class='uk-icon-check-square '></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }


                                   echo


                                   "<tr>
                                        <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'> <h5 class='uk-margin-remove'> {$row['feature']} </h5> </td>
                                      
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state1</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state2</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state3</div></td>
                                        
                                      
                                       </tr>\n";

                                }

                                ?>


                                <!--Letzte Zeile  -->
                                <tfoot>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove"></h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                </tr>

                                </tfoot>


                                </tbody>
                            </table>
                        </div>


                        <!-- Preise -->
                        <div class=" uk-panel-space">
                            <h3 class="uk-padding-small">Preise</h3>
                            <table class="uk-table ">
                                <thead>
                                <tr>
                                    <th class="uk-width-large-2-5 "></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary  ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>

                                <tbody>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove">Preise für das
                                            TTS Modul pro Monat</h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom"> 45,00 €</div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom"> 75,00 €</div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom"> 95,00 €</div>
                                    </td>
                                </tr>

                                </tbody>


                                </thead>
                            </table>
                        </div>


                </th>
            </tr>


            </tbody>
        </table>
    </div>
</section>

<!-- HR -->
<section class="uk-container uk-container-center uk-margin-large-top uk-padding-remove  ">
    <div class="uk-panel uk-panel-box uk-panel-space has-shadow white-bg uk-overflow-container">

        <table class="uk-table ">
            <tbody class="uk-accordion" data-uk-accordion="{showfirst : false}">


            <tr>
                <th style="padding: 12px">


                    <div class="uk-grid uk-flex uk-flex-left uk-flex-middle">
                        <img class="uk-grid-width-1-4" src="/produkte/Grafiken/HR.png" width="90px"
                             alt="IDALABS MaWi">
                        <h2 style="margin-left: unset"
                            class=" uk-grid-width-1-4 uk-align-center  uk-margin-small uk-heading-medium uk-text-bold ">
                            Personalmanagement - HR</h2>
                        <a class="uk-button uk-button-primary uk-margin-remove uk-flex uk-accordion-title  uk-margin-large-top">Funktionen
                            & Preise</a>
                    </div>

                    <div class=" uk-accordion-content">

                        <!--Infobox -->
                       <?php $query = mysqli_query($conn, "SELECT * FROM module where modul_id = 4 ")
                       ;

                       while ($row = mysqli_fetch_array($query)) {
                          $kurztext = $row['kurztext'];
                       }


                       echo "  
                            <div style='padding-left: 30px; padding-right: 30px' class='uk-panel uk-space'>
                                <div style='padding-left: 0px;  padding-right: 0px' class='uk-panel uk-panel-box uk-flex uk-flex-middle uk-flex-center uk-panel-space '>
                                    <div style='padding-left: 0px;  padding-right: 0px' class='uk-panel uk-panel-space  uk-text-normal '>
                                        <p style='font-weight: normal'>  $kurztext</p>
                                    </div>
                                </div>
                            </div>";

                       ?>

                        <!--Ohne Subkategorie -->
                        <div class=" uk-panel-space">
                            <table class="uk-table uk-table-striped  ">
                                <thead>
                                <tr>
                                    <th class=" uk-width-large-1-5 uk-padding-remove "><h3 class="text-ida-red">
                                        </h3></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary   ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php
                                $query = mysqli_query($conn, "SELECT * FROM features where modul_id = 4 and featuregruppe is NULL ")
                                ;


                                while ($row = mysqli_fetch_array($query)) {
                                   if ($row['ab_plan'] == 3) {

                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-remove'></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 2) {
                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 1) {
                                      $state1 = "<div class='uk-icon-check-square '></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }


                                   echo


                                   "<tr>
                                        <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'> <h5 class='uk-margin-remove'> {$row['feature']} </h5> </td>
                                      
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state1</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state2</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state3</div></td>
                                        
                                      
                                       </tr>\n";

                                }

                                ?>


                                <!--Letzte Zeile  -->
                                <tfoot>



                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove"></h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                </tr>

                                </tfoot>


                                </tbody>
                            </table>
                        </div>

                        <!--Reporting-->
                        <div class=" uk-panel-space">
                            <table class="uk-table uk-table-striped  ">
                                <thead>
                                <tr>
                                    <th class=" uk-width-large-1-5 uk-padding-remove "><h3 class="text-ida-red">
                                            Reporting </h3></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary   ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php
                                $query = mysqli_query($conn, "SELECT * FROM features where modul_id = 4 and featuregruppe = 'Reporting' ")
                                ;


                                while ($row = mysqli_fetch_array($query)) {
                                   if ($row['ab_plan'] == 3) {

                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-remove'></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 2) {
                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 1) {
                                      $state1 = "<div class='uk-icon-check-square '></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }


                                   echo


                                   "<tr>
                                        <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'> <h5 class='uk-margin-remove'> {$row['feature']} </h5> </td>
                                      
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state1</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state2</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state3</div></td>
                                        
                                      
                                       </tr>\n";

                                }

                                ?>


                                <!--Letzte Zeile  -->
                                <tfoot>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove"></h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                </tr>

                                </tfoot>


                                </tbody>
                            </table>
                        </div>

                        <!--Kapazitätsplanung-->
                        <div class=" uk-panel-space">
                            <table class="uk-table uk-table-striped  ">
                                <thead>
                                <tr>
                                    <th class=" uk-width-large-1-5 uk-padding-remove "><h3 class="text-ida-red">
                                            Kapazitätsplanung </h3></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary   ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php
                                $query = mysqli_query($conn, "SELECT * FROM features where modul_id = 4 and featuregruppe = 'Kapazitätsplanung' ")
                                ;


                                while ($row = mysqli_fetch_array($query)) {
                                   if ($row['ab_plan'] == 3) {

                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-remove'></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 2) {
                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 1) {
                                      $state1 = "<div class='uk-icon-check-square '></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }


                                   echo


                                   "<tr>
                                        <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'> <h5 class='uk-margin-remove'> {$row['feature']} </h5> </td>
                                      
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state1</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state2</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state3</div></td>
                                        
                                      
                                       </tr>\n";

                                }

                                ?>
                                <tr>
                                    <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'> <h5 class='uk-margin-remove'> Anzahl zu verwaltende Mitarbeiter</h5> </td>

                                    <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>25</div></td>
                                    <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>75</div></td>
                                    <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>unbegrenzt</div></td>


                                </tr>

                                <!--Letzte Zeile  -->
                                <tfoot>



                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove"></h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                </tr>

                                </tfoot>


                                </tbody>
                            </table>
                        </div>


                        <!-- Preise -->
                        <div class=" uk-panel-space">
                            <h3 class="uk-padding-small">Preise</h3>
                            <table class="uk-table ">
                                <thead>
                                <tr>
                                    <th class="uk-width-large-2-5 "></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary  ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>

                                <tbody>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove">Preise für das
                                            HR Modul pro Monat</h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom"> 45,00 €</div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom"> 75,00 €</div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom"> 95,00 €</div>
                                    </td>
                                </tr>

                                </tbody>


                                </thead>
                            </table>
                        </div>


                </th>
            </tr>


            </tbody>
        </table>
    </div>
</section>

<!-- FI  -->
<section class="uk-container uk-container-center uk-margin-large-top uk-padding-remove  ">
    <div class="uk-panel uk-panel-box uk-panel-space has-shadow white-bg uk-overflow-container">

        <table class="uk-table ">
            <tbody class="uk-accordion" data-uk-accordion="{showfirst : false}">


            <tr>
                <th style="padding: 12px">


                    <div class="uk-grid uk-flex uk-flex-left uk-flex-middle">
                        <img class="uk-grid-width-1-4" src="/produkte/Grafiken/FI.png" width="90px"
                             alt="IDALABS MaWi">
                        <h2 style="margin-left: unset"
                            class=" uk-grid-width-1-4 uk-align-center  uk-margin-small uk-heading-medium uk-text-bold ">
                            Finanzen und Controlling - FI</h2>
                        <a class="uk-button uk-button-primary uk-margin-remove uk-flex uk-accordion-title  uk-margin-large-top">Funktionen
                            & Preise</a>
                    </div>

                    <div class=" uk-accordion-content">

                        <!--Infobox -->
                       <?php $query = mysqli_query($conn, "SELECT * FROM module where modul_id = 5 ")
                       ;

                       while ($row = mysqli_fetch_array($query)) {
                          $kurztext = $row['kurztext'];
                       }


                       echo "  
                            <div style='padding-left: 30px; padding-right: 30px' class='uk-panel uk-space'>
                                <div style='padding-left: 0px;  padding-right: 0px' class='uk-panel uk-panel-box uk-flex uk-flex-middle uk-flex-center uk-panel-space '>
                                    <div style='padding-left: 0px;  padding-right: 0px' class='uk-panel uk-panel-space  uk-text-normal '>
                                        <p style='font-weight: normal'>  $kurztext</p>
                                    </div>
                                </div>
                            </div>";

                       ?>


                        <!--Ohne Unterkategorie-->
                        <div class=" uk-panel-space">
                            <table class="uk-table uk-table-striped  ">
                                <thead>
                                <tr>
                                    <th class=" uk-width-large-1-5 uk-padding-remove "><h3 class="text-ida-red">
                                        </h3></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary   ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php
                                $query = mysqli_query($conn, "SELECT * FROM features where modul_id = 5 ")
                                ;


                                while ($row = mysqli_fetch_array($query)) {
                                   if ($row['ab_plan'] == 3) {

                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-remove'></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 2) {
                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 1) {
                                      $state1 = "<div class='uk-icon-check-square '></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }


                                   echo


                                   "<tr>
                                        <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'> <h5 class='uk-margin-remove'> {$row['feature']} </h5> </td>
                                      
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state1</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state2</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state3</div></td>
                                        
                                      
                                       </tr>\n";

                                }

                                ?>


                                <!--Letzte Zeile  -->
                                <tfoot>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove"></h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                </tr>

                                </tfoot>


                                </tbody>
                            </table>
                        </div>


                        <!-- Preise -->
                        <div class=" uk-panel-space">
                            <h3 class="uk-padding-small">Preise</h3>
                            <table class="uk-table ">
                                <thead>
                                <tr>
                                    <th class="uk-width-large-2-5 "></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary  ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>

                                <tbody>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove">Preise für das
                                            FI Modul pro Monat</h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom"> 75,00 €</div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom"> 95,00 €</div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom"> 145,00 €</div>
                                    </td>
                                </tr>

                                </tbody>


                                </thead>
                            </table>
                        </div>


                </th>
            </tr>


            </tbody>
        </table>
    </div>
</section>

<!-- CRM  -->
<section class="uk-container uk-container-center uk-margin-large-top uk-padding-remove  ">
    <div class="uk-panel uk-panel-box uk-panel-space has-shadow white-bg uk-overflow-container">

        <table class="uk-table ">
            <tbody class="uk-accordion" data-uk-accordion="{showfirst : false}">


            <tr>
                <th style="padding: 12px">


                    <div class="uk-grid uk-flex uk-flex-left uk-flex-middle">
                        <img class="uk-grid-width-1-4" src="/produkte/Grafiken/CRM.png" width="90px"
                             alt="IDALABS MaWi">
                        <h2 style="margin-left: unset"
                            class=" uk-grid-width-1-4 uk-align-center  uk-margin-small uk-heading-medium uk-text-bold ">
                            Kunden- und Kontaktmanagement - CRM</h2>
                        <a class="uk-button uk-button-primary uk-margin-remove uk-flex uk-accordion-title  uk-margin-large-top">Funktionen
                            & Preise</a>
                    </div>

                    <div class=" uk-accordion-content">

                        <!--Infobox -->
                       <?php $query = mysqli_query($conn, "SELECT * FROM module where modul_id = 3 ")
                       ;

                       while ($row = mysqli_fetch_array($query)) {
                          $kurztext = $row['kurztext'];
                       }


                       echo "  
                            <div style='padding-left: 30px; padding-right: 30px' class='uk-panel uk-space'>
                                <div style='padding-left: 0px;  padding-right: 0px' class='uk-panel uk-panel-box uk-flex uk-flex-middle uk-flex-center uk-panel-space '>
                                    <div style='padding-left: 0px;  padding-right: 0px' class='uk-panel uk-panel-space  uk-text-normal '>
                                        <p style='font-weight: normal'>  $kurztext</p>
                                    </div>
                                </div>
                            </div>";

                       ?>


                        <!--Kontaktpflege und Dokumentationn-->
                        <div class=" uk-panel-space">
                            <table class="uk-table uk-table-striped  ">
                                <thead>
                                <tr>
                                    <th class=" uk-width-large-1-5 uk-padding-remove "><h3 class="text-ida-red">
                                            Kontaktpflege und Dokumentation</h3></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary   ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php
                                $query = mysqli_query($conn, "SELECT * FROM features where modul_id = 3 and featuregruppe = 'Kontaktpflege und Dokumentation'")
                                ;


                                while ($row = mysqli_fetch_array($query)) {
                                   if ($row['ab_plan'] == 3) {

                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-remove'></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 2) {
                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 1) {
                                      $state1 = "<div class='uk-icon-check-square '></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }


                                   echo


                                   "<tr>
                                        <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'> <h5 class='uk-margin-remove'> {$row['feature']} </h5> </td>
                                      
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state1</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state2</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state3</div></td>
                                        
                                      
                                       </tr>\n";

                                }

                                ?>


                                <!--Letzte Zeile  -->
                                <tfoot>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove"></h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                </tr>

                                </tfoot>


                                </tbody>
                            </table>
                        </div>

                        <!--Sales Prozess-->
                        <div class=" uk-panel-space">
                            <table class="uk-table uk-table-striped  ">
                                <thead>
                                <tr>
                                    <th class=" uk-width-large-1-5 uk-padding-remove "><h3 class="text-ida-red">
                                            Sales Prozess</h3></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary   ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php
                                $query = mysqli_query($conn, "SELECT * FROM features where modul_id = 3 and featuregruppe = 'Sales Prozess'")
                                ;


                                while ($row = mysqli_fetch_array($query)) {
                                   if ($row['ab_plan'] == 3) {

                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-remove'></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 2) {
                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 1) {
                                      $state1 = "<div class='uk-icon-check-square '></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }


                                   echo


                                   "<tr>
                                        <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'> <h5 class='uk-margin-remove'> {$row['feature']} </h5> </td>
                                      
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state1</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state2</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state3</div></td>
                                        
                                      
                                       </tr>\n";

                                }

                                ?>


                                <!--Letzte Zeile  -->
                                <tfoot>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove"></h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                </tr>

                                </tfoot>


                                </tbody>
                            </table>
                        </div>


                        <!-- Preise -->
                        <div class=" uk-panel-space">
                            <h3 class="uk-padding-small">Preise</h3>
                            <table class="uk-table ">
                                <thead>
                                <tr>
                                    <th class="uk-width-large-2-5 "></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary  ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>

                                <tbody>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove">Preise für das
                                            CRM Modul pro Monat</h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom"> 45,00 €</div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom"> 75,00 €</div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom"> 95,00 €</div>
                                    </td>
                                </tr>

                                </tbody>


                                </thead>
                            </table>
                        </div>


                </th>
            </tr>


            </tbody>
        </table>
    </div>
</section>

<!-- CAFM  -->
<section class="uk-container uk-container-center uk-margin-large-top uk-padding-remove  ">
    <div class="uk-panel uk-panel-box uk-panel-space has-shadow white-bg uk-overflow-container">

        <table class="uk-table ">
            <tbody class="uk-accordion" data-uk-accordion="{showfirst : false}">


            <tr>
                <th style="padding: 12px">


                    <div class="uk-grid uk-flex uk-flex-left uk-flex-middle">
                        <img class="uk-grid-width-1-4" src="/produkte/Grafiken/CAFM.png" width="90px"
                             alt="IDALABS MaWi">
                        <h2 style="margin-left: unset"
                            class=" uk-grid-width-1-4 uk-align-center  uk-margin-small uk-heading-medium uk-text-bold ">
                            Gebäude und Bauvorhaben - CAFM</h2>
                        <a class="uk-button uk-button-primary uk-margin-remove uk-flex uk-accordion-title  uk-margin-large-top">Funktionen
                            & Preise</a>
                    </div>

                    <div class=" uk-accordion-content">

                        <!--Infobox -->
                       <?php $query = mysqli_query($conn, "SELECT * FROM module where modul_id = 6 ")
                       ;

                       while ($row = mysqli_fetch_array($query)) {
                          $kurztext = $row['kurztext'];
                       }


                       echo "  
                            <div style='padding-left: 30px; padding-right: 30px' class='uk-panel uk-space'>
                                <div style='padding-left: 0px;  padding-right: 0px' class='uk-panel uk-panel-box uk-flex uk-flex-middle uk-flex-center uk-panel-space '>
                                    <div style='padding-left: 0px;  padding-right: 0px' class='uk-panel uk-panel-space  uk-text-normal '>
                                        <p style='font-weight: normal'>  $kurztext</p>
                                    </div>
                                </div>
                            </div>";

                       ?>


                        <!--Ohne Unterkategorie-->
                        <div class=" uk-panel-space">
                            <table class="uk-table uk-table-striped  ">
                                <thead>
                                <tr>
                                    <th class=" uk-width-large-1-5 uk-padding-remove "><h3 class="text-ida-red">
                                        </h3></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary   ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php
                                $query = mysqli_query($conn, "SELECT * FROM features where modul_id = 6 ")
                                ;


                                while ($row = mysqli_fetch_array($query)) {
                                   if ($row['ab_plan'] == 3) {

                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-remove'></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 2) {
                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 1) {
                                      $state1 = "<div class='uk-icon-check-square '></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }


                                   echo


                                   "<tr>
                                        <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'> <h5 class='uk-margin-remove'> {$row['feature']} </h5> </td>
                                      
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state1</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state2</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state3</div></td>
                                        
                                      
                                       </tr>\n";

                                }

                                ?>


                                <!--Letzte Zeile  -->
                                <tfoot>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove"></h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                </tr>

                                </tfoot>


                                </tbody>
                            </table>
                        </div>


                        <!-- Preise -->
                        <div class=" uk-panel-space">
                            <h3 class="uk-padding-small">Preise</h3>
                            <table class="uk-table ">
                                <thead>
                                <tr>
                                    <th class="uk-width-large-2-5 "></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary  ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>

                                <tbody>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove">Preise für das
                                            CAFM Modul pro Monat</h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom"> 25,00 €</div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom"> 45,00 €</div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom"> 65,00 €</div>
                                    </td>
                                </tr>

                                </tbody>


                                </thead>
                            </table>
                        </div>


                </th>
            </tr>


            </tbody>
        </table>
    </div>
</section>

<!-- Mail  -->
<section class="uk-container uk-container-center uk-margin-large-top uk-padding-remove  ">
    <div class="uk-panel uk-panel-box uk-panel-space has-shadow white-bg uk-overflow-container">

        <table class="uk-table ">
            <tbody class="uk-accordion" data-uk-accordion="{showfirst : false}">


            <tr>
                <th style="padding: 12px">


                    <div class="uk-grid uk-flex uk-flex-left uk-flex-middle">
                        <img class="uk-grid-width-1-4" src="/produkte/Grafiken/Mail.png" width="90px"
                             alt="IDALABS MaWi">
                        <h2 style="margin-left: unset"
                            class=" uk-grid-width-1-4 uk-align-center  uk-margin-small uk-heading-medium uk-text-bold ">
                            MAIL</h2>
                        <a class="uk-button uk-button-primary uk-margin-remove uk-flex uk-accordion-title  uk-margin-large-top">Funktionen
                            & Preise</a>
                    </div>

                    <div class=" uk-accordion-content">

                        <!--Infobox -->
                       <?php $query = mysqli_query($conn, "SELECT * FROM module where modul_id = 10 ")
                       ;

                       while ($row = mysqli_fetch_array($query)) {
                          $kurztext = $row['kurztext'];
                       }


                       echo "  
                            <div style='padding-left: 30px; padding-right: 30px' class='uk-panel uk-space'>
                                <div style='padding-left: 0px;  padding-right: 0px' class='uk-panel uk-panel-box uk-flex uk-flex-middle uk-flex-center uk-panel-space '>
                                    <div style='padding-left: 0px;  padding-right: 0px' class='uk-panel uk-panel-space  uk-text-normal '>
                                        <p style='font-weight: normal'>  $kurztext</p>
                                    </div>
                                </div>
                            </div>";

                       ?>


                        <!--Ohne Unterkategorie-->
                        <div class=" uk-panel-space">
                            <table class="uk-table uk-table-striped  ">
                                <thead>
                                <tr>
                                    <th class=" uk-width-large-1-5 uk-padding-remove "><h3 class="text-ida-red">
                                        </h3></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary   ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php
                                $query = mysqli_query($conn, "SELECT * FROM features where modul_id = 10 ")
                                ;


                                while ($row = mysqli_fetch_array($query)) {
                                   if ($row['ab_plan'] == 3) {

                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-remove'></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 2) {
                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 1) {
                                      $state1 = "<div class='uk-icon-check-square '></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }


                                   echo


                                   "<tr>
                                        <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'> <h5 class='uk-margin-remove'> {$row['feature']} </h5> </td>
                                      
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state1</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state2</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state3</div></td>
                                        
                                      
                                       </tr>\n";

                                }

                                ?>

                                <tr>
                                    <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'> <h5 class='uk-margin-remove'> Mailaccounts </h5> </td>

                                    <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>1</div></td>
                                    <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>25</div></td>
                                    <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>100</div></td>


                                </tr>


                                <!--Letzte Zeile  -->
                                <tfoot>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove"></h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                </tr>

                                </tfoot>


                                </tbody>
                            </table>
                        </div>


                        <!-- Preise -->
                        <div class=" uk-panel-space">
                            <h3 class="uk-padding-small">Preise</h3>
                            <table class="uk-table ">
                                <thead>
                                <tr>
                                    <th class="uk-width-large-2-5 "></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary  ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>

                                <tbody>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove">Preise für das
                                            MAIL Modul pro Monat</h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom"> 45,00 €</div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom"> 75,00 €</div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom"> 95,00 €</div>
                                    </td>
                                </tr>

                                </tbody>


                                </thead>
                            </table>
                        </div>


                </th>
            </tr>


            </tbody>
        </table>
    </div>
</section>

<!--Zeiterfassung  -->
<section class="uk-container uk-container-center uk-margin-large-top uk-padding-remove  ">
    <div class="uk-panel uk-panel-box uk-panel-space has-shadow white-bg uk-overflow-container">

        <table class="uk-table ">
            <tbody class="uk-accordion" data-uk-accordion="{showfirst : false}">


            <tr>
                <th style="padding: 12px">


                    <div class="uk-grid uk-flex uk-flex-left uk-flex-middle">
                        <img class="uk-grid-width-1-4" src="/produkte/Grafiken/ZeWi.png" width="90px"
                             alt="IDALABS MaWi">
                        <h2 style="margin-left: unset"
                            class=" uk-grid-width-1-4 uk-align-center  uk-margin-small uk-heading-medium uk-text-bold ">
                            Zeiterfassung</h2>
                        <a class="uk-button uk-button-primary uk-margin-remove uk-flex uk-accordion-title  uk-margin-large-top">Funktionen
                            & Preise</a>
                    </div>

                    <div class=" uk-accordion-content">

                        <!--Infobox -->
                       <?php $query = mysqli_query($conn, "SELECT * FROM module where modul_id = 11 ")
                       ;

                       while ($row = mysqli_fetch_array($query)) {
                          $kurztext = $row['kurztext'];
                       }


                       echo "  
                            <div style='padding-left: 30px; padding-right: 30px' class='uk-panel uk-space'>
                                <div style='padding-left: 0px;  padding-right: 0px' class='uk-panel uk-panel-box uk-flex uk-flex-middle uk-flex-center uk-panel-space '>
                                    <div style='padding-left: 0px;  padding-right: 0px' class='uk-panel uk-panel-space  uk-text-normal '>
                                        <p style='font-weight: normal'>  $kurztext</p>
                                    </div>
                                </div>
                            </div>";

                       ?>


                        <!--Ohne Unterkategorie-->
                        <div class=" uk-panel-space">
                            <table class="uk-table uk-table-striped  ">
                                <thead>
                                <tr>
                                    <th class=" uk-width-large-1-5 uk-padding-remove "><h3 class="text-ida-red">
                                        </h3></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary   ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php
                                $query = mysqli_query($conn, "SELECT * FROM features where modul_id = 11 ")
                                ;


                                while ($row = mysqli_fetch_array($query)) {
                                   if ($row['ab_plan'] == 3) {

                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-remove'></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 2) {
                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 1) {
                                      $state1 = "<div class='uk-icon-check-square '></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }


                                   echo


                                   "<tr>
                                        <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'> <h5 class='uk-margin-remove'> {$row['feature']} </h5> </td>
                                      
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state1</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state2</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state3</div></td>
                                        
                                      
                                       </tr>\n";

                                }

                                ?>


                                <!--Letzte Zeile  -->
                                <tfoot>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove"></h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                </tr>

                                </tfoot>


                                </tbody>
                            </table>
                        </div>


                        <!-- Preise -->
                        <div class=" uk-panel-space">
                            <h3 class="uk-padding-small">Preise</h3>
                            <table class="uk-table ">
                                <thead>
                                <tr>
                                    <th class="uk-width-large-2-5 "></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary  ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>

                                <tbody>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove">Preise für das
                                            Zeiterfassungs Modul pro Monat</h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom"> 45,00 €</div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom"> 75,00 €</div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom"> 95,00 €</div>
                                    </td>
                                </tr>

                                </tbody>


                                </thead>
                            </table>
                        </div>


                </th>
            </tr>


            </tbody>
        </table>
    </div>
</section>

<!--CAL  -->
<section class="uk-container uk-container-center uk-margin-large-top uk-padding-remove  ">
    <div class="uk-panel uk-panel-box uk-panel-space has-shadow white-bg uk-overflow-container">

        <table class="uk-table ">
            <tbody class="uk-accordion" data-uk-accordion="{showfirst : false}">


            <tr>
                <th style="padding: 12px">


                    <div class="uk-grid uk-flex uk-flex-left uk-flex-middle">
                        <img class="uk-grid-width-1-4" src="/produkte/Grafiken/CAL.png" width="90px"
                             alt="IDALABS MaWi">
                        <h2 style="margin-left: unset"
                            class=" uk-grid-width-1-4 uk-align-center  uk-margin-small uk-heading-medium uk-text-bold ">
                            Kalender - CAL</h2>
                        <a class="uk-button uk-button-primary uk-margin-remove uk-flex uk-accordion-title  uk-margin-large-top">Funktionen
                            & Preise</a>
                    </div>

                    <div class=" uk-accordion-content">

                        <!--Infobox -->
                       <?php $query = mysqli_query($conn, "SELECT * FROM module where modul_id = 8 ")
                       ;

                       while ($row = mysqli_fetch_array($query)) {
                          $kurztext = $row['kurztext'];
                       }


                       echo "  
                            <div style='padding-left: 30px; padding-right: 30px' class='uk-panel uk-space'>
                                <div style='padding-left: 0px;  padding-right: 0px' class='uk-panel uk-panel-box uk-flex uk-flex-middle uk-flex-center uk-panel-space '>
                                    <div style='padding-left: 0px;  padding-right: 0px' class='uk-panel uk-panel-space  uk-text-normal '>
                                        <p style='font-weight: normal'>  $kurztext</p>
                                    </div>
                                </div>
                            </div>";

                       ?>

                        <!--Ohne Unterkategorie-->
                        <div class=" uk-panel-space">
                            <table class="uk-table uk-table-striped  ">
                                <thead>
                                <tr>
                                    <th class=" uk-width-large-1-5 uk-padding-remove "><h3 class="text-ida-red">
                                        </h3></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary   ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php
                                $query = mysqli_query($conn, "SELECT * FROM features where modul_id = 8 ")
                                ;


                                while ($row = mysqli_fetch_array($query)) {
                                   if ($row['ab_plan'] == 3) {

                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-remove'></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 2) {
                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 1) {
                                      $state1 = "<div class='uk-icon-check-square '></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }


                                   echo


                                   "<tr>
                                        <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'> <h5 class='uk-margin-remove'> {$row['feature']} </h5> </td>
                                      
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state1</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state2</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state3</div></td>
                                        
                                      
                                       </tr>\n";

                                }

                                ?>


                                <!--Letzte Zeile  -->
                                <tfoot>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove"></h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                </tr>

                                </tfoot>


                                </tbody>
                            </table>
                        </div>


                        <!-- Preise -->
                        <div class=" uk-panel-space">
                            <h3 class="uk-padding-small">Preise</h3>
                            <table class="uk-table ">
                                <thead>
                                <tr>
                                    <th class="uk-width-large-2-5 "></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary  ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>

                                <tbody>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove">Preise für das
                                            CAL Modul pro Monat</h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom"> 45,00 €</div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom"> 75,00 €</div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom"> 95,00 €</div>
                                    </td>
                                </tr>

                                </tbody>


                                </thead>
                            </table>
                        </div>


                </th>
            </tr>


            </tbody>
        </table>
    </div>
</section>

<!--DMS  -->
<section class="uk-container uk-container-center uk-margin-large-top uk-padding-remove  ">
    <div class="uk-panel uk-panel-box uk-panel-space has-shadow white-bg uk-overflow-container">

        <table class="uk-table ">
            <tbody class="uk-accordion" data-uk-accordion="{showfirst : false}">


            <tr>
                <th style="padding: 12px">


                    <div class="uk-grid uk-flex uk-flex-left uk-flex-middle">
                        <img class="uk-grid-width-1-4" src="/produkte/Grafiken/CAL.png" width="90px"
                             alt="IDALABS MaWi">
                        <h2 style="margin-left: unset"
                            class=" uk-grid-width-1-4 uk-align-center  uk-margin-small uk-heading-medium uk-text-bold ">
                            Dokumentenmanagement - DMS</h2>
                        <a class="uk-button uk-button-primary uk-margin-remove uk-flex uk-accordion-title  uk-margin-large-top">Funktionen
                            & Preise</a>
                    </div>

                    <div class=" uk-accordion-content">

                        <!--Infobox -->
                       <?php $query = mysqli_query($conn, "SELECT * FROM module where modul_id = 7 ")
                       ;

                       while ($row = mysqli_fetch_array($query)) {
                          $kurztext = $row['kurztext'];
                       }


                       echo "  
                            <div style='padding-left: 30px; padding-right: 30px' class='uk-panel uk-space'>
                                <div style='padding-left: 0px;  padding-right: 0px' class='uk-panel uk-panel-box uk-flex uk-flex-middle uk-flex-center uk-panel-space '>
                                    <div style='padding-left: 0px;  padding-right: 0px' class='uk-panel uk-panel-space  uk-text-normal '>
                                        <p style='font-weight: normal'>  $kurztext</p>
                                    </div>
                                </div>
                            </div>";

                       ?>


                        <!--Ohne Unterkategorie-->
                        <div class=" uk-panel-space">
                            <table class="uk-table uk-table-striped  ">
                                <thead>
                                <tr>
                                    <th class=" uk-width-large-1-5 uk-padding-remove "><h3 class="text-ida-red">
                                        </h3></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary   ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php
                                $query = mysqli_query($conn, "SELECT * FROM features where modul_id = 7 ")
                                ;


                                while ($row = mysqli_fetch_array($query)) {
                                   if ($row['ab_plan'] == 3) {

                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-remove'></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 2) {
                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 1) {
                                      $state1 = "<div class='uk-icon-check-square '></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }


                                   echo


                                   "<tr>
                                        <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'> <h5 class='uk-margin-remove'> {$row['feature']} </h5> </td>
                                      
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state1</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state2</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state3</div></td>
                                        
                                      
                                       </tr>\n";

                                }

                                ?>

                                <tr>
                                    <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'> <h5 class='uk-margin-remove'> Speicherplatz </h5> </td>

                                    <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>1 GB</div></td>
                                    <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>10 GB</div></td>
                                    <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>100 GB</div></td>


                                </tr>


                                <!--Letzte Zeile  -->
                                <tfoot>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove"></h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                </tr>

                                </tfoot>


                                </tbody>
                            </table>
                        </div>


                        <!-- Preise -->
                        <div class=" uk-panel-space">
                            <h3 class="uk-padding-small">Preise</h3>
                            <table class="uk-table ">
                                <thead>
                                <tr>
                                    <th class="uk-width-large-2-5 "></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary  ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>

                                <tbody>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove">Preise für das
                                            DMS Modul pro Monat</h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom"> 45,00 €</div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom"> 75,00 €</div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom"> 95,00 €</div>
                                    </td>
                                </tr>

                                </tbody>


                                </thead>
                            </table>
                        </div>


                </th>
            </tr>


            </tbody>
        </table>
    </div>
</section>

<!-- Daisy  -->
<section class="uk-container uk-container-center uk-margin-large-top uk-padding-remove  ">
    <div class="uk-panel uk-panel-box uk-panel-space has-shadow white-bg uk-overflow-container">

        <table class="uk-table ">
            <tbody class="uk-accordion" data-uk-accordion="{showfirst : false}">


            <tr>
                <th style="padding: 12px">


                    <div class="uk-grid uk-flex uk-flex-left uk-flex-middle">
                        <img class="uk-grid-width-1-4" src="/produkte/Grafiken/Daisy.png" width="90px"
                             alt="IDALABS MaWi">
                        <h2 style="margin-left: unset"
                            class=" uk-grid-width-1-4 uk-align-center  uk-margin-small uk-heading-medium uk-text-bold ">
                            Externe Software Integrationen - DAISY</h2>
                        <a class="uk-button uk-button-primary uk-margin-remove uk-flex uk-accordion-title  uk-margin-large-top">Funktionen
                            & Preise</a>
                    </div>

                    <div class=" uk-accordion-content">

                        <!--Infobox -->
                       <?php $query = mysqli_query($conn, "SELECT * FROM module where modul_id = 21 ")
                       ;

                       while ($row = mysqli_fetch_array($query)) {
                          $kurztext = $row['kurztext'];
                       }


                       echo "  
                            <div style='padding-left: 30px; padding-right: 30px' class='uk-panel uk-space'>
                                <div style='padding-left: 0px;  padding-right: 0px' class='uk-panel uk-panel-box uk-flex uk-flex-middle uk-flex-center uk-panel-space '>
                                    <div style='padding-left: 0px;  padding-right: 0px' class='uk-panel uk-panel-space  uk-text-normal '>
                                        <p style='font-weight: normal'>  $kurztext</p>
                                    </div>
                                </div>
                            </div>";

                       ?>


                        <!-- Kaufmännische Software-->
                        <div class=" uk-panel-space">
                            <table class="uk-table uk-table-striped  ">
                                <thead>
                                <tr>
                                    <th class=" uk-width-large-1-5 uk-padding-remove "><h3 class="text-ida-red">
                                            Kaufmännische Software</h3></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary   ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>


                                <tr>
                                    <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'><h5
                                                class='uk-margin-remove'> DateV </h5></td>

                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>1</div>
                                    </td>
                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>2</div>
                                    </td>
                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>unbegrentzt</div>
                                    </td>

                                </tr>
                                <tr>
                                    <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'><h5
                                                class='uk-margin-remove'> ELO</h5></td>

                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>1</div>
                                    </td>
                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>2</div>
                                    </td>
                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>unbegrentzt</div>
                                    </td>

                                </tr>
                                <tr>
                                    <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'><h5
                                                class='uk-margin-remove'> StarMoney</h5></td>

                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>1</div>
                                    </td>
                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>2</div>
                                    </td>
                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>unbegrentzt</div>
                                    </td>

                                </tr>


                                <!--Letzte Zeile  -->
                                <tfoot>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove"></h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                </tr>

                                </tfoot>


                                </tbody>
                            </table>
                        </div>

                        <!-- Konstruktionssoftware-->
                        <div class=" uk-panel-space">
                            <table class="uk-table uk-table-striped  ">
                                <thead>
                                <tr>
                                    <th class=" uk-width-large-1-5 uk-padding-remove "><h3 class="text-ida-red">
                                            Konstruktionssoftware</h3></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary   ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>

                                <tr>
                                    <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'><h5
                                                class='uk-margin-remove'> Klaes</h5></td>

                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>1</div>
                                    </td>
                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>2</div>
                                    </td>
                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>unbegrentzt</div>
                                    </td>

                                </tr>
                                <tr>
                                    <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'><h5
                                                class='uk-margin-remove'> Schücal</h5></td>

                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>1</div>
                                    </td>
                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>2</div>
                                    </td>
                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>unbegrentzt</div>
                                    </td>

                                </tr>
                                <tr>
                                    <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'><h5
                                                class='uk-margin-remove'> Digi</h5></td>

                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>1</div>
                                    </td>
                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>2</div>
                                    </td>
                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>unbegrentzt</div>
                                    </td>

                                </tr>
                                <tr>
                                    <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'><h5
                                                class='uk-margin-remove'> FePro</h5></td>

                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>1</div>
                                    </td>
                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>2</div>
                                    </td>
                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>unbegrentzt</div>
                                    </td>

                                </tr>
                                <tr>
                                    <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'><h5
                                                class='uk-margin-remove'> OrgaData</h5></td>

                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>1</div>
                                    </td>
                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>2</div>
                                    </td>
                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>unbegrentzt</div>
                                    </td>

                                </tr>
                                <tr>
                                    <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'><h5
                                                class='uk-margin-remove'> SEMA</h5></td>

                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>1</div>
                                    </td>
                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>2</div>
                                    </td>
                                    <td class='uk-text-center uk-width-large-1-5  '>
                                        <div class='uk-panel-box-flo uk-color-g '>unbegrentzt</div>
                                    </td>

                                </tr>


                                <!--Letzte Zeile  -->
                                <tfoot>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove"></h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                </tr>

                                </tfoot>


                                </tbody>
                            </table>
                        </div>

                        <!-- Preise -->
                        <div class=" uk-panel-space">
                            <h3 class="uk-padding-small">Preise</h3>
                            <table class="uk-table ">
                                <thead>
                                <tr>
                                    <th class="uk-width-large-2-5 "></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary  ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>

                                <tbody>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove">Preise für das
                                            DAISY Modul pro Monat</h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom"> 95,00 €</div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom"> 195,00 €</div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom"> 245,00 €</div>
                                    </td>
                                </tr>

                                </tbody>


                                </thead>
                            </table>
                        </div>


                </th>
            </tr>


            </tbody>
        </table>
    </div>
</section>

<!--Service & Support  -->
<section class="uk-container uk-container-center uk-margin-large-top uk-padding-remove  ">
    <div class="uk-panel uk-panel-box uk-panel-space has-shadow white-bg uk-overflow-container">

        <table class="uk-table ">
            <tbody class="uk-accordion" data-uk-accordion="{showfirst : false}">


            <tr>
                <th style="padding: 12px">


                    <div class="uk-grid uk-flex uk-flex-left uk-flex-middle">
                        <img class="uk-grid-width-1-4" src="/produkte/Grafiken/idalabslogo.png" width="90px"
                             alt="IDALABS MaWi">
                        <h2 style="margin-left: unset"
                            class=" uk-grid-width-1-4 uk-align-center  uk-margin-small uk-heading-medium uk-text-bold ">
                            Service & Support</h2>
                        <a class="uk-button uk-button-primary uk-margin-remove uk-flex uk-accordion-title  uk-margin-large-top">Funktionen
                            & Preise</a>
                    </div>

                    <div class=" uk-accordion-content">

                        <!--Infobox -->
                       <?php $query = mysqli_query($conn, "SELECT * FROM module where modul_id = 22 ")
                       ;

                       while ($row = mysqli_fetch_array($query)) {
                          $kurztext = $row['kurztext'];
                       }


                       echo "  
                            <div style='padding-left: 30px; padding-right: 30px' class='uk-panel uk-space'>
                                <div style='padding-left: 0px;  padding-right: 0px' class='uk-panel uk-panel-box uk-flex uk-flex-middle uk-flex-center uk-panel-space '>
                                    <div style='padding-left: 0px;  padding-right: 0px' class='uk-panel uk-panel-space  uk-text-normal '>
                                        <p style='font-weight: normal'>  $kurztext</p>
                                    </div>
                                </div>
                            </div>";

                       ?>


                        <!--Ohne Unterkategorie-->
                        <div class=" uk-panel-space">
                            <table class="uk-table uk-table-striped  ">
                                <thead>
                                <tr>
                                    <th class=" uk-width-large-1-5 uk-padding-remove "><h3 class="text-ida-red">
                                        </h3></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary   ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>
                                </thead>

                                <tbody>

                                <?php
                                $query = mysqli_query($conn, "SELECT * FROM features where modul_id = 22 ")
                                ;


                                while ($row = mysqli_fetch_array($query)) {
                                   if ($row['ab_plan'] == 3) {

                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-remove'></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 2) {
                                      $state1 = "<div class='uk-icon-remove'></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }

                                   if ($row['ab_plan'] == 1) {
                                      $state1 = "<div class='uk-icon-check-square '></div>";
                                      $state2 = "<div class='uk-icon-check-square '></div>";
                                      $state3 = "<div class='uk-icon-check-square '></div>";
                                   }


                                   echo


                                   "<tr>
                                        <td class='uk-width-large-2-5 uk-padding-remove uk-color-g'> <h5 class='uk-margin-remove'> {$row['feature']} </h5> </td>
                                      
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state1</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state2</div></td>
                                        <td class='uk-text-center uk-width-large-1-5  '><div class='uk-panel-box-flo uk-color-g '>$state3</div></td>
                                        
                                      
                                       </tr>\n";

                                }

                                ?>


                                <!--Letzte Zeile  -->
                                <tfoot>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove"></h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom">
                                            <div></div>
                                        </div>
                                    </td>
                                </tr>

                                </tfoot>


                                </tbody>
                            </table>
                        </div>


                        <!-- Preise -->
                        <div class=" uk-panel-space">
                            <h3 class="uk-padding-small">Preise</h3>
                            <table class="uk-table ">
                                <thead>
                                <tr>
                                    <th class="uk-width-large-2-5 "></th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary  ">Standard</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5  ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Professional</div>
                                    </th>
                                    <th class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-top uk-panel-box-primary ">Premium</div>
                                    </th>
                                </tr>

                                <tbody>
                                <tr>
                                    <td class="uk-padding-remove"><h5 class="uk-margin-remove">Preise für das
                                            Service & Support Modul pro Monat</h5></td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom"> 144,00 €</div>
                                    </td>
                                    <td class="uk-text-center">
                                        <div class="uk-panel-box-flo-bottom"> 194,00 €</div>
                                    </td>
                                    <td class="uk-text-center uk-width-large-1-5 ">
                                        <div class="uk-panel-box-flo-bottom"> 244,00 €</div>
                                    </td>
                                </tr>

                                </tbody>


                                </thead>
                            </table>
                        </div>


                </th>
            </tr>
            </tbody>
        </table>
    </div>
</section>


<?php include("../_templates/kontakt.inc.php"); ?>
<?php include("../_templates/footer.inc.php"); ?>
