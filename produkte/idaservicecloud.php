<?php
$pageTitle = "IDA Service Cloud";
$metaKeywords = "Cloud Service, Lieferantenanbindung, OpenTrans, EDI";
include("../_templates/header.inc.php");
?>
    <section class="uk-container uk-container-center uk-margin-large-top uk-padding-vertical-remove">
        <h1 class="uk-heading-large uk-text-center"><?php echo $pageTitle ?></h1>
        </div>
    </section>


    <!-- Projekte Box -->
    <section class="uk-container uk-container-center uk-margin-large-top uk-padding-remove ">
        <div class="uk-grid uk-grid-collapse sec has-shadow white-bg ">
            <div class="uk-width-large-1-2 uk-position- uk-panel uk-panel-box">
                <div style="padding-bottom: 0px!important;"
                     class="uk-panel uk-panel-box  uk-flex uk-flex-middle uk-flex-center uk-padding-remove">
                    <div class="uk-panel-space">
                        <h1>Betreiben</h1>
                        <p>
                            Mit der <strong>IDA Service Cloud</strong> bieten wir Ihnen ein rundum
                            sorglos Angebot für den Betrieb unserer Produkte.
                            Sie sind Handwerker. Sie müssen sich nicht um die IT kümmern. Das machen wir für Sie.
                            Überlassen Sie die Sorgen um IT-Sicherheit, DSGVO, Verfügbarkeit, Stabilität usw. echt
                            Profis auf
                            diesem Gebiet.
                            Dabei ist die IDA Service Cloud für Sie ein echter Kostenvorteil - denn Hardware, Strom und
                            sämtliche laufenden Kosten rund um den Betrieb von IT Infrastruktur sind in der IDA
                            Servicecloud
                            automatisch inbegriffen.
                        </p>
                    </div>
                </div>

            </div>
            <div class="uk-width-large-1-2">
                <div class="uk-panel uk-panel-box uk-panel-space ">
                    <div class="uk-position-relative uk-visible-large">
                        <img src="/assets/img/teaser/teaser.png" alt="IDALABS Porojekte">
                    </div>

                </div>
            </div>
        </div>
    </section>

    <!-- IDA Ökosystem-->
    <section id="Haus" class="uk-container uk-container-center uk-margin-large-top uk-padding-remove ">
        <div class="uk-grid uk-grid-collapse sec has-shadow white-bg ">

            <div class="uk-width-large-3-4 uk-panel uk-panel-box">
                <div class="uk-panel-space">
                    <h2 class="text-ida-red">IDA Ökosystem </h2>
                    <p>Wir kümmern uns um den Betrieb der IDA in Ihrem Unternehmen und halten Sie als Handwerksbetrieb
                        frei von
                        den
                        Erschwernissen der IT, so dass die bislang in der EDV gebundenen Ressourcen für die
                        betrieblichen
                        Kernaufgaben frei werden. In der IDA-Servicecloud bündeln wir für Sie jede Menge
                        IT-Infrastruktur, um die Sie sich
                        sonst
                        kümmern müßten.</p>

                    <div class="uk-grid">
                        <div class="uk-width-1-3">
                            <ul class="uk-list-space">
                                <li>Backup</li>
                                <li>Firewall</li>
                                <li>Datenbank</li>
                                <li>Monitoring</li>
                            </ul>
                        </div>

                        <div class="uk-width-1-3">
                            <ul class="uk-list-space">
                                <li>Updates</li>
                                <li>Authentifizierung</li>
                                <li>DNS</li>
                                <li>Email</li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
            <div class="uk-width-large-1-4 uk-visible-large uk-panel uk-panel-box ">
                <div class="uk-panel-space">
                    <img src="/assets/img/idaservicecloud.png" width="170" >
                </div>
            </div>
            <div class="uk-width-large-4-4 uk-panel uk-panel-box">
                <div class="uk-panel-space uk-padding-top-remove">

                    <div class="uk-panel uk-panel-box uk-padding-top-remove uk-align-center">
                        <img src="/produkte/Grafiken/uebersicht_betreiben.png" alt="IDALABS Betreiben" id="img" onclick="swipe(id)">
                    </div>
                </div>
            </div>
        </div>
    </section>





<?php include("../_templates/kontakt.inc.php"); ?>
<?php include("../_templates/footer.inc.php");