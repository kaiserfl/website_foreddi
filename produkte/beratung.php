<?php
$pageTitle    = "Beratung und Dienstleistungen";
$metaKeywords = "IT und Prozessberatung, IT-Sicherheits Reifegradanalyse, Materialwirtschaft Lageroptimierung, Haus der Digitalisierung, IT-Strategie Beratung";
include("../_templates/header.inc.php");
?>

<!-- Überschrift -->
<section class="uk-container uk-container-center uk-margin-large-top uk-padding-vertical-remove">
    <div class="uk-panel uk-space uk-text-center">
        <h1 class="uk-heading-large"><?php echo $pageTitle; ?></h1>
    </div>
</section>

<!-- Info Kasten -->
<section class="uk-container uk-container-center uk-margin-large-top uk-padding-remove ">
    <div class="uk-grid uk-grid-collapse sec has-shadow white-bg ">
        <div class="uk-width-large-1-2 uk-position- uk-panel uk-panel-box">
            <div style="padding-bottom: 50px!important;"
                 class="uk-panel uk-panel-box  uk-flex uk-flex-middle uk-flex-center uk-padding-remove">
                <div class="uk-panel-space">
                    <h1>IT & Prozessberatung</h1>
                    <p> Können Sie einschätzen wie der digitale Reifegrad ihres Betriebes ist ? Gerade im Hinblick auf
                        Sicherheit und Strategie, ist bei vielen Handwerksbetrieben noch Luft nach oben. Geimeinsam
                        finden wir heraus wie ihr Unternehmen digital aufgestellt ist und können Ihnen eine
                        maßgeschneiderte Lösung aufzeigen. Ganz gleich, ob Sie eine zielgerichtete Beratung benötigen
                        oder eine vollumfassende Analyse mit umsetzbaren Handlungsempfehlungen, zu Ihrer Digitalisierung
                        suchen, wir sind für Sie der perfekte Ansprechpartner. </p>
                </div>
            </div>

        </div>
        <div class="uk-width-large-1-2">
            <div
                    class="uk-panel uk-panel-box uk-panel-box-primary uk-flex uk-flex-middle uk-flex-center uk-padding-remove">
                <div class="uk-position-relative">
                    <img src="/assets/img/table.jpg" alt="IDALABS Tisch">
                </div>

            </div>
        </div>
    </div>
</section>

<!-- Unser Beratungsangebot -->
<section class="uk-container uk-container-center white-bg uk-margin-large-top has-shadow">
    <div class="uk-panel uk-text-center">
        <h2>Unser Beratungsangebot</h2>
    </div>
    <div class="uk-panel uk-margin-large-top uk-width-medium-4-5 uk-push-1-10">
        <div data-uk-grid-margin class="uk-grid uk-margin-large-top">
            <div class="uk-width-large-1-2">
                <div class="uk-grid">
                    <div class="uk-width-1-5"><i class="uk-icon-lock uk-icon-large uk-icon-justify "></i></div>
                    <div class="uk-width-4-5">
                        <h6>IT-Sicherheits Reifegradanalyse</h6>
                        <p>Analyse der betrieblichen IT-Sicherheit und Erstellung eines Sicherheitskonzepts mit
                            entsprechenden Maßnahmen. </p>
                        <a href="#Sicherheit" data-uk-smooth-scroll="{offset: 100}"
                           class="uk-button uk-button-danger uk-button-small  ">Mehr
                            erfahren</a>
                    </div>
                </div>
            </div>
            <div class="uk-width-large-1-2">
                <div class="uk-grid">
                    <div class="uk-width-1-5"><i class="uk-icon-road uk-icon-large uk-icon-justify "></i></div>
                    <div class="uk-width-4-5">
                        <h6>IT-Strategie Beratung</h6>
                        <p> Ziel ist die ganzheitliche Digitalisierung Ihres Unternehmens durch eine ausgefeilte
                            Strategie und kooperative Begleitung durch uns. </p>
                        <a href="#Strategie" data-uk-smooth-scroll="{offset: 100}"
                           class="uk-button uk-button-danger uk-button-small  ">Mehr
                            erfahren</a>
                    </div>
                </div>
            </div>
        </div>

        <div data-uk-grid-margin class="uk-grid">

            <div class="uk-width-large-1-2">
                <div class="uk-grid">
                    <div class="uk-width-1-5"><i class="uk-icon-cubes uk-icon-large uk-icon-justify "></i></div>
                    <div class="uk-width-4-5">
                        <h6>Materialwirtschaft Lageroptimierung</h6>
                        <p>Wir helfen Ihnen dabei Ihr Lager zu optimieren um anschließend ein Lagerverwaltungssystem zu
                            implementieren, wie das Idalabs MaWi Modul.</p>
                        <a href="#Lager" data-uk-smooth-scroll="{offset: 100}"
                           class="uk-button uk-button-danger uk-button-small  ">Mehr
                            erfahren</a>
                    </div>
                </div>
            </div>

            <div class="uk-width-large-1-2">
                <div class="uk-grid">
                    <div class="uk-width-1-5"><i class="uk-icon-university uk-icon-large uk-icon-justify "></i></div>
                    <div class="uk-width-4-5">
                        <h6>Haus der Digitalisierung</h6>
                        <p>
                            Digitaler- Markt und Angebot erweitern die IT-Strategie zu einem
                            allumfassenden Konzept. Für die Umsetzung stehen wir Ihnen zur Verfügung.</p>
                        <a href="#Haus" data-uk-smooth-scroll="{offset: 100}"
                           class="uk-button uk-button-danger uk-button-small  ">Mehr
                            erfahren</a>
                    </div>
                </div>
            </div>
        </div>


    </div>
</section>

<!-- IT-Sicherheit  -->
<section id="Sicherheit" class="uk-container uk-container-center uk-margin-large-top uk-padding-remove ">
    <div class="uk-grid uk-grid-collapse sec has-shadow white-bg ">
        <div class="uk-width-large-3-4 uk-panel uk-panel-box ">
            <div class="uk-panel-space">
                <h2 class="text-ida-red ">IT-Sicherheits Reifegradanalyse</h2>
                <p> Durch Sicherheitslücken können Angriffe von Außen starke Schäden verursachen. Datenverlust,
                    Informationsdiebstahl oder unerlaubter Zugriff auf Bankkonten sind nur einige Risiken, die es zu
                    minimieren gilt. Ein weiteres Risiko sind Strafen, die durch Missachtung der
                    Datenschutz-Grundverordnung drohen. Im Rahmen unserer IT-Sicherheits Reifegradanalyse wird Ihr
                    Sicherheitskonzept auf Herz und Nieren geprüft und entsprechend angepasst oder neu erstellt. </p>
            </div>
        </div>

        <div class="uk-width-large-1-4 uk-visible-large uk-panel uk-panel-box">
            <div class="uk-panel-box uk-panel-space ">
                <img src="/produkte/Grafiken/lock.png" class="uk-align-right" width="170px" alt="IDALABS Schloss">
            </div>
        </div>

        <!-- Sicherheitsziele -->
        <h2 class="uk-align-center">Sicherheitsziele</h2>
        <div class="uk-width-large 2-4 uk-panel uk-panel-box"
        >
            <div class="uk-panel uk-align-center">

                <!-- Box 1 -->
                <div class="uk-panel-space ">
                    <div class="uk-grid  uk-flex">

                        <!-- Box 1 -->
                        <div class="uk-width-large-1-3 ">
                            <div class="uk-panel uk-panel-box uk-padding-remove uk-flex uk-flex-center has-shadow ">
                                <div class="uk-panel uk-panel-space">
                                    <div class="uk-panel uk-text-center ">
                                        <h3 class="uk-text-center text-ida-red ">Vertraulichkeit</h3>
                                        <p class="uk-text-large uk-text-center">Zugriffsmanagement und
                                            Verschlüsselung: Nur berechtigte Personen sollen Daten einsehen und nutzen
                                            dürfen.
                                        </p>
                                        <h3 class="uk-text-center"> Maßnahmen:</h3>
                                        <p>Passwortsicherheit</p>
                                        <p>Firewall</p>
                                        <p>2 Faktor Authentifizierung</p>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <!-- Box 2 -->
                        <div class="uk-width-large-1-3  ">
                            <div class="uk-panel uk-panel-box  uk-padding-remove uk-flex uk-flex-center has-shadow ">
                                <div class="uk-panel uk-panel-space">
                                    <div class="uk-panel uk-text-center ">
                                        <h3 class=" uk-text-center text-ida-red">Verfügbarkeit</h3>
                                        <p class="uk-text-large uk-text-center "> Betriebsstabilität und
                                            Ausfallsicherheit: Nur Daten und Dienste, die auch funktionieren, sind
                                            sicher
                                            und zuverlässig.
                                        </p>

                                        <h3 class="uk-text-left uk-text-center "> Maßnahmen:</h3>
                                        <p>Backup Daten</p>
                                        <p>Cloudservice</p>
                                        <p>Stabile Systeme</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Box 3 -->
                        <div class="uk-width-large-1-3 ">
                            <div class="uk-panel uk-panel-box  uk-padding-remove uk-flex uk-flex-bottom uk-flex-center has-shadow">
                                <div class="uk-panel uk-panel-space">
                                    <div class="uk-panel uk-text-center ">
                                        <h3 class="uk-text-center text-ida-red">Integrität</h3>
                                        <p class="uk-text-large uk-text-center "> Transparenz und
                                            Nachvollziehbarkeit: Es darf nicht möglich sein, Daten unbemerkt bzw.
                                            unerkannt zu verändern.
                                        </p>

                                        <h3 class="uk-text-center  "> Maßnahmen:</h3>
                                        <p>Gefährdungsanalyse</p>
                                        <p>Sicherheitskonzept</p>
                                        <p>Mitarbeiterschulung</p>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</section>

<!-- It-Strategie -->
<section id="Strategie" class="uk-container uk-container-center uk-margin-large-top uk-padding-remove ">
    <div class="uk-grid uk-grid-collapse sec has-shadow white-bg ">

        <div class="uk-width-large-3-4 uk-panel uk-panel-box ">
            <div class="uk-panel-space">
                <h2 class="text-ida-red">IT-Strategie Beratung</h2>
                <p>

                    Wir erleben gegenwärtig einen massiven Wandel, der durch den Fachkräftemangel bedingt ist, die immer
                    schneller voranschreitende Globalisierung aber insbesondere auch durch die Digitalisierung unseres
                    Arbeitsalltages und die damit einhergehenden Prozesse.

                    Zukunftsorientierte Handwerker stellen sich zunehmend erfolgreich auf diesen Wandel ein. Dabei ist
                    die Ausgangslage jedes einzelnen Betriebes unterschiedlich zu betrachten. Vorhandene
                    IT-Infrastruktur, IT-Affinität bei der Belegschaft und Ziele der digitalen Maßnahmen. Diese
                    Gegebenheiten werden von uns
                    analysiert, um Ihnen spezifischen Potenziale zur Optimierung Ihrer Prozesse aufzuzeigen.

                    Gerne bieten wir Ihnen an, Sie in einer agilen, vertrauensvollen und partnerschaftlichen Kooperation
                    auf dem Weg der Digitalisierung zu beraten und zu begleiten.

                </p>
            </div>
        </div>
        <div class="uk-width-large-1-4 uk-visible-large uk-panel uk-panel-box  ">
            <div class="uk-panel-space">
                <img src="/produkte/Grafiken/street.png " class="uk-align-right" width="170px" alt="IDALABS MaWi">
            </div>
        </div>


        <div class="uk-width-large-3-4 uk-panel uk-panel-box uk-align-center">
            <div class="uk-panel-space">
                <img src="/produkte/Grafiken/strategieberatung.png" alt="IDALABS Strategie">
            </div>

            <div class="uk-panel uk-panel-box uk-panel-space">
                <ul data-uk-tab="" data-uk-switcher="{connect:'#tabbed-content', animation: 'uk-animation-fade'}"
                    class="uk-tab uk-tab-grid uk-width-medium-2-3 uk-push-1-6 uk-margin-large-top">
                    <li class="uk-width-1-4 uk-active" aria-expanded="true"><a href="#">Einstieg</a>
                    </li>
                    <li class="uk-width-1-4" aria-expanded="false"><a href="#">Analyse</a></li>
                    <li class="uk-width-1-4" aria-expanded="false"><a href="#">Entwicklung</a></li>
                    <li class="uk-width-1-4" aria-expanded="false"><a href="#">Plan</a></li>
                    <li class="uk-tab-responsive uk-hidden" aria-haspopup="true" aria-expanded="false"><a>Mehr
                            Zeit</a>
                        <div class="uk-dropdown uk-dropdown-small">
                            <ul class="uk-nav uk-nav-dropdown"></ul>
                            <div></div>
                        </div>
                    </li>
                </ul>

                <ul id="tabbed-content" class="uk-switcher uk-width-medium-4-5 uk-push-1-10 uk-margin-large-top">
                    <li aria-hidden="false" class="uk-active" style="animation-duration: 200ms;">
                        <div class="uk-panel">
                            <div data-uk-grid-margin="" class="uk-grid uk-margin-top">
                                <div class="uk-width-large-1-1 uk-text-center">
                                    <p>An drei Werktagen widmen wir Ihnen unsere vollständige Aufmerksamkeit. Dabei
                                        begleiten wir Sie durch Ihren Betrieb und erfassen die aktuellen Prozessabläufe
                                        der verschiedenen Abteilungen.
                                        Nach unserer Bestandsanalyse arbeiten wir mit Ihnen in einem Workshop ihre
                                        genauen Ziele heraus und wie hierbei die Digitalisierung dabei eine führende
                                        Rolle spielen kann.
                                    </p>
                                </div>

                            </div>
                        </div>
                    </li>
                    <li aria-hidden="true" class="" style="animation-duration: 200ms;">
                        <div class="uk-panel">
                            <div data-uk-grid-margin="" class="uk-grid uk-margin-top">
                                <div class="uk-width-large-2-2 uk-text-center">
                                    <p>Zur Ermittlung des allgemeinen Zustandes Ihrer Infrastruktur und Ihrer
                                        Prozessabläufe schauen wir Ihren Mitarbeitenden über
                                        die Schulter. Nach unserer Bestandsaufnahme analysieren wir mit Ihnen gemeinsam
                                        die einzelnen Prozesse. </p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li aria-hidden="true" class="" style="animation-duration: 200ms;">
                        <div class="uk-panel">
                            <div data-uk-grid-margin="" class="uk-grid uk-margin-top">
                                <div class="uk-width-large-2-2 uk-text-center">
                                    <p>Wir entwickeln für Sie eine Strategie zur Verfolgung Ihrer Ziele, sowie einen
                                        Maßnahmen- und Ablaufplan.</p>
                                </div>


                            </div>
                        </div>
                    </li>
                    <li aria-hidden="true" class="" style="animation-duration: 200ms;">
                        <div class="uk-panel">
                            <div data-uk-grid-margin="" class="uk-grid uk-margin-top">
                                <div class="uk-width-large-1-1 uk-text-center">
                                    <p>Die Ergebnisse unserer Strategieentwicklung und unseres Maßnahmenplans werden am
                                        dritten Tag präsentiert und eine individuelle Handlungsempfehlung ausgesprochen.
                                        Zum Schluss erhalten Sie von uns einen detaillierten Ablaufplan zur Umsetzung
                                        der Handlungsempfehlung. Die Präsentation dauert ca. 2 Stunden an und kann
                                        online oder vor Ort erfolgen. </p>
                                </div>

                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

    </div>
</section>

<!-- Materialwirtschaft -->
<section id="Lager" class="uk-container uk-container-center uk-margin-large-top uk-padding-remove ">
    <div class="uk-grid uk-grid-collapse sec has-shadow white-bg ">

        <div class="uk-width-large-3-4 uk-panel uk-panel-box">
            <div class="uk-panel-space">
                <h2 class="text-ida-red">Materialwirtschaft Lageroptimierung</h2>
                <p>
                    Gerade in Handwerksbetrieben, die schon über mehrere Generationen hinweg gewachsen sind, werden
                    Lagerprozesse oft nicht regelmäßig überprüft und angepasst. Das hat zur Folge, dass schnell
                    Probleme,
                    wie nicht bestellte Artikel, überflüssige Artikel oder nicht auffindbare Artikel, an der
                    Tagesordnung
                    stehen. Weitere Probleme sind ein händischer Bestellprozess oder ein grundsätzlicher Mangel an
                    Platz.
                    Hier kommen wir ins Spiel. Wir bieten Ihnen eine Lageroptimierung, die Ihnen hohe Kostenersparnis
                    und starke Arbeitsentlastung bringt.
                    Ihr Lager wird anschließend effizienter, nachhaltiger und
                    zukunftssicher sein.

                </p>
            </div>
        </div>


        <div class="uk-width-large-1-4 uk-visible-large uk-panel uk-panel-box ">
            <div class="uk-panel-space">
                <img src="/produkte/Grafiken/cube.png" class="uk-align-right" width="170px" alt="IDALABS MaWi">
            </div>
        </div>


        <div class="uk-width-large 2-4 uk-panel uk-panel-box uk-align-center ">
            <div class="uk-panel ">

                <div class="uk-panel-space uk-padding-top-remove  ">
                    <div class="uk-grid  ">

                        <!-- Box 1 -->
                        <div class="uk-width-large-1-3 ">
                            <div class="uk-panel uk-panel-box  uk-flex uk-flex-center  ">
                                <div class="uk-panel uk-panel-space uk-panel-box-primary uk-padding-remove-left">
                                    <div class="uk-panel uk-text-center ">
                                        <h3 class="uk-text-center uk-text-contrast uk-text-bold ">Hohe Einsparung</h3>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Box 2 -->
                        <div class="uk-width-large-1-3 ">
                            <div class="uk-panel uk-panel-box  uk-flex uk-flex-center  ">
                                <div class="uk-panel uk-panel-space uk-panel-box-primary uk-padding-remove-left">
                                    <div class="uk-panel uk-text-center ">
                                        <h3 class="uk-text-center uk-text-contrast uk-text-bold ">Mehr
                                            Produktivität</h3>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Box 3 -->
                        <div class="uk-width-large-1-3 ">
                            <div class="uk-panel uk-panel-box  uk-flex uk-flex-center  ">
                                <div class="uk-panel uk-panel-space uk-panel-box-primary uk-padding-remove-left">
                                    <div class="uk-panel uk-text-center ">
                                        <h3 class="uk-text-center uk-text-contrast uk-text-bold ">Höherer Gewinn</h3>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

            </div>
        </div>


        <div class="uk-width-large-3-4 uk-panel uk-panel-box uk-align-center">
            <div class="uk-panel-space">
                <img src="/produkte/Grafiken/lageroptimierung.png" alt="IDALABS Strategie">
            </div>

            <div class="uk-panel uk-panel-box uk-panel-space">
                <ul data-uk-tab="" data-uk-switcher="{connect:'#tabbed-content2', animation: 'uk-animation-fade'}"
                    class="uk-tab uk-tab-grid uk-width-medium-2-3 uk-push-1-6 uk-margin-large-top">
                    <li class="uk-width-1-3 uk-active" aria-expanded="true"><a href="#">Lageranalyse</a>
                    </li>
                    <li class="uk-width-1-3" aria-expanded="false"><a href="#">Grundkonzept</a></li>
                    <li class="uk-width-1-3" aria-expanded="false"><a href="#">System</a></li>
                    <li class="uk-tab-responsive uk-hidden" aria-haspopup="true" aria-expanded="false"><a>Mehr
                            Zeit</a>
                        <div class="uk-dropdown uk-dropdown-small">
                            <ul class="uk-nav uk-nav-dropdown"></ul>
                            <div></div>
                        </div>
                    </li>
                </ul>

                <ul id="tabbed-content2" class="uk-switcher uk-width-medium-4-5 uk-push-1-10 uk-margin-large-top">
                    <li aria-hidden="false" class="uk-active" style="animation-duration: 200ms;">
                        <div class="uk-panel">
                            <div data-uk-grid-margin="" class="uk-grid uk-margin-top">
                                <div class="uk-width-large-1-1 uk-text-center">
                                    <p> Wir analysiern Ihr Lager vorort und extrahieren alle vorhandenen Probleme.
                                        Anschließend leiten wir aus der Problemanalyse einen Katalog an Zielen ab. So
                                        könnte das Ergebnis des ersten Schrittes aussehen:
                                    </p>
                                    <div class="uk-grid">

                                        <div class=" uk-width-1-2">
                                            <div class="uk-panel uk-panel-box has-shadow">
                                                <h4 class="text-ida-red">Probleme</h4>
                                                <p>Platzprobleme</p>
                                                <p>Händischer Bestellprozess </p>
                                                <p>Lange Artikelsuche </p>
                                                <p>Unauffindbare Artikel</p>
                                                <p>Überflüssige Bestellungen </p>
                                                <p>Händische Protokollierung </p>
                                            </div>
                                        </div>

                                        <div class=" uk-width-1-2">
                                            <div class="uk-panel uk-panel-box has-shadow">
                                                <h4 class="text-ida-red">Ziele</h4>
                                                <p>Flächennutzungsgrad optimiert</p>
                                                <p>Zeitreduktion Suche </p>
                                                <p>Pull Prinzip / Kanban</p>
                                                <p>Optimale Bestellmenge</p>
                                                <p>Minimaler Lagerbestand </p>
                                                <p>Automatisierung </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li aria-hidden="true" class="" style="animation-duration: 200ms;">
                        <div class="uk-panel">
                            <div data-uk-grid-margin="" class="uk-grid uk-margin-top">
                                <div class="uk-width-large-2-2 uk-text-center">
                                    <p> Aus den Ergebnissen der Analyse leiten wir Maßnahmen ab, die in einem Lager
                                        Grundkonzept zusammengefasst werden. Zum Beispiel: </p>

                                    <div class="uk-grid"></div>

                                    <div class=" uk-width-1-2 uk-align-center ">
                                        <div class="uk-panel uk-panel-box has-shadow">
                                            <h4 class="text-ida-red">Maßnahmen</h4>
                                            <p>Lager von unnötigen Artikeln befreien</p>
                                            <p>Lagerlayout verändern </p>
                                            <p>Einführung Lagerverwaltungssystem </p>
                                            <p>Automatisieren händischer Vorgänge</p>
                                            <p>Einführung Kanban Prinzip </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li aria-hidden="true" class="" style="animation-duration: 200ms;">
                        <div class="uk-panel">
                            <div data-uk-grid-margin="" class="uk-grid uk-margin-top">
                                <div class="uk-width-large-2-2 uk-text-center">
                                    <p>Letztendlich mündet die Analyse und die Erstellung eines Konzeptes in der
                                        Einführung eines Lagerverwaltungssystems. Wir bieten Ihnen dafür unser MaWi
                                        Modul an. Eine beispielhafte Einführung wäre: <a href="/produkte/projekte.php"
                                        ">MaWi in einer Woche</p>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>

        </div>


    </div>
</section>

<!-- Haus der Digitalisierung -->
<section id="Haus" class="uk-container uk-container-center uk-margin-large-top uk-padding-remove ">
    <div class="uk-grid uk-grid-collapse sec has-shadow white-bg ">

        <div class="uk-width-large-3-4 uk-panel uk-panel-box">
            <div class="uk-panel-space">
                <h2 class="text-ida-red">Das Haus der Digitalisierung</h2>
                <p>
                    Sprichwörtlich handelt es sich hier um ein ganzes Haus voller Konzepte und Methoden die im Folgenden
                    dazu dienen
                    Ihr Unternehmen vollumfänglich zu digitaliseren. Hier geht es nicht nur um die
                    Geschäftsprozesse oder die unternehmensinterne IT, sondern auch um die digitale Repräsentation des
                    Unternehmens sowie die digitale Anpassung des Angebots. Ziele sind die signifikante Steigerung der
                    Wettbewerbsfähigkeit durch erhöhte Kundenbindung bzw. Kundenzufriedenheit und erhebliche
                    Kostensenkung.
                    Im Folgenden sehen Sie alle Ansatzpunkte des Konzepts. Wir stehen Ihnen hier beratendend zur Seite und
                    unterstützen Sie gerne
                    dabei Ihr Unternehmen komplett zu digitalisieren.

                </p>
            </div>
        </div>
        <div class="uk-width-large-1-4 uk-visible-large uk-panel uk-panel-box ">
            <div class="uk-panel-space">
                <img src="/produkte/Grafiken/uni.png" class="uk-align-right" width="170px" alt="IDALABS MaWi">
            </div>
        </div>


        <div class="uk-width-large-4-4 uk-panel uk-panel-box">
            <div class="uk-panel-space">

                <div class="uk-panel uk-panel-box  uk-align-center">

                    <img src="/produkte/Grafiken/hausdigital.png" alt="IDALABS Haus der Digitalsierung">
                </div>

            </div>
        </div>


    </div>
</section>


<?php include("../_templates/kontakt.inc.php"); ?>
<?php include("../_templates/footer.inc.php"); ?>
