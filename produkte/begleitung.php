<?php
$pageTitle = "Begleitung";
$metaKeywords = "Cloud Service, Lieferantenanbindung, OpenTrans, EDI";

include("../_templates/header.inc.php");
?>
    <section class="uk-container uk-container-center uk-margin-large-top uk-padding-vertical-remove">
        <h1 class="uk-heading-large uk-text-center"><?php echo $pageTitle ?></h1>
        </div>
    </section>


    <!-- Projekte Box -->
    <section class="uk-container uk-container-center uk-margin-large-top uk-padding-remove ">
        <div class="uk-grid uk-grid-collapse sec has-shadow white-bg ">
            <div class="uk-width-large-1-2 uk-position- uk-panel uk-panel-box">
                <div style="padding-bottom: 0px!important;"
                     class="uk-panel uk-panel-box  uk-flex uk-flex-middle uk-flex-center uk-padding-remove">
                    <div class="uk-panel-space">
                        <h1>Ambassador - Service </h1>
                        <p>Dieser Service ist aktiv auf Deinen Erfolg mit unseren Lösungen ausgerichtet. Der Ambassador
                            ist Dein persönlicher Ansprechpartner. Seine Aufgabe ist es
                            nicht nur, Dich engmaschig darüber zu informieren, welche Entwicklungen in der Software
                            zur Anwendung bringen, für Dich relevant sind, sondern
                            er entwickelt auch fortlaufend individuelle Pläne, wie die digitalen
                            Geschäftsprozesse besser in Deinem Unternehmen und der Denkweise Deiner Mitarbeiter
                            verankert werden können. Nicht zuletzt wird er so zum Botschafter unseres Unternehmens in Deinem
                            Unternehmen – und vertritt umgekehrt stets mit dem Fokus auf Deinen Erfolg auch mit starker
                            Stimme Deine Interessen in unserem Unternehmen.
                        </p>


                    </div>
                </div>

            </div>
            <div class="uk-width-large-1-2">
                <div class="uk-panel uk-panel-box uk-panel-box-primary uk-flex uk-flex-middle uk-flex-center uk-padding-remove">
                    <div class="uk-position-relative uk-visible-large">
                        <img src="/produkte/Grafiken/ambassador.jpg" alt="IDALABS Porojekte">
                    </div>
                </div>
            </div>
        </div>
    </section>




<?php include("../_templates/kontakt.inc.php"); ?>
<?php include("../_templates/footer.inc.php");
