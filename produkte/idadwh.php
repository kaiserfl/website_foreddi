<?php
$pageTitle = "IDA Datawarehouse";
$metaKeywords = "Datawarehouse, Lieferantenanbindung, OpenTrans, EDI";
include("../_templates/header.inc.php");
?>

<section class="uk-container uk-container-center uk-margin-large-top uk-padding-vertical-remove">
    <h1 class="uk-heading-large uk-text-center"><?php echo $pageTitle; ?></h1>
    </div>
</section>

<!-- Projekte Box -->
<section class="uk-container uk-container-center uk-margin-large-top uk-padding-remove ">
    <div class="uk-grid uk-grid-collapse sec has-shadow white-bg ">
        <div class="uk-width-large-1-2 uk-position- uk-panel uk-panel-box">
            <div style="padding-bottom: 0px!important;"
                 class="uk-panel uk-panel-box  uk-flex uk-flex-middle uk-flex-center uk-padding-remove">
                <div class="uk-panel-space">
                    <h1>Anbinden</h1>
                    <p> Das <strong> IDA Datawarehouse</strong> bindet deine Lieferanten an. Um das Material für einen
                        Auftrag zu kommissionieren, sind Artikel
                        verschiedener Lieferanten in der IDA zu finden. Anstatt sich dazu in mehreren Webshops anzumelden,
                        diverse Telefonate zu führen und dicke Artikelkataloge zu wälzen, kannst du die
                        Warenwirtschaft auch einfach mit dem IDADWH verbinden und die Bestellungen
                        lieferantenübergreifend elektronisch aufgeben.
                    </p></p>
                    </p>
                </div>
            </div>

        </div>
        <div class="uk-width-large-1-2">
            <div class="uk-panel uk-panel-box uk-panel-box-primary uk-flex uk-flex-middle uk-flex-center uk-padding-remove">
                <div class="uk-position-relative uk-visible-large">
                    <img src="/produkte/Grafiken/anbinden.jpeg" alt="IDALABS Porojekte">
                </div>

            </div>
        </div>
    </div>
</section>

<!-- Elektronische Beschaffung-->
<section id="Haus" class="uk-container uk-container-center uk-margin-large-top uk-padding-remove ">
    <div class="uk-grid uk-grid-collapse sec has-shadow white-bg ">
        <div class="uk-width-large-3-4 uk-panel uk-panel-box">
            <div class="uk-panel-space">
                <h2 class="text-ida-red">Digitale Lieferantenanbindung </h2>
                <p>Jeder Lieferant muss nur einmal mit dem IDADWH über Konnektoren verbunden werden,
                    um ein Teil der digitalen Wertschöpfungskette aller Betriebe, die IDA nutzen, zu werden. Im
                    Umkehrschluss erhält jeder IDA-Nutzer direkten Zugriff auf Lieferantenkataloge mit individuellen
                    Preisinformationen. So kannst du mit der IDA auf direktem Weg, voll digital, bestellen.
                    Neben dem Austausch von Artikelstammdaten und der digitalen Bestellübermittlung werden über das DWH
                    z.B. auch Auftragsbestätigungen, Lieferscheine oder Rechnungen voll digital bereitgestellt.
                </p>
            </div>
        </div>
        <div class="uk-width-large-4-4 uk-panel uk-panel-box">
            <div class="uk-panel-space uk-padding-top-remove">
                <div class="uk-panel uk-panel-box uk-padding-top-remove  uk-align-center">
                    <img src="/produkte/Grafiken/dwh_prozess.png" alt="IDALABS DWH" id="img" onclick="swipe(id)">
                </div>
            </div>
        </div>
    </div>
</section>

<!-- IDA Ökosystem-->
<section id="Haus" class="uk-container uk-container-center uk-margin-large-top uk-padding-remove ">
    <div class="uk-grid uk-grid-collapse sec has-shadow white-bg ">
        <div class="uk-width-large-3-4 uk-panel uk-panel-box">
            <div class="uk-panel-space">
                <h2 class="text-ida-red">IDA Ökosystem </h2>
                <p>Das IDADWH ist aber nicht nur ein
                    Bestellmedium, mit dem Sie Ihre Arbeitsvorbereitung spürbar entlasten es stellt
                    auch Artikelstammdaten bereit, mit denen Sie ihre Warenwirtschaftssystem speisen können.
                    Das IDADWH beschleunigt ihre Bestellprozesse und bringt Transparenz in die Angebote Ihrer
                    Lieferanten.</p>
                <div class="uk-panel">
                    <p>Ein paar Beispielanbindungen</p>
                    <ul class="uk-list-space">
                        <li>Meesenburg</li>
                        <li>Profine</li>
                        <li>Schüco</li>
                        <li>Würth</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="uk-width-large-1-4 uk-visible-large uk-panel uk-panel-box ">
            <div class="uk-panel-space">
                <img src="//www.idalabs.de/assets/img/idadwh.png" width="170">
            </div>
        </div>
        <div class="uk-width-large-4-4 uk-panel uk-panel-box">
            <div class="uk-panel-space uk-padding-top-remove">
                <div class="uk-panel uk-panel-box uk-padding-top-remove  uk-align-center">
                    <img src="/produkte/Grafiken/uebersicht_anbinden.png" alt="IDALABS IDADWH" id="img2" onclick="swipe(id)">
                </div>
            </div>
        </div>
    </div>
</section>




<?php include("../_templates/kontakt.inc.php"); ?>
<?php include("../_templates/footer.inc.php");
