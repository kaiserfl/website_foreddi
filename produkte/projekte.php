<?php
$pageTitle    = "Projekte";
$metaKeywords = "Softwareeinführung, Prozessverbesserungen, Lieferantenanbindungen, Materialwirtschaft, Einführung Materialwirtschaft, Einführung ERP";
include("../_templates/header.inc.php");
?>

   <!-- Überschrift  -->
   <section class="uk-container uk-container-center uk-margin-large-top uk-padding-vertical-remove">
      <div class="uk-panel uk-space uk-text-center">
         <h1 class="uk-heading-large"><?php echo $pageTitle ?></h1>
      </div>
   </section>

   <!-- Projekte Box -->
   <section class="uk-container uk-container-center uk-margin-large-top uk-padding-remove ">
      <div class="uk-grid uk-grid-collapse sec has-shadow white-bg ">
         <div class="uk-width-large-1-2 uk-position- uk-panel uk-panel-box">
            <div style="padding-bottom: 0px!important;"
                 class="uk-panel uk-panel-box  uk-flex uk-flex-middle uk-flex-center uk-padding-remove">
               <div class="uk-panel-space">
                  <h1>Projekte</h1>
                  <p>Die Einführung neuer Software stellt häufig ein großes Problem für Handwerksbetriebe dar. Das
                     gilt insbesondere für ERP-Software, die tief in die Unternehmensprozesse eingreift und
                     Veränderungen in den Arbeitsweisen der Mitarbeitenden nach sich zieht.
                     Um die angespannte Personalsituation unserer Kunden nicht zu belasten und keine
                     unnötigen Kosten zu verursachen, haben wir standardisierte Vorgehensmodelle für die
                     Einführung unserer Software entwickelt.
                  </p>
               </div>
            </div>

         </div>
         <div class="uk-width-large-1-2">
            <div class="uk-panel uk-panel-box uk-panel-box-primary uk-flex uk-flex-middle uk-flex-center uk-padding-remove">
               <div class="uk-position-relative">
                  <img src="/assets/img/Projekte.jpg" alt="IDALABS Porojekte">
               </div>

            </div>
         </div>
      </div>
   </section>

   <!-- Übersicht Mawi in, Klassische Einführung -->
   <section class="uk-container uk-container-center uk-margin-large-top uk-padding-vertical-remove">
      <div class="uk-panel uk-space ">
         <div class="uk-grid uk-grid-collapse has-shadow uk-flex">

            <!-- Box 1 -->
            <div class="uk-width-large-1-3 ">
               <div class="uk-panel uk-panel-box uk-panel-box-secondary uk-padding-remove uk-flex uk-flex-center ">
                  <div class="uk-panel uk-panel-space">
                     <div class="uk-panel uk-text-center ">
                        <h3 class="uk-text-center uk-contrast">Klassische Einführung</h3>
                        <p class="uk-text-large uk-text-center uk-contrast">Vollständige und ausführliche
                           Einführung unserer Software
                           von Kickoff bis Support</p>
                        <a href="#ERP" data-uk-smooth-scroll="{offset: 100}" class="uk-button uk-margin-top ">Mehr
                           erfahren</a>
                     </div>
                  </div>
               </div>
            </div>


            <!-- Box 2 -->
            <div class="uk-width-large-1-3  ">
               <div class="uk-panel uk-panel-box uk-panel-box-primary uk-padding-remove uk-flex uk-flex-center ">
                  <div class="uk-panel uk-panel-space">
                     <div class="uk-panel uk-text-center ">
                        <h3 class=" uk-text-center uk-contrast">MaWi in einer Woche</h3>
                        <p class="uk-text-large uk-text-center uk-contrast"> Kompakte Einführung des
                           Warenwirtschaftsmoduls MaWi innerhalb einer Woche</p>
                        <a href="#Mawi" data-uk-smooth-scroll="{offset: 100}" class="uk-button uk-margin-top ">Mehr
                           erfahren</a>
                     </div>
                  </div>
               </div>
            </div>

            <!-- Box 3 -->
            <div class="uk-width-large-1-3 ">
               <div class="uk-panel uk-panel-box uk-panel-box-secondary uk-padding-remove uk-flex uk-flex-bottom uk-flex-center   ">
                  <div class="uk-panel uk-panel-space">
                     <div class="uk-panel uk-text-center ">
                        <h3 class="uk-text-center uk-contrast">Beratung</h3>
                        <p class="uk-text-large uk-text-center uk-contrast">Beratung und Dienstleistungen rund
                           um das Thema Prozesse und Digitalisierung </p>
                        <a href="#Lean" data-uk-smooth-scroll="{offset: 100}" class="uk-button uk-margin-top ">Mehr
                           erfahren</a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>

   </section>

   <!-- Klassisches Einführungsprojekt -->
   <section id="ERP" class="uk-container uk-container-center uk-margin-large-top uk-padding-remove ">
      <div class="uk-grid uk-grid-collapse sec has-shadow white-bg ">

         <div class="uk-width-large-3-4 uk-panel uk-panel-box">
            <div class="uk-panel-space">
               <h2 class="text-ida-red">Klassische Einführung</h2>
               <p>
                  Es handelt sich bei diesem Projekt um eine voll umfassende Einführung der IDA. Ziel ist es die
                  Anforderungen des Betriebes genau zu erfassen und so eine perfekt abgestimmte Software zu
                  etablieren.
                  Das Komplettprogramm beinhaltet auch die anschließende Betreuung nach der Einführung.
                  Der gesamte Vorgang umfasst sechs Phasen die nachfolgend detailliert aufgeschlüsselt sind.


               </p>
            </div>
         </div>
         <div class="uk-width-large-1-4 uk-visible-large uk-panel uk-panel-box uk-flex uk-flex-center uk-flex-middle">
            <div class="uk-panel-space">
               <img src="/assets/img/idaservicecloud.png " width="180px" alt="IDALABS MaWi">
            </div>
         </div>

         <div class="uk-width-large-3-3 uk-panel uk-panel-box">
            <div class="uk-panel-space">
               <img src="/produkte/Grafiken/einfuehrung.png" alt="IDALABS Porojekte">
            </div>

            <div class="uk-panel uk-panel-box uk-panel-space">
               <ul data-uk-tab="" data-uk-switcher="{connect:'#tabbed-content', animation: 'uk-animation-fade'}"
                   class="uk-tab uk-tab-grid uk-width-medium-3-3 uk-push-1-8 uk-margin-large-top">
                  <li class="uk-width-1-6 uk-active" aria-expanded="true"><a href="#">Einstieg & Analyse</a>
                  </li>
                  <li class="uk-width-1-6" aria-expanded="false"><a href="#">Beratung</a></li>
                  <li class="uk-width-1-6" aria-expanded="false"><a href="#">Anpassung & Import</a></li>
                  <li class="uk-width-1-6" aria-expanded="false"><a href="#">Schulung</a></li>
                  <li class="uk-width-1-6" aria-expanded="false"><a href="#">Start</a></li>
                  <li class="uk-width-1-6" aria-expanded="false"><a href="#">Service & Hilfe</a></li>
                  <li class="uk-tab-responsive uk-hidden" aria-haspopup="true" aria-expanded="false"><a>Mehr
                        Zeit</a>
                     <div class="uk-dropdown uk-dropdown-small">
                        <ul class="uk-nav uk-nav-dropdown"></ul>
                        <div></div>
                     </div>
                  </li>
               </ul>
               <ul id="tabbed-content" class="uk-switcher uk-width-medium-4-5 uk-push-1-10 uk-margin-large-top">
                  <li aria-hidden="false" class="uk-active" style="animation-duration: 200ms;">
                     <div class="uk-panel">
                        <div data-uk-grid-margin="" class="uk-grid uk-margin-top">
                           <div class="uk-width-large-1-2 uk-row-first" style="">
                              <h6>Einstieg</h6>
                              <p>Nach Vertragsschluss vereinbaren wir einen Termin für einen Einstieg, während
                                 dessen
                                 wir mit den Mitarbeitenden, die mit der Software arbeiten sollen, über unser
                                 Produkt, das vor uns liegende Projekt, aber auch offen über Erwartungen,
                                 Vorbehalte und Wünsche sprechen.</p>
                           </div>
                           <div class="uk-width-large-1-2" style="">
                              <h6>Analyse</h6>
                              <p> Hier kristallisieren sich Fähigkeitslücken, etwa im Bereich der Prozesse,
                                 heraus, die wir im zweiten Schritt gezielt mit Beratung und
                                 Prozessverbesserungen aufarbeiten.</p>
                           </div>
                        </div>
                     </div>
                  </li>
                  <li aria-hidden="true" class="" style="animation-duration: 200ms;">
                     <div class="uk-panel">
                        <div data-uk-grid-margin="" class="uk-grid uk-margin-top">
                           <div class="uk-width-large-1-2" style="">
                              <h6>Beratung</h6>
                              <p>Wir bringen Struktur und Transparenz in die Vertriebs- und Vergabe-Prozesse
                                 und ermöglichen es
                                 dadurch schneller und passgenauer auf Ausschreibungen und Anfragen zu
                                 reagieren.</p>
                           </div>
                           <div class="uk-width-large-1-2" style="">
                              <h6>Prozessverbesserungen</h6>
                              <p>Die Digitalisierung sorgt für weniger Verschwendung, verkürzte Prozesse, eine
                                 geringere
                                 Fehlerquote und weniger Abstimmungsaufwände. Das spart am Ende bares Geld.
                              </p></div>
                        </div>
                     </div>
                  </li>
                  <li aria-hidden="true" class="" style="animation-duration: 200ms;">
                     <div class="uk-panel">
                        <div data-uk-grid-margin="" class="uk-grid uk-margin-top">
                           <div class="uk-width-large-1-4">
                              <h6>Anpassung</h6>
                              <p>Konfiguration und Anpassung der Software nach Kundenwünschen</p>
                           </div>
                           <div class="uk-width-large-1-4">
                              <h6>Import</h6>
                              <p>Import von Bestands-Daten z.B. Kontaktdaten, Kalkulationsdaten etc.</p>
                           </div>
                           <div class="uk-width-large-1-4">
                              <h6>Lieferantenanbindung</h6>
                              <p>Wir binden die Lieferanten des Kunden an die Warenwirtschaft
                                 MaWi an.</p>
                           </div>
                           <div class="uk-width-large-1-4">
                              <h6>Integration</h6>
                              <p>Integration von Drittanbieter-Software.</p>
                           </div>

                        </div>
                     </div>
                  </li>
                  <li aria-hidden="true" class="" style="animation-duration: 200ms;">
                     <div class="uk-panel">
                        <div data-uk-grid-margin="" class="uk-grid uk-margin-top">
                           <div class="uk-width-large-1-1 uk-text-center">
                              <h6>Schulung</h6>
                              <p>Wir erklären vorort durch theoretische Vorträge
                                 und praktische Übungen, den Umgang mit der neuen Software. Wir
                                 beantworten alle
                                 Fragen die bei der Schulung entstehen und bereiten die Mitarbeitenden so
                                 vor, dass sie die Software, direkt sicher verwenden können. </p>
                           </div>

                        </div>
                     </div>
                  </li>
                  <li aria-hidden="true" class="" style="animation-duration: 200ms;">
                     <div class="uk-panel">
                        <div data-uk-grid-margin="" class="uk-grid uk-margin-top">
                           <div class="uk-width-large-1-2">
                              <h6>Start</h6>
                              <p>Sobald die Software den betrieblichen Anforderungen entspricht, starten wir
                                 mit dem Betrieb. </p>
                           </div>
                           <div class="uk-width-large-1-2">
                              <h6>Einführung</h6>
                              <p>Die Software kann modulweise eingeführt werden, wobei dieser Prozess dann
                                 iterativ bzw. schrittweise durchlaufen wird.</p>
                           </div>
                        </div>
                     </div>
                  </li>
                  <li aria-hidden="true" class="" style="animation-duration: 200ms;">
                     <div class="uk-panel">
                        <div data-uk-grid-margin="" class="uk-grid uk-margin-top">
                           <div class="uk-width-large-1-2">
                              <h6>Service</h6>
                              <p> Wir unterstützen in einer „Hypercare“ Phase im direkten Anschluss an das
                                 Projekt
                                 die produktive Nutzung in der Frühphase.</p>
                           </div>
                           <div class="uk-width-large-1-2">
                              <h6>Hilfe</h6>
                              <p>Wir bieten vollumfängliche Hilfe und Unterstützung nach Einführung der
                                 IDA. </p>
                           </div>
                        </div>
                     </div>
                  </li>
               </ul>
            </div>
         </div>

      </div>
   </section>

   <!-- Mawi in einer Woche -->
   <section id="Mawi" class="uk-container uk-container-center uk-margin-large-top uk-padding-remove ">
      <div class="uk-grid uk-grid-collapse sec has-shadow white-bg ">
         <div class="uk-width-large-3-4 uk-panel uk-panel-box">
            <div class="uk-panel-space">
               <h2 class="text-ida-red">Mawi in einer Woche </h2>
               <p>Ein Sonderfall ist das Warenwirtschafts-Modul (MaWi) der IDA, welches wir gemeinsam mit
                  Lieferanten (Meesenburg, Kömmerling, Remmers) vertreiben und einführen wollen.
                  Die Lieferanten sind hier in erster Linie Nutznießer der Digitalisierung unserer Kunden, da der
                  Austausch von Artikelstammdaten vollautomatisch erfolgen kann, was eine digitale
                  Bestellübermittlung ermöglicht , sodass nicht länger per E-Mail, Fax oder Telefon von einem
                  Mitarbeitenden
                  verarbeitet werden müssen.
               </p>
            </div>
         </div>
         <div class="uk-width-large-1-4 uk-visible-large uk-panel uk-panel-box uk-flex uk-flex-center uk-flex-middle">
            <div class="uk-panel-space">
               <img src="/produkte/Grafiken/ERP.png " width="180px" alt="IDALABS MaWi">
            </div>
         </div>

         <div style="padding-top: 0px !important" class="uk-width-large-3-3 uk-panel uk-panel-box uk-panel-space">
            <img src="/produkte/Grafiken/mawiin.png" alt="IDALABS MaWi">
         </div>

      </div>
   </section>


   <!-- Beratung -->
   <section id="Lean" class="uk-container uk-container-center uk-margin-large-top uk-padding-remove ">
      <div class="uk-grid uk-grid-collapse sec has-shadow white-bg ">
         <div class="uk-width-large-3-4 uk-panel uk-panel-box">
            <div class="uk-panel-space">
               <h2 class="text-ida-red">Beratung</h2>
               <p> Wo steht mein Unternehmen in Sachen Digitalisierung ? Sind meine Prozesse effizient und wie ist
                  es um meine IT-Sicherheit bestellt ? Das sind nur einige Fragen die wir Ihnen im Rahmen unseres
                  Beratungsprojektes beantworten können. </p>

               <a href="/produkte/beratung.php" class="uk-button uk-button-danger uk-margin-top  ">Mehr
                  erfahren</a>
            </div>

         </div>
         <div class="uk-width-large-1-4 uk-visible-large uk-panel uk-panel-box">
            <div class="uk-panel-space">
               <img src="/assets/img/ida.png" alt="IDALABS Logo">

            </div>

         </div>
      </div>
   </section>


<?php include("../_templates/kontakt.inc.php"); ?>
<?php include("../_templates/footer.inc.php"); ?>
