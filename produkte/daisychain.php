<?php
$pageTitle = "IDA Daisychain";
$metaKeywords = "ERP Integration, Integration, Klaes, Datev, Schuecal, Fensterbau, Konstruktionssoftware";
include("../_templates/header.inc.php");
?>
<section class="uk-container uk-container-center uk-margin-large-top uk-padding-vertical-remove">
    <h1 class="uk-heading-large uk-text-center"><?php echo $pageTitle; ?></h1>


</section>

<!-- Projekte Box -->
<section class="uk-container uk-container-center uk-margin-large-top uk-padding-remove ">
    <div class="uk-grid uk-grid-collapse sec has-shadow white-bg ">
        <div class="uk-width-large-1-2 uk-position- uk-panel uk-panel-box">
            <div style="padding-bottom: 0px!important;"
                 class="uk-panel uk-panel-box  uk-flex uk-flex-middle uk-flex-center uk-padding-remove">
                <div class="uk-panel-space">
                    <h1>Integrieren</h1>
                    <p>Die <strong>Daisychain</strong> verknüpft die Softwareinseln in ihrem Unternehmen, indem sie den
                        Datenaustausch über Schnittstellen koordiniert und die Daten anpasst. So vermeidest du die
                        doppelte Sicherung von Daten und verhinderst verschiedene Bearbeitungsstände. Es wird alles
                        von einer einheitlichen Benutzeroberfläche gesteuert, ohne zwischen den verschiedenen Programmen
                        hin und her wechseln zu müssen.
                    </p>
                </div>
            </div>

        </div>
        <div class="uk-width-large-1-2">
            <div class="uk-panel uk-panel-box uk-panel-box-primary uk-flex uk-flex-middle uk-flex-center uk-padding-remove">
                <div class="uk-position-relative uk-visible-large">
                    <img src="/produkte/Grafiken/integrieren_teaser.jpeg" alt="IDALABS Porojekte">
                </div>

            </div>
        </div>
    </div>
</section>

<!-- Daisy Überblick-->
<section id="Haus" class="uk-container uk-container-center uk-margin-large-top uk-padding-remove ">
    <div class="uk-grid uk-grid-collapse sec has-shadow white-bg ">
        <div class="uk-width-large-3-4 uk-panel uk-panel-box">
            <div class="uk-panel-space">
                <h2 class="text-ida-red">Anbindung verschiedener Softwareprodukte </h2>
                <p> Die Daisy stellt die Basis unserer Softwareintegration dar. Vernetzung der im Unternehmen
                    vorhandenen IT Anwendungen sind somit möglich. Es können Datentöpfe (Datenbanken,
                    Backups, Festplattenordner etc.) sowie Infrastrukturkomponten (Internet der Dinge, Fahrzeuge,
                    Scanner etc.) angebunden werden. Weiterhin können Cloud Dienste (Email, DateV,
                    Signaturlösungen etc.) integriert werden.


                </p>
            </div>
        </div>
        <div class="uk-width-large-4-4 uk-panel uk-panel-box">
            <div class="uk-panel-space uk-padding-top-remove">
                <div class="uk-panel uk-panel-box uk-padding-top-remove  uk-align-center">
                    <img src="/produkte/Grafiken/daisy_funktionen.png" alt="IDALABS DWH" id="img2" onclick="swipe(id)">
                </div>
            </div>
        </div>
    </div>
</section>

<!-- IDA Ökosystem-->
<section id="Haus" class="uk-container uk-container-center uk-margin-large-top uk-padding-remove ">
    <div class="uk-grid uk-grid-collapse sec has-shadow white-bg ">

        <div class="uk-width-large-3-4 uk-panel uk-panel-box">
            <div class="uk-panel-space">
                <h2 class="text-ida-red">IDA Ökosystem </h2>
                <p>
                    Die Daisy ist die Integrationsstelle unserer IDA Umgebung und bildet die Basis um verscheidene
                    Softwareprodukte in die IDA zu integrieren. Die einzelnen Produkte können so in übergreifende,
                    ganzheitliche Geschäftsprozesse integriert werden und die Anforderung der
                    Datenschutzgrundverordnung (DSGVO) werden erfüllt. Unterschiedliche Datentypen müssen jeweils nur
                    noch in
                    einer Anwendung gespeichert und gepflegt werden. Durch die Daisy, werden diese Daten dann
                    mit den anderen Anwendungen geteilt. Einige Beispielintegrationen sind Outlook, DateV, OSD, Klaes,
                    ELO, Schücal, Trello.

                </p>
            </div>
        </div>
        <div class="uk-width-large-1-4 uk-visible-large uk-panel uk-panel-box ">
            <div class="uk-panel-space">
                <img src="/assets/img/daisy.png" class="uk-align-right" width="170px" alt="IDALABS MaWi">
            </div>
        </div>


        <div class="uk-width-large-4-4 uk-panel uk-panel-box">
            <div class="uk-panel-space uk-padding-top-remove">

                <div class="uk-panel uk-panel-box uk-padding-top-remove uk-align-center">
                    <img src="/produkte/Grafiken/uebersicht_integrieren.png" alt="IDALABS Daisychain"
                         id="img1" onclick="swipe(id)">
                    </button>
                </div>
            </div>
        </div>
    </div>
</section>




<?php include("../_templates/kontakt.inc.php"); ?>
<?php include("../_templates/footer.inc.php");