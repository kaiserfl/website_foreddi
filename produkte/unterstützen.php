<?php
$pageTitle = "IDA Hypercare";
$metaKeywords = "Cloud Service, Lieferantenanbindung, OpenTrans, EDI";

include("../_templates/header.inc.php");
?>
    <section class="uk-container uk-container-center uk-margin-large-top uk-padding-vertical-remove">
        <h1 class="uk-heading-large uk-text-center"><?php echo $pageTitle ?></h1>
        </div>
    </section>


    <!-- Projekte Box -->
    <section class="uk-container uk-container-center uk-margin-large-top uk-padding-remove ">
        <div class="uk-grid uk-grid-collapse sec has-shadow white-bg ">
            <div class="uk-width-large-1-2 uk-position- uk-panel uk-panel-box">
                <div style="padding-bottom: 0px!important;"
                     class="uk-panel uk-panel-box  uk-flex uk-flex-middle uk-flex-center uk-padding-remove">
                    <div class="uk-panel-space">
                        <h1>Unterstützen </h1>
                        <p>Wir unterstützen dich bei der Einführung von IDA in unserer Hypercare Phase. Im direkten
                            Anschluss an das Projekt kümmern wir uns gemeinsam darum das, du IDA produktiv nutzen kannst
                            und alle Funktionen vollumfänglich deinen Wünschen entsprechen. Wie eine klassische
                            Einführung durchgeführt wird siehst du hier:
                        </p>

                        <a href="/produkte/projekte.php#ERP" class="uk-button uk-button-danger uk-margin-top  ">
                            Klassische Einführung </a>
                    </div>
                </div>

            </div>
            <div class="uk-width-large-1-2">
                <div class="uk-panel uk-panel-box uk-panel-box-primary uk-flex uk-flex-middle uk-flex-center uk-padding-remove">
                    <div class="uk-position-relative uk-visible-large">
                        <img src="/produkte/Grafiken/handshake.jpg" alt="IDALABS Porojekte">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- IDA Support und Begleitung-->
    <section class="uk-container uk-container-center uk-margin-large-top uk-padding-remove ">
        <div class="uk-grid uk-grid-collapse sec has-shadow white-bg ">

            <div class="uk-width-large-3-4 uk-panel uk-panel-box">
                <div class="uk-panel-space">
                    <h2 class="text-ida-red">IDA Support und Begleitung </h2>
                    <p>
                        Damit Du die Software dauerhaft optimal und wertschöpfend im Betrieb einsetzen kannst, bieten
                        wir natürlich einen exzellenten Service und Support, der Dir und Deinen Nutzern jederzeit
                        telefonisch, per Mail – oder gerne auch per Chat zur Verfügung steht.
                    </p>
                    <a href="../kontakt/support.php" class="uk-button uk-button-danger uk-margin-large-bottom  ">
                        Support </a>
                    <p>
                        Wir bieten dir einen exklusiven Ambassador Services. Der Ambassador ist Dein persönlicher
                        Ansprechpartner und Betreuer, der Dich während der gesamten Nutzungsdauer begleitet. Mehr Infos gibt es hier:
                    </p>
                    <a href="begleitung.php" class="uk-button uk-button-danger  ">
                        Begleitung</a>
                </div>
            </div>
            <div class="uk-width-large-1-4 uk-visible-large uk-panel uk-panel-box ">
                <div class="uk-panel-space">
                    <img src="/produkte/Grafiken/unterstützen.png" class="uk-align-right" width="170px"
                         alt="IDALABS MaWi">
                </div>
            </div>


        </div>
    </section>





<?php include("../_templates/kontakt.inc.php"); ?>
<?php include("../_templates/footer.inc.php");
