<?php
$pageTitle = "Impressum";
$metaKeywords = "Impressum";
include("../_templates/header.inc.php");
?>


<section class="uk-container uk-container-center uk-margin-large-top uk-padding-vertical-remove">
    <div class="uk-panel uk-space uk-text-center">
        <h1 class="uk-heading-large"><?php echo $pageTitle; ?></h1>
    </div>
</section>




<section class="uk-container uk-container-center white-bg uk-margin-large-top has-shadow">
    <h2 class="uk-text-center" id="impressum">IDALABS GmbH & Co. KG</h2>
    <div class="uk-panel uk-width-medium-4-5 uk-push-1-10">
        <div data-uk-grid-match="{target:'.uk-panel'}" class="uk-grid uk-grid-divider">
            <div class="uk-width-medium-1-2">

                <address class="uk-margin-large-top">
                    <dl class="uk-description-list-horizontal">
                        <dt>Geschäftsführer</dt>
                        <dd>Alexander Westen und Tobias Gürtler</dd>
                    </dl>
                    <dl class="uk-description-list-horizontal">
                        <dt>Anschrift</dt>
                        <dd>Johann-Heuck-Straße 19<br>24111 Kiel</dd>
                    </dl>
                    <dl class="uk-description-list-horizontal">
                        <dt>Büroanschrift</dt>
                        <dd>Holstenstraße 68a<br>24103 Kiel</dd>
                    </dl>
                    <hr>
                    <dl class="uk-description-list-horizontal">
                        <dt>Email</dt>
                        <dd><a href="mailto:info@idalabs.de">info@idalabs.de</a></dd>
                    </dl>
                    <dl class="uk-description-list-horizontal">
                        <dt>Telefon</dt>
                        <dd><a href="tel:+494318885558">+49 (0) 431 556 981 59</a></dd>
                    </dl>
                    <dl class="uk-description-list-horizontal">
                        <dt>Mobil</dt>
                        <dd><a href="tel:+491732406312">+49 (0) 173 240 63 12</a></dd>

                </address>
                <hr class="uk-margin-large-bottom uk-margin-large-top uk-visible-small">
            </div>
            <div class="uk-width-medium-1-2">

                <address class="uk-margin-large-top">

                    <dl class="uk-description-list-horizontal">
                        <dt>Handelsregister</dt>
                        <dd>Amtsgericht Kiel HRA 9700 KI</dd>
                    </dl>
                    <dl class="uk-description-list-horizontal">
                        <dt>Finanzamt</dt>
                        <dd>Kiel-Süd</dd>
                    </dl>
                    <dl class="uk-description-list-horizontal">
                        <dt>Steuernummer</dt>
                        <dd>20/284/67753</dd>
                    </dl>
                    <dl class="uk-description-list-horizontal">
                        <dt>USt-IdNr</dt>
                        <dd>DE312167127</dd>
                    </dl>
                    <hr>
                    <dl class="uk-description-list-horizontal">
                        <dt>Persönlich haftende Gesellschafter</dt>
                        <dd>WQnetwork GmbH<br/>Geschäftsführer Alexander Westen und Tobias Gürtler<br/>Johann-Heuck-Straße
                            19<br/>24111 Kiel<br/>Amtsgericht Kiel HRB 18735 KI
                        </dd>
                    </dl>
                </address>
                <hr class="uk-margin-large-bottom uk-margin-large-top uk-visible-small">
            </div>

        </div>
    </div>
</section>

<?php include("../_templates/footer.inc.php"); ?>

