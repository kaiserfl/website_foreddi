<?php
$pageTitle    = "Datenschutz";
$metaKeywords = "Datenschutz";
include("../_templates/header.inc.php");
?>


   <section class="uk-container uk-container-center uk-margin-large-top uk-padding-vertical-remove">
   <div class="uk-panel uk-space uk-text-center">
      <h1 class="uk-heading-large"><?php echo $pageTitle; ?></h1>
   </div>
   </section>

<section id="after_nl" class="uk-container uk-container-center white-bg uk-margin-large-top has-shadow">
   <div class="uk-panel uk-text-center">
      <h2 id="datenschutz">Datenschutz</h2>
      <h4>Datenschutz made in Germany</h4>
   </div>

   <div data-uk-grid-margin class="uk-grid uk-margin-large-top">
      <div class="uk-width-large-1-3">
         <div class="uk-grid">
            <div class="uk-width-4-5">
               <h6>Einsatz von Matomo</h6>
               <p>Auf dieser Website werden unter Einsatz der durch uns gehosteten Webanalysedienst-Software Matomo
                  (www.matomo.org) auf Basis unseres berechtigten Interesses an der statistischen
                  Analyse des Nutzerverhaltens zu Optimierungs- und Marketingzwecken gemäß Art. 6 Abs. 1 lit. f DSGVO
                  Daten gesammelt und gespeichert.</p>
            </div>
         </div>
      </div>

      <div class="uk-width-large-1-3">
         <div class="uk-grid">
            <div class="uk-width-4-5">

               <h6>Umgang mit den erfassten Daten</h6>
               <p>Aus diesen Daten können zum selben Zweck pseudonymisierte Nutzungsprofile erstellt und untersucht
                  werden. Hierzu können Cookies eingesetzt werden. Bei Cookies handelt es sich um kleine
                  Textdateien, die lokal im Zwischenspeicher Ihres Internet-Browsers gespeichert werden.
                  die mit der Matomo-Technologie erhobenen Daten werden auf unseren Servern verarbeitet.
                  die durch das Cookie erzeugten Informationen im pseudonymen Nutzerprofil werden nicht dazu benutzt,
                  den Besucher dieser Website persönlich zu identifizieren und nicht mit personenbezogenen
                  Daten über den Träger des Pseudonyms zusammengeführt.</p>
               <div class="uk-width-4-5">
               </div>
            </div>
         </div>
      </div>

      <div class="uk-width-large-1-3">
         <div class="uk-grid">
            <div class="uk-width-4-5">
               <h6>Widerspruchsmöglichkeiten</h6>
               <p>Wenn Sie mit der Speicherung und Auswertung dieser Daten aus Ihrem Besuch nicht einverstanden sind,
                  dann können Sie der Speicherung und Nutzung nachfolgend per Mausklick jederzeit widersprechen.
                  In diesem Fall wird in Ihrem Browser ein sog. Opt-Out-Cookie abgelegt, was zur Folge hat, dass Matomo
                  keinerlei Sitzungsdaten erhebt. Bitte beachten Sie, dass die vollständige Löschung Ihrer Cookies
                  zur Folge hat, dass auch das Opt-Out-Cookie gelöscht wird und ggf. von Ihnen erneut aktiviert werden
                  muss.</p>
               <?php if(!isset($_COOKIE['ida_opt_out'])) { ?>
                  <a href="../opt-out.php">Den Opt-Out-Cookie setzen</a>
               <?php } else { ?>
                  <div>
                     Es erfolgt keine Aufzeichnung mit Matomo.
                  </div>
               <?php } ?>
            </div>
         </div>
      </div>
   </div>
   <div class="uk-panel ">
      <h6>Die IDA-App</h6>
      <p>Die IDA App ist ein Element der IDA ERP Produktfamilie. Die App bildet die mobilen Anwendungsfälle in
         verarbeitenden Gewerben ab, wie zum Beispiel Montageaktivitäten.
         In welchem Umfang die App durch den Nutzer verwendet wird, unterliegt daher dem Direktionsrecht des
         ERP-Kunden (datenverarbeitende Stelle gem. DSGVO), der die IDALABS
         mit der technischen Verarbeitung der Daten beauftragt. Die Rechte und Pflichten des ERP-Kunden gegenüber
         den IDALABS sowie weitere Ausführungen zur Gewährleistung
         des Datenschutzes werden durch einen entsprechenden Auftragsverarbeitungsvertrag gem. DSGVO zwischen der
         datenverarbeitenden Stelle und dem Auftragsverarbeiter geregelt.
      </p>

      <p>Dort wird auch die Dauer der Speicherung der Daten vereinbart, die in der Regel der Dauer des
         Anstellungsverhältnisses bzw. der Wirksamkeit des Direktionsrechts entspricht und
         üblicherweise arbeitsvertraglich zwischen dem Arbeitgeber - als ERP-Nutzer und datenverarbeitender Stelle
         -
         und dem Mitarbeiter als Nutzer dieser App geregelt ist.</p>

      <p>Eine Nutzung der IDA App ist ohne das vorherige Erzeugen eines Nutzeraccounts im IDA ERP-System nicht
         möglich. Insofern ist eine Nutzung - und damit eine Verarbeitung von
         personenbezogenen Daten - außerhalb des Wirkungsbereiches des jeweiligen Auftragsverarbeitungsvertrages
         nicht möglich</p>

      <p>Die IDA App verarbeitet Daten, die von den Nutzern zu Mitarbeitern, Kunden, Lieferanten, Produkten und
         Fahrzeugen gepflegt oder abgerufen werden. Ziel ist die digitale
         Steuerung und Dokumentation der Durchführung von Kundenaufträgen in beiderseitigem Interesse. Die Eingabe
         und Hergabe von Daten erfolgt dabei in freiwilliger Form, jeder
         Nutzer entscheidet selbständig über Menge und Form der Daten. Der Nutzer kann Codenamen und -begriffe
         verwenden und seine eigene Identität pseudonymisieren.
         Eine automatisierte Entscheidungsfindung findet nicht statt.</p>

      <p>Die verarbeiten Daten liegen in Form von schriftlichen Aufzeichnungen, Fotos oder Audio-Daten vor.</p>

      <p>Die Authentifizierung an der App erfolgt über einen Benutzeraccount im ERP System, das per Token oder
         persönlichem Passwort, das durch den Arbeitgeber bzw. Auftraggeber
         administriert wird, ausgewiesen wird.</p>

      <p>Eine Kern-Funktion der IDA App besteht im Austausch von Informationen zwischen den Mitarbeitenden einer
         Firma. Daher sollte die App für die Nutzung auf folgende Services des Smartphones freigegeben werden, um
         die
         gesetzlichen
         und vertraglichen Dokumentations- verpflichtungen des ERP-Nutzers zu erfüllen:</p>

      <ul>
         <li>Name</li>
         <li>Emailadresse</li>
         <li>Persönliche Kennungen
         <li>Adresse</li>
         <li>Telefonnummer</li>
         <li>Suchverlauf der App</li>
         <li>Geräte-ID oder andere persönliche Kennungen</li>
         <li>Dateien und Dokumente</li>
         <li>Fotos und Videos</li>
         <li>In App Mitteilungen</li>
         <li>Sprach oder Tonaufnahmen</li>
         <li>Genauer und ungefährer Standort</li>
      </ul>

      <p>Eine Datenübermittlung in Staaten außerhalb der europäischen Union findet im Verantwortungsbereich des
         APP-Anbieters nicht statt.</p>

      <h6>Auskunftsrecht, Widerruf und Löschung der Daten</h6>

      <p>Begehren bezüglich Auskunft, Widerruf und Löschung von Daten sind an die datenverarbeitende Stelle zu
         richten, durch die IDALABS ggf. mit der technischen Umsetzung des
         Begehrens beauftragt wird.
      </p>
   </div>

</section>

<?php include("../_templates/footer.inc.php"); ?>

