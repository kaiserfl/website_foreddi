<?php
$pageTitle = "Vertrieb";
$metaKeywords = "Vertrieb";
include("../_templates/header.inc.php");
?>


<section class="uk-container uk-container-center uk-margin-large-top uk-padding-vertical-remove">
    <div class="uk-panel uk-space uk-text-center">
        <h1 class="uk-heading-large"><?php echo $pageTitle; ?></h1>
    </div>
</section>
<section class="uk-container uk-container-center white-bg uk-margin-large-top has-shadow">
    <div class="uk-panel uk-width-medium-4-5 uk-push-1-10">
        <div data-uk-grid-match="{target:'.uk-panel'}" class="uk-grid uk-grid-divider">
            <div class="uk-width-medium-1-2">

                <address class="">
                    <h2>Schreib uns</h2>
                    <p class="uk-margin-bottom">Wir freuen uns auf einem Mail von dir und schicken dir alle
                        Informationen über unsere Ida direkt zu</p>
                    <dl class="uk-description-list-horizontal">
                        <dt>Email</dt>
                        <dd><a href="mailto:vertrieb@idalabs.de">vertrieb@idalabs.de</a></dd>
                    </dl>
                    <hr>
                    <h2>Ruf uns an</h2>
                    <p class="uk-margin-bottom">Wir telefonieren gerne und freuen uns dir, unsere Ida bei einem Gespräch
                        ausführlich zu erklären</p>

                    <dl class="uk-description-list-horizontal">
                        <dt>Telefon</dt>
                        <dd><a href="tel:+494318885558">+49 (0) 431 556 981 59</a></dd>
                    </dl>
                    <dl class="uk-description-list-horizontal">
                        <dt>Mobil</dt>
                        <dd><a href="tel:+491732406312">+49 (0) 173 240 63 12</a></dd>
                    </dl>

                </address>
                <hr class="uk-margin-large-bottom uk-margin-large-top uk-visible-small">
            </div>
            <div id="kontakt" class="uk-width-medium-1-2">
                <h2>Kontaktformular</h2>
               <?php if ($_REQUEST['msg'] == "sent") { ?>
                   <p class="uk-margin-bottom">Vielen Dank für Deine Nachricht. Wir melden uns bei Dir.</p>
               <?php } else { ?>
                   <p class="uk-margin-bottom">Wenn du dich über unsere Produkte informieren willst, freuen wir uns über
                       eine Nachricht direkt hier über unser Kontaktformular</p>
                   <form action="../mail.php" method="post" class="uk-form">
                       <fieldset>
                           <div class="uk-form-row">
                               <input type="text" placeholder="Dein Name." name="name" required
                                      class="floatlabel uk-width-1-1">
                           </div>
                           <div class="uk-form-row">
                               <input type="email" placeholder="Deine Emailadresse." name="email" required
                                      class="floatlabel uk-width-1-1">
                           </div>
                           <div class="uk-form-row">
                        <textarea type="email" rows="5" placeholder="Deine Nachricht an uns." name="message" required
                                  class="floatlabel uk-width-1-1"></textarea>
                           </div>
                           <div class="uk-form-row">
                               <button type="submit" class="uk-button uk-button-danger">Senden</button>
                           </div>
                       </fieldset>
                   </form>
               <?php } ?>
            </div>
        </div>
    </div>
</section>


<?php include("../_templates/footer.inc.php"); ?>
