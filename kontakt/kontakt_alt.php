<?php
$pageTitle    = "Kontakt";
$metaKeywords = "ERP Testen";
include("../_templates/header.inc.php");
?>

<?php include("../_templates/jobs-list.inc.php"); ?>

<section class="uk-container uk-container-center uk-margin-large-top uk-padding-vertical-remove">
   <div class="uk-panel uk-space uk-text-center">
      <h1 class="uk-heading-large"><?php echo $pageTitle; ?></h1>
   </div>
</section>
<section class="uk-container uk-container-center white-bg uk-margin-large-top has-shadow">
   <div class="uk-panel uk-width-medium-4-5 uk-push-1-10">
      <div data-uk-grid-match="{target:'.uk-panel'}" class="uk-grid uk-grid-divider">
         <div class="uk-width-medium-1-2">
            <h2 id="impressum">IDALABS GmbH & Co. KG</h2>
            <address class="uk-margin-large-top">
               <dl class="uk-description-list-horizontal">
                  <dt>Geschäftsführer</dt>
                  <dd>Alexander Westen und Tobias Gürtler</dd>
               </dl>
               <dl class="uk-description-list-horizontal">
                  <dt>Anschrift</dt>
                  <dd>Johann-Heuck-Straße 19<br>24111 Kiel</dd>
               </dl>
               <dl class="uk-description-list-horizontal">
                  <dt>Büroanschrift</dt>
                  <dd>Holstenstraße 68a<br>24103 Kiel</dd>
               </dl>
               <hr>
               <dl class="uk-description-list-horizontal">
                  <dt>Email</dt>
                  <dd><a href="mailto:info@idalabs.de">info@idalabs.de</a></dd>
               </dl>
               <dl class="uk-description-list-horizontal">
                  <dt>Telefon</dt>
                  <dd><a href="tel:+494318885558">+49 (0) 431 556 981 59</a></dd>
               </dl>
               <dl class="uk-description-list-horizontal">
                  <dt>Mobil</dt>
                  <dd><a href="tel:+491732406312">+49 (0) 173 240 63 12</a></dd>
               </dl>
               <hr>
               <dl class="uk-description-list-horizontal">
                  <dt>Handelsregister</dt>
                  <dd>Amtsgericht Kiel HRA 9700 KI</dd>
               </dl>
               <dl class="uk-description-list-horizontal">
                  <dt>Finanzamt</dt>
                  <dd>Kiel-Süd</dd>
               </dl>
               <dl class="uk-description-list-horizontal">
                  <dt>Steuernummer</dt>
                  <dd>20/284/67753</dd>
               </dl>
               <dl class="uk-description-list-horizontal">
                  <dt>USt-IdNr</dt>
                  <dd>DE312167127</dd>
               </dl>
               <hr>
               <dl class="uk-description-list-horizontal">
                  <dt>Persönlich haftende Gesellschafterin</dt>
                  <dd>WQnetwork GmbH<br/>Geschäftsführer Alexander Westen und Tobias Gürtler<br/>Johann-Heuck-Straße
                     19<br/>24111 Kiel<br/>Amtsgericht Kiel HRB 18735 KI
                  </dd>
               </dl>
            </address>
            <hr class="uk-margin-large-bottom uk-margin-large-top uk-visible-small">
         </div>
         <div id="kontakt" class="uk-width-medium-1-2">
            <h2>Schreiben Sie uns.</h2>
            <?php if($_REQUEST['msg'] == "sent") { ?>
               <p class="uk-margin-bottom">Vielen Dank für Ihre Nachricht. Wir melden uns bei Ihnen.</p>
            <?php } else { ?>
               <p class="uk-margin-bottom">Egal, ob Sie sich über unsere Produkte informieren wollen oder Anregungen für
                  uns haben: Über unser Kontaktformular treten Sie unmittelbar mit uns in Kontakt.</p>
               <form action="../mail.php" method="post" class="uk-form">
                  <fieldset>
                     <div class="uk-form-row">
                        <input type="text" placeholder="Ihr Name." name="name" required class="floatlabel uk-width-1-1">
                     </div>
                     <div class="uk-form-row">
                        <input type="email" placeholder="Ihre Emailadresse." name="email" required
                               class="floatlabel uk-width-1-1">
                     </div>
                     <div class="uk-form-row">
                        <textarea type="email" rows="5" placeholder="Ihre Nachricht an uns." name="message" required
                                  class="floatlabel uk-width-1-1"></textarea>
                     </div>
                     <div class="uk-form-row">
                        <button type="submit" class="uk-button uk-button-danger">Senden.</button>
                     </div>
                  </fieldset>
               </form>
            <?php } ?>

            <h2>Jobs</h2>

            <p class="uk-margin-bottom">Wir haben immer viel zu tun! Willst Du uns unterstützen?</p>

            <dl class="uk-description-list-horizontal">
               <dt>Jobangebote (m/w/d)</dt>

               <?php foreach ($jobs as $job) {
                  if(!$job["deaktiviert"]) { ?>
                     <dd><a href="jobs/<?php echo $job["url"]; ?>"><?php echo $job["kurztitel"]; ?></a></dd>
                  <?php }
               } ?>
            </dl>
         </div>

      </div>
   </div>
</section>



<?php include("../_templates/footer.inc.php"); ?>
