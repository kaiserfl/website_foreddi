UIkit.on('domready.uk.dom', function() {

		$('html').append().attr('lang','de');
		
    outdatedBrowser({
        bgColor: '#B9132B',
        color: '#f1f1f1',
        lowerThan: 'transform',
        languagePath: 'bower_components/outdated-browser/outdatedbrowser/lang/de.html'
    });

    $('.floatlabel').floatlabel();
    autosize($('textarea'));

    var url = window.location.href;
    var origin = window.location.origin + '/'
    $(".uk-navbar-nav li a").each(function() {
        if ($(this).attr("href") == url || $(this).attr("href") == '')
            $(this).addClass("active");
    })


    var fields = $('input,textarea,select').filter('[required]:visible');
    fields.keyup(function() {
        var emptyFields = fields.filter(function() {

            return $.trim(this.value) === "";
        });
        if (!emptyFields.length) {
            $('button').removeAttr('disabled');
        }
        if (emptyFields.length != !fields.length) {
            $('button').attr('disabled', 'true');
        }
    });


    contactForm('#contact-form');
    contactForm('#demo-form');
    contactForm('#newsletter-form');

    function contactForm(theId) {
        var $contactForm = $(theId);
        $contactForm.submit(function(e) {
            e.preventDefault();
            $.ajax({
                url: '//formspree.io/info@idalabs.de',
                method: 'POST',
                data: $(this).serialize(),
                dataType: 'json',
                beforeSend: function() {
                    $contactForm.append('<div class="uk-animation-fade uk-alert uk-alert-warning uk-margin-large-top" data-uk-alert>Ihre Anfrage wird gesendet...</div>');
                },
                success: function(data) {
                    $contactForm.find('.uk-alert-warning').hide();
                    $contactForm.append('<div class="uk-animation-fade uk-alert uk-alert-success uk-margin-large-top">Ihre Anfrage wurde erfolgreich gesendet!</div>');
                    $contactForm[0].reset();
                    setTimeout(function() {
                        $contactForm.find('.uk-alert-success').hide();
                    }, 3500);
                },
                error: function(err) {
                    $contactForm.find('.uk-alert-warning').hide();
                    $contactForm.append('<div class="uk-animation-fade uk-alert uk-alert-danger uk-margin-large-top">Oops, da ist ein Fehler aufgetreten. Bitte versuchen Sie es nochmal.</div>');
                }
            });
        });
    };

});
