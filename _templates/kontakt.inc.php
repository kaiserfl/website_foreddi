<section id="kontakt" class="uk-container uk-container-center white-bg uk-margin-large-top has-shadow">
    <div class="uk-panel uk-text-center">
        <h2>Schreib uns</h2>
        <h4>Wir freuen uns über Deinen Kontakt</h4><a href="../kontakt/vertrieb.php" class="uk-button uk-button-danger uk-margin-large-top">Kontakt</a>
    </div>
</section>
