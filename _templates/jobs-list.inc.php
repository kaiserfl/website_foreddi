<?php

//$wirSindPurpose = "Wir begleiten verarbeitende Betriebe des Baugewerbes und deren Lieferanten in der digitalen Transformation. Dazu entwickeln wir eine durchgängige, modular aufgebaute ERP-Software zur Steuerung von Geschäftsprozessen und Lösungen zur Vernetzung der Wertschöpfungsketten sowie zur Integration von Daten und Applikationen";
$wirSindPurpose = "Wir entwickeln für unsere Kunden eine durchgängige, modular aufgebaute ERP-Software zur Steuerung von Geschäftsprozessen und Lösungen zur Vernetzung der Wertschöpfungsketten sowie zur Integration von Daten und Applikationen";
$wirSindTech    = "Unsere Produkte basieren auf einer modernen SaaS-Architektur. Zum Einsatz kommen Node.js, TypeScript und PHP in den Services, Vue.js und TypeScript im Frontend und Flutter/ Dart in der App";
$wirSindProcess = "Bei der agilen Entwicklung fahren wir auf einheitliches Design, Performance und mobile Nutzbarkeit ab. Wir organisieren uns mit OKR und Scrum, pflegen flache Hierarchien und setzen auf enge Zusammenarbeit";

$wirSind = [
   $wirSindPurpose,
   $wirSindTech,
   $wirSindProcess,
];

$wirBieten = [
   "Nagelneues, geräumiges Büro mitten in der Kieler City mit einem modernen Arbeitsplatz, den wir individuell auf deine Bedürfnisse zuschneiden",
	"Getränke und Snacks for free - wir kochen regelmäßig gemeinsam und treffen uns auch mal bei After Work Events",
	"Gutes Arbeitsklima mit einem wertschätzenden, aufgeschlossenen und konstruktiven Miteinander",
   "Flexible Arbeitszeiten im flexiblen Umfeld: neben dem Office arbeiten wir auch mal im Home-Office, gemeinsam im Garten, im Fördedampfer oder am Strand, machen regelmäßige Offsite-Events mit dem ganzen Team und für Weltenbummler bieten wir die Möglichkeit, für eine Weile komplett remote zu arbeiten",
	"Eine langfristige Perspektive, moderne Fortbildungskonzepte, die wir mit dem Team stetig weiter entwickeln und intensiven Wissensaustausch",
	"Unser familienfreundliches Unternehmen ist Lean organisiert, kennt keine geschlossenen Türen und stellt stets den Menschen - egal ob beim Kunden oder im eigenen Unternehmen - in den Mittelpunkt",
	"Gestaltungsspielraum, damit Du Deinen ganz individuellen Footprint tief in unser Unternehmen und unser Produkt drücken kannst",
	"Freie Auswahl deiner Hardware, auch zur privaten Nutzung"
];

$jobs = [
   "frontend-developer"  => [
      "deaktiviert" => false,
      "slogan"    => "Backend sucht Frontend",
      "titel"     => "Frontend Entwickler:in (m/w/d)",
      "kurztitel" => "Frontend Entwickler:in",
      "image"     => "../assets/img/jobs/frontend-engineer.jpeg",
      "url"       => "../jobs/frontend-developer.php",
      "einleitung" =>
         "
				Wir suchen eine:n Frontend Entwickler:in (m/w/d) zur Weiterentwicklung und Gestaltung unseres ERP-Systems. 
				Als Teil eines agilen Teams sorgst du für ein konsistentes Nutzererlebnis und unterstüzt damit die Digitalisierung unserer Kunden. 
				Du entwickelst neue Komponenten und unterstützt bei der Etablierung und Einhaltung von UI/ UX Patterns.
			",
      "wirsind"   => $wirSind,
      "suchen"    => [
         // Geforderte Technologien
         // Geforderte Qualifikationen
         // Geforderte Fähigkeiten & Techniken
         // Weiterführen
         // Nice 2 have
         "Du hast Bock unser Frontend zu gestalten, neue Ideen und komplexe Anforderungen umzusetzen und unsere Software kontinuierlich weiterzuentwickeln",
         "Dabei beherrschst Du gängige Frontend Technologien wie HTML5, SCSS/ CSS und TypeScript/ JavaScript",
         "Du verfügst über Expertise in der modernen Frontend-Entwicklung und damit verbundenen Frameworks und Technologien wie z.B Vue.js/ TypeScript",
         "Als kreativer Team Player sorgst du für ein durchgängiges und einheitliche Nutzererfahrung und unterstützt die Kollegen bei der Etablierung und Einhaltung von UI/ UX Pattern",
         "Nice to have: Erfahrung mit Node.js, GIT, Docker, UI / End-to-End Testing, UI/ UX Patterns",
         "Noch unerfahren? Alles kann, nichts muss: Überzeuge uns von deinem Wissensdurst und deiner Zielstrebigkeit, dann bringen wir Dir bei, was Dir aktuell an Kenntnissen und Fähigkeiten fehlt"
      ],
      "bieten"    => $wirBieten
   ],
   "fullstack-developer"  => [
      "deaktiviert" => false,
      "slogan"    => "One Dev to rule them all",
      "titel"     => "Fullstack Entwickler:in (m/w/d) App/Web",
      "kurztitel" => "Fullstack Entwickler:in",
      "image"     => "../assets/img/jobs/fullstack.jpg",
      "url"       => "../jobs/fullstack-developer.php",
      "einleitung" =>
         "Wir suchen eine:n Full Stack Softwareentwickler:in für die Weiterentwicklung unseres cloudbasierten ERP-Systems. Als Teil eines agilen Teams baust Du 
         durchgängige und komplexe Features und Workflows im Backend, dem Web-Frontend und der App und wirkst mit beim Optimieren unserer Entwicklungspraktiken.",
      "wirsind"   => $wirSind,
      "suchen"    => array(
         "Du hast Bock, ein ERP-System in allen Dimensionen zu beherrschen und komplexe Anforderungen ganzheitlich umzusetzen",
         "Dabei bist in Backend-Technologien wie TypeScript (node.js), PHP und MySQL erfahren und bewegst Dich sicher in der App- (Flutter/ Dart) oder Frontend-Entwicklung im Web (react,js, vue.js etc.) mit",
         "Du verfügst über Erfahrungen mit modernen Architekturprinzipien wie Microservices, Microfrontends, Design Patterns sowie agilen Methoden (Scrum, Kanban)",
         "Als Team Player mit einem Mindset für Software Engineering tauschst du dich mit deinen Kollegen im Coaching und durch Code Reviews aus",
         "Nice to have: Expertise mit Development Pipelines (z. B. Jenkins), Docker oder der Automatisierung von Tests und Infrastruktur",
      ),
      "bieten"    => $wirBieten
   ],
   "projektleiter"      	=> [
      "deaktiviert" => false,
      "slogan"    => "Trage die digitale Transformation ins Handwerk.",
      "titel"     => "Projektleiter:in (m/w/d)",
      "kurztitel" => "Projektleiter:in",
      "image"       => "../assets/img/jobs/projektleiter.jpeg",
      "url"       => "../jobs/projektleiter.php",
      "einleitung" =>
         "Wir suchen eine:n Projektleiter:in (m/w/d) zur digitalen Transformation von Handwerksbetrieben. Du übernimmst eigenverantwortlich Projekte zur Einführung unseres ERP-Systems und unterstützt die Betriebe bei der Digitalisierung Ihrer Geschäftsprozesse. 
			Gemeinsam mit unserem Team standardisierst Du die Beratungs- und Einführungsleistungen weiter und verbesserst sie.<br />" .
         "Verantwortlich für den Erfolg deiner Kunden spiegelst du zudem die Anforderungen und Bedürfnisse deiner Kunden in unserem Entwicklungsprozess wieder, um so den Erfolg und die Zufriedenheit unser Kunden zu maximieren.",
      "wirsind"   => $wirSind,
      "suchen"    => [
         "Du hast Bock, unsere Kunden bei der Einführung und Nutzung unserer Software zu unterstützen",
         "Du steuerst alle notwendigen Schritte, um das Kundenprojekt zum Erfolg zu bringen",
         "Du verstehst auch komplexe Prozesse und kannst sie Deinen Kunden plastisch erklären und sie dafür begeistern",
         "Dein technisches Verständnis ist ausgeprägt und Du hast Spaß daran, mit moderner Software zu arbeiten",
         "Du bist gerne beim Kunden vor Ort und kommunizierst mit verschiedensten Anwendern",
         "Idealerweise bringst du einen handwerklichen Hintergrund z.B. als Techniker, Meister oder technischer Betriebswirt mit - bevorzugt aus den Bereichen Zimmerei, Tischlerei, Metall- oder Fensterbau. 
          Oder du verfügst über baufachliche Expertise als Bau- oder Wirtschaftsingenieur. Oder du hast wenigstens BWL studiert ;-)",
      ],
      "bieten"    => $wirBieten,
   ],
   "service-support"      	=> [
      "deaktiviert" => true,
      "slogan"    => "Trage die digitale Transformation ins Handwerk.",
      "titel"     => "Mitarbeiter:in Service/Support & Vertriebsunterstützung (m/w/d)",
      "kurztitel" => "Service/Support & Vertriebsunterstützung",
      "image"       => "../assets/img/jobs/projektleiter.jpeg",
      "url"       => "../jobs/projektleiter.php",
      "einleitung" =>
         "<p>Wir suchen eine(n) Projektleiter:in (m/w/d) zur digitalen Transformation von klein und mittelständischen Handwerksbetrieben. Du übernimmst eigenverantwortlich Projekte zur Einführung unseres ERP-Sytems und unterstützt die Betriebe bei der digitalisierung Ihrer Geschäftsprozesse durch unser Beratung- und Schulungangebot. Zudem hilfst du unserem Team die Beratungs- und Einführungsleistungen weiter zu standardisieren und zu verbessern.</p>" .
         "<p>Verantwortlich für den Erfolg deiner Kunden spiegelst du zudem die Anforderungen und Bedürfnisse deiner Kunden in unserem Entwicklungsprozess wieder, um so den Erfolg und die Zufriedenheit unser Kunden zu maximieren.</p>",
      "wirsind"   => $wirSind,
      "suchen"    => [
         "Du hast Lust unsere Kunden bei der Einführung und Nutzung unserer Software zu unterstützen",
         "Du begleitest alle notwendigen Schritte um das Kundenprojekt zum Erfolg zu bringen",
         "Du verstehst auch komplexe Prozesse und kannst sie Deinen Kunden plastisch erklären und dafür begeistern",
         "Dein technisches Verständnis ist ausgeprägt und Du hast Spaß daran mit moderner Software zu arbeiten",
         "Du bist gerne beim Kunden vor Ort und kommunizierst mit verschiedensten Anwendern",
         "Idealerweise bringst du einen handwerklichen Hintergrund z.B. als Techniker, Meister oder technischer Betriebswirt, bevorzugt aus den Bereichen Zimmerei, Tischlerei oder Fensterbau mit. Oder du verfügst über baufachliche Expertise als Bau- oder Wirtschaftsingeneur. Oder du hast wenigstens BWL studiert ;-)",
      ],
      "bieten"    => $wirBieten,
   ],
   "dualer-student"     	=> [
      "deaktiviert" => false,
      "slogan"    => "Handwerk hat goldenen Boden - Informatik auch",
      "titel"     => "Dualer Student:in Wirtschaftsinformatik (m/w/d)",
      "kurztitel" => "Dualer Student:in",
      "image"       => "../assets/img/jobs/bachelor.jpeg",
      "url"       => "../jobs/dualer-student.php",
      "wirsind"   => $wirSind,
      "suchen"    => [
         "Du möchtest neben der Theorie auch praktisch im Studium etwas lernen",
         "Du packst gerne mit an, denkst logisch und analytisch und hast großes Interesse an verschiedenen Bereichen der IT",
         "Du hast Lust aktiv an unserem Produkt mitzuwirken, ständig dazuzulernen und neue Technologien auszuprobieren",
         "Du hast oder machst gerade Abitur/Fachhochschulreife und überzeugst mit guten Noten",
         "Idealerweise hast du schon erste Erfahrung mit Programmierung in PHP oder JavaScript",
      ],
      "bieten"    => $wirBieten,
   ],
   "software-developer"  	=> [
      "deaktiviert" => true,
      "slogan"    => "Software zu ende gedacht",
      "titel"     => "Softwareentwickler:in (m/w/d)",
      "kurztitel" => "Softwareentwickler:in",
      "image"     => "../assets/img/jobs/frontend-engineer.jpeg",
      "url"       => "../jobs/software-developer.php",
      "einleitung" =>
         "<p>Wir suchen einen Software Developer (m/w/d) mit dem wir ein konsistentes Design System für Web- und Mobile-Anwendungen unseres B2B-Systems etablieren. Als Teil eines agilen Teams sorgst du für ein konsistentes Nutzererlebnis und entwickelst unser Design System kontinuierlich weiter. Du pflegst bestehende Komponenten und schreckst nicht davor zurück, auch selber bei der technischen Umsetzung mitzuwirken.</p>" .
         "<p>Bei der Entwicklung unseres neuen B2C-Produktes, über das unsere Unternehmenskunden digital mit Ihren Endkunden kommunizieren, entwirfst du Design und Layout von Grund auf unter Berücksichtigung von Nutzerbedürfnissen und Machbarkeit in enger Abstimmung mit der Zielgruppe.</p>",
      "wirsind"   => $wirSind,
      "suchen"    => [
         // Geforderte Technologien
         // Geforderte Qualifikationen
         // Geforderte Fähigkeiten & Techniken
         // Weiterführen
         // Nice 2 have
         "Du beherrschst gängige Frontend Technologien wie HTML5, SCSS, JavaScript und TypeScript",
         "Du hast Erfahrung mit JavaScript/TypeScript und idealerweise Vue.js oder vergleichbare Frameworks",
         "Du hast Lust unser Frontend zu gestalten, neue Ideen und komplexe Anforderungen umzusetzen und unsere Software kontinuierlich weiterzuentwickeln",
         "Dich interessieren Themen wie Flutter und mobile, bestenfalls bringst du erste Erfahrungen mit",
         "Nice to have: Erfahrung mit Node.js, GIT, Docker, Flutter, Testautomatisierung"
      ],
      "bieten"    => $wirBieten
   ],
   "vue-developer"      	=> [
      "deaktiviert" => true,
      "slogan"    => "Backend sucht Frontend",
      "titel"     => "Vue.js Entwickler:in (m/w/d)",
      "kurztitel" => "Vue.js Entwickler:in",
      "image"     => "../assets/img/jobs/frontend-developer.jpeg",
      "url"       => "../jobs/frontend-developer.php",
      "wirsind"   => [
         "Wir arbeiten jeden Tag daran, unser Produkt noch ein Stück besser zu machen",
         "Unsere Software basiert auf einem Node.js/PHP REST Backend. Unser Frontend entwickeln wir mit Vue.js und unsere App mit Flutter",
         "Bei der agilen Entwicklung unserer Produkte setzen wir viel Wert auf einheitliche Gestaltung, Performance und mobile Nutzbarkeit",
      ],
      "suchen"    => [
         "Du beherrschst gängige Frontend Technologien wie HTML5, SCSS, JavaScript und TypeScript",
         "Du hast Erfahrung mit JavaScript Frameworks wie Angular, React oder idealerweise Vue.js",
         "Du hast Lust unser Frontend zu gestalten, neue Ideen und komplexe Anforderungen umzusetzen und unsere Software kontinuierlich weiterzuentwickeln",
         "Dich interessieren Themen wie Flutter, PWA und mobile, bestenfalls bringst du erste Erfahrungen mit",
         "Nice to have: Erfahrung mit Node.js, GIT, Docker, Flutter, Testautomatisierung"
      ],
      "bieten"    => [
         "Freie Auswahl deiner Hardware, auch zur privaten Nutzung",
         "Zentrales Büro in der Kieler Innenstadt",
         "Einen Arbeitsplatz der individuell auf deine Bedürfnisse abgestimmt ist",
         "Du kannst deine wöchentliche Arbeitszeit und Zahl der Urlaubstage frei wählen",
         "Home-Office Möglichkeit und regelmäßige Offsites",
         "Wir investieren in deine Weiterbildung"
      ]
   ],
   "web-designer"       	=> [
      "deaktiviert" => true,
      "slogan"    => "Gestalte die digitale Transformation des Handwerks.",
      "titel"     => "UI/IX Designer:in (m/w/d)",
      "kurztitel" => "UI/IX Designer:in",
      "image"     => "../assets/img/jobs/ux_designer.jpg",
      "url"       => "../jobs/ui-ux-designer.php",
      "einleitung" =>
         "<p>Wir suchen einen UX/UI Designer:in mit dem wir ein konsistentes Design System für Web- und Mobile-Anwendungen unseres B2B-Systems etablieren. Als Teil eines agilen Teams sorgst du für ein konsistentes Nutzererlebnis und entwickelst unser Design System kontinuierlich weiter. Du pflegst bestehende Komponenten und schreckst nicht davor zurück, auch selber bei der technischen Umsetzung mitzuwirken.</p>" .
         "<p>Bei der Entwicklung unseres neuen B2C-Produktes, über das unsere Unternehmenskunden digital mit Ihren Endkunden kommunizieren, entwirfst du Design und Layout von Grund auf unter Berücksichtigung von Nutzerbedürfnissen und Machbarkeit in enger Abstimmung mit der Zielgruppe.</p>",
      "wirsind"   => $wirSind,
      "suchen"    => [
         "Als UX/UI Designer:in motiviert es Dich, aus der effektivsten und funktional ausgereiftesten, auch die schönste und am besten bedienbare ERP-Software der Welt zu machen.",
         "Du hast ein gutes Gespür für funktionales und nutzerzentriertes Design. Auch in komplexen Softwareprodukten findest Du Dich gut zurecht und kannst die technische Umsetzbarkeit Deiner Ideen objektiv beurteilen.",
         "Design, Layout, eine großartige User Experience und das Etablieren von Design Systemen kennzeichnen Deinen bisherigen beruflichen Werdegang.",
         "Du hast umfangreiche Erfahrungen mit SCSS/CSS und erste Erfahrungen mit Vue.js und/oder Flutter.",
         "Dein Qualitätsanspruch an Dich selbst und andere stellt die einheitliche Umsetzung des Design-Systems in allen Bereichen des Produktes sicher."
      ],
      "bieten"    => $wirBieten,
   ],
   "jsdev"             	 	=> [
      "deaktiviert" => true,
      "slogan"    => "Data Transformation ",
      "titel"     => "Node.js Entwickler:in (m/w/d)",
      "kurztitel" => "Node.js Entwickler:in",
      "image"     => "../assets/img/jobs/devops.jpeg",
      "url"       => "../jobs/javascript-developer.php",
      "wirsind"   => $wirSind,
      "suchen"    => [
         "Du verfügst über Kenntnisse in den Bereichen Node.js (TypeScript, Express.js), APIs, Protokolle zum Datenaustausch und MySQL",
         "Du hast Lust unser IDA Data Warehouse weiterzuentwickeln, zu gestalten, neue Ideen umzusetzen und unsere Software kontinuierlich weiterzuentwickeln",
         "Du kennst dich mit komplexen Datenstrukturen und ETL-Prozessen aus und sorgst für die stabile Anbindung externer Systeme",
         "Du hast Bock aus großen Datenmengen Informationen zu extrahieren und hältst Data Mining nicht für eine Bergbaumethode",
         "Nice to have: Du hast Erfahrung mit CI/CD und Container Technologie"
      ],
      "bieten"    => $wirBieten,
   ],
   "devops"             	=> [
      "deaktiviert" => true,
      "slogan"    => "Automate everything",
      "titel"     => "DevOps Engineer:in (m/w/d)",
      "kurztitel" => "DevOps Engineer:in",
      "image"       => "../assets/img/jobs/devops.jpeg",
      "url"       => "../jobs/devops-engineer.php",
      "wirsind"   => $wirSind,
      "suchen"    => [
         "Du hast Erfahrung mit Infrastructure as Code (Terraform), Konfigurationsmanagement (Puppet, Ansible), CI/CD und Container Technologie",
         "Du verfügst über Kenntnisse in den Bereichen Webserver (Apache, NGINX), Log Management (ELK) und IT-Infrastruktur",
         "Neben DevOps hast du Lust aktiv an der Entwicklung unseres Produktes mitzuarbeiten",
         "Du hast Lust neue Ideen umzusetzen und unsere Deployment Prozesse kontinuierlich weiterzuentwickeln",
         "Du hast Erfahrung mit Node.js/PHP oder Lust dich ins Thema Softwareentwicklung einzuarbeiten",
      ],
      "bieten"    => $wirBieten,
   ],
   "digikffr"           	=> [
      "deaktiviert" => true,
      "slogan"      => "Digitalisierungsrockstar gesucht",
      "titel"       => "Ausbildung Kaufmann&Kauffrau für Digitalisierungsmanagement (m/w/d)",
      "kurztitel"   => "Ausbildung Digi Mgmt",
      "image"       => "../assets/img/jobs/digirockstar.png",
      "url"         => "../jobs/kffr_digimgmt.php",
      "wirsind"   => $wirSind,
      "suchen"      => [
         "Du wirst unsere Kunden bei der Digitalisierung ihrer Geschäftsprozesse unterstützen",
         "Du übernimmst vielfältige Aufgaben in Digitalisierungsprojekten",
         "Du unterstützt unser Vertriebsteam im Vertrieb unserer Software, erstellst Angebote, dokumentierst Absprachen und setzt Verträge auf",
         "Du wirkst im betrieblichen Controlling mit und analysierst Kennzahlen zur Steuerung des Unternehmens",
         "Wir bringen Dir das Programmieren bei. Wie tief Du hier einsteigst, bestimmst Du selbst",
         "Du bringst sicheres Auftreten, Serviceorientierung, Kommunikations- und Kontaktfreudigkeit mit"
      ],
      "bieten"    => $wirBieten,
   ],
   "werkstudent"        	=> [
      "deaktiviert" => true,
      "slogan"    => "Erlerne dein Handwerk - auch praktisch",
      "titel"     => "Werkstudent:in Informatik (m/w/d)",
      "kurztitel" => "Werkstudent:in",
      "image"       => "../assets/img/jobs/werkstudent.jpeg",
      "url"       => "../jobs/werkstudent.php",
      "wirsind"   => $wirSind,
      "suchen"    => [
         "Du studierst (Wirtschafts)-Informatik oder etwas vergleichbares",
         "Als Teil des Teams bringst du dich aktiv in die Weiterentwicklung unserer Software ein",
         "Du kennst aktuelle Frontend-Technologien wie HTML5, SCSS, JavaScript",
         "Du hast erste Erfahrung mit aktuellen JavaScript-Frameworks (Angular, React, oder Vue.js)",
         "Du hast Lust unser Frontend zu gestalten, neue Ideen umzusetzen und unsere Software kontinuierlich",
      ],
      "bieten"    => $wirBieten,
   ],
	"assistenz"					=> [
		"deaktiviert" 			=> false,
		"slogan"    			=> "KEEP CALM AND CALL YOUR ASSISTANT",
		"titel"     			=> "Team-Assistent:in",
		"kurztitel" 			=> "Team-Assisten:in",
		"image"       			=> "../assets/img/jobs/Assistenz.jpg",
		"url"       			=> "../jobs/assistenz.php",
		"einleitung" 			=>
										"Wir suchen eine:n Team-Assisent:in zur Verstärkung unseres Teams insbesondere in drei Bereichen:
											<ul>
												<li class=\"uk-text-large\"><strong>Managementassistenz</strong>: Du unterstützt unsere beiden Geschäftsführer z.B. beim Erstellen von Analysen, Reports und Statistiken, bei der Organisation von Events und bei repräsentativen Aufgaben</li>
												<li class=\"uk-text-large\"><strong>Projektassistenz</strong>: Du unterstützt unsere Projektleiter, erfolgreich &quot;in Time&quot; und &quot;in Budget&quot; zu arbeiten und führst das Projekt Management Office</li>
												<li class=\"uk-text-large\"><strong>Vertriebsassistenz</strong>: Du unterstützt unseren Vertrieb mit knackigen Präsentationen, zielgenau formulierten Projektskizzen und Angeboten - unsere Kunden wissen ihre Anfragen bei Dir in guten Händen</li>
											</ul>",
		"wirsind"   			=> $wirSind,
		"suchen"    			=> [
										"Du hast einschlägig studiert, eine kaufmännische Ausbildung oder jede Menge beeindruckender Praxiserfahrung",
										"Du hast ein feines Sprachgefühl und es macht Dir Freude, richtig eingängige und klare Texte zu schreiben",
										"Berührungsängste mit Spitzentechnologie sind Dir fremd, weil Du Dich für digitale Themen begeisterst",
										"Im Team arbeitest Du ebenso strukturiert wie flexibel und bist eine kundenorientierte Persönlichkeit mit starken Kommunikationsfähigkeiten",
		],
		"bieten"   				 => $wirBieten,
	]
];
