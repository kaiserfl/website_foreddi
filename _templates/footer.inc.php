<footer class="uk-panel uk-panel-box uk-panel-box-secondary uk-contrast uk-margin-large-top">
   <div class="uk-container uk-container-center">


      <div class="uk-text-center uk-margin-large-top">
         <h3 class="uk-text-muted">Wir <em class="uk-icon-heart text-ida-red" style="color:#aaaaaa;"></em> das
            Handwerk</h3>
      </div>

      <div class="uk-grid uk-contrast uk-margin-top">
         <div class="uk-width-medium-1-5 uk-margin-top">
            <div class="uk-panel uk-text-center ">
               <h5>Kontakt aufnehmen</h5>
               <a class="uk-text-muted" href="/kontakt/kontakt_alt.php" title="Kontakt zu Idalabs">Zum Kontaktformular</a>
            </div>
         </div>

          <div class="uk-width-medium-1-5 uk-margin-top">
              <div class="uk-panel uk-text-center ">
                  <h5>IDA-APP</h5>
                  <a class="uk-text-muted" href="https://play.google.com/store/apps/details?id=de.idalabs.idaApp" title="Zum Appstore">Zum App-Store</a>
              </div>
          </div>

         <div class="uk-width-medium-1-5 uk-margin-top">
            <div class="uk-panel uk-text-center ">
               <h5><i class="uk-icon-phone uk-margin-small-right"></i>Mo. - Fr. | 07 - 17 Uhr</h5>
               <a class="uk-text-muted"
                  href="tel:+4943155698159">
                  +49 (0) 431 - 556 981 59</a>
            </div>
         </div>

         <div class="uk-width-medium-1-5 uk-margin-top">
            <div class="uk-panel uk-text-center ">
               <h5><i class="uk-icon-envelope uk-margin-small-right"></i>Brauchst du Hilfe?</h5>
               <a href="mailto:support@idalabs.de"
                  class="uk-text-muted">
                  support@idalabs.de</a>
            </div>
         </div>

         <div class="uk-width-medium-1-5 uk-margin-top">
            <div class="uk-panel uk-text-center ">
               <h5><i class="uk-icon-thumbs-up uk-margin-small-right"></i>Folge uns</h5>
               <a href="https://www.facebook.com/InnovationHandwerk"
                  title="Idalabs auf Facebook"
                  target="_blank"
                  class="uk-text-muted uk-margin-small-right">
                  <i class="uk-icon-facebook-official uk-icon-medium"></i></a>
               <a href="https://www.youtube.com/channel/UC4B4mXIJn8RVIOiEhDQisGQ"
                  title="Idalabs auf YouTube"
                  target="_blank"
                  class="uk-text-muted uk-margin-small-right">
                  <i class="uk-icon-youtube-square uk-icon-medium"></i></a>
               <a href="https://www.xing.com/pages/idalabsgmbh-co-kg-softwareistunserhandwerk"
                  title="Idalabs auf XING"
                  target="_blank"
                  class="uk-text-muted uk-margin-small-right">
                  <i class="uk-icon-xing-square uk-icon-medium"></i></a>
               <a href="https://www.linkedin.com/company/idalabs/"
                  title="Idalabs auf LinkedIn"
                  target="_blank"
                  class="uk-text-muted">
                  <i class="uk-icon-linkedin-square uk-icon-medium"></i></a>
<!--               <a href="https://twitter.com/IDALABS_IT"-->
<!--                  title="Idalabs auf Twitter"-->
<!--                  target="_blank"-->
<!--                  class="uk-text-muted uk-margin-small-right">-->
<!--                  <i class="uk-icon-twitter uk-icon-medium"></i></a>-->
               <a href="https://www.instagram.com/idalabs/"
                  title="Idalabs auf Instagram"
                  target="_blank"
                  class="uk-text-muted uk-margin-small-right">
                  <i class="uk-icon-instagram uk-icon-medium"></i></a>
            </div>
         </div>
      </div>

      <div class="uk-text-right uk-margin-large-top uk-text-muted uk-text-small">
         <a class="uk-margin-right uk-text-muted" href="/kontakt/datenschutz.php">Datenschutz</a>
         <a class="uk-margin-right uk-text-muted" href="/kontakt/impressum.php">Impressum</a>
         <strong class="uk-text-muted">
            © <?php echo date("Y"); ?> - IDALABS GmbH & Co KG. | Alle Rechte vorbehalten</strong>
      </div>

   </div>
</footer>


<script src="/assets/js/min/app-min.js"></script>
<script src="/bower_components/uikit/js/components/accordion.min.js"></script>
<script src="/bower_components/uikit/js/core/toggle.min.js"></script>
<script src="/assets/js/extra.js"> </script>



</body>
</html>
