<?php
include("../_templates/jobs-list.inc.php");

if(empty($displayJob)) {
   $displayJob = "frontend";
}

$job          = $jobs[$displayJob];
$pageTitle    = $job['titel'];
$metaKeywords = "Stellenausschreibung, " . $job['titel'];

include("../_templates/header.inc.php");

?>
<section class="uk-container uk-container-center uk-margin-arge-top uk-padding-remove">
   <div class="uk-panel uk-text-center"><img src="<?php echo $job['image']; ?>" alt="<?php echo $job['slogan']; ?>">
      <h1 class="uk-heading-large uk-margin-large-top"><?php echo $job['titel']; ?></h1>
   </div>
</section>

<?php if($job["deaktiviert"]) { ?>
   <section class="uk-container uk-container-center uk-margin-arge-top uk-padding-remove">
      <div class="uk-panel uk-text-center">
         <h1 class="uk-heading-large uk-margin-large-top uk-text-danger">Die Stellenausschreibung ist nicht mehr
            aktiv.</h1>
      </div>
   </section>
<?php } ?>


<section class="uk-container uk-container-center white-bg uk-margin-large-top uk-padding-vertical-remove has-shadow">
   <div class="uk-panel uk-panel-space">
      <p class="uk-text-large"><strong>Software ist unser Handwerk:</strong> Wir unterstützen  verarbeitende Betriebe des
          Baugewerbes und deren Lieferanten bei der Digitalisierung. Wir wollen mit allen Beteiligten mehr erreichen:
          Produktivität, Umsatz und Freude bei der Arbeit.
          Die Nachfrage ist sensationell - deswegen brauchen wir deine Unterstützung.
      </p>
      <?php
      if(!empty($job["einleitung"])) {?>
			<p class="uk-text-large">
               <?php echo $job["einleitung"];?>
			</p>
      <?php }
      ?>
      <h3 class="text-ida-red"># Was wir tun</h3>
      <ul>
         <?php
         foreach ($job["wirsind"] as $text) {
            echo "<li class=\"uk-text-large\">" . $text . "</li>";
         }
         ?>
      </ul>
      <h3 class="text-ida-red"># Wen wir suchen</h3>
      <ul>
         <?php
         foreach ($job["suchen"] as $text) {
            echo "<li class=\"uk-text-large\">" . $text . "</li>";
         }
         ?>
      </ul>
      <h3 class="text-ida-red"># Was wir bieten</h3>
      <ul>
         <?php
         foreach ($job["bieten"] as $text) {
            echo "<li class=\"uk-text-large\">" . $text . "</li>";
         }
         ?>
      </ul>
       <br />
      <p class="uk-text-large"><strong>Wir haben dein Interesse geweckt?</strong> Dann werde Teil unseres Teams! Melde
         dich einfach und unkompliziert per Mail - gerne mit einem kurzen Lebenslauf - und wir vereinbaren kurzfristig
         einen Termin zum Kennenlernen.
      </p>
       <br />
   </div>
</section>

<?php if($job["deaktiviert"]) { ?>
   <section id="kontakt" class="uk-container uk-container-center white-bg uk-margin-large-top has-shadow">
      <div class="uk-panel uk-text-center">
         <h2>Die Stellenausschreibung ist leider deaktiviert.</h2>
         <h4>Wir freuen uns aber auch über eine Initiativ-Bewerbung oder du schaust dir die untenstehenden offenen
            Stellen an.</h4>
         <a href="mailto:kontakt@idalabs.de"
            class="uk-button uk-button-danger uk-margin-large-top">Kontakt.</a>
      </div>
   </section>
<?php } else { ?>
   <section id="kontakt" class="uk-container uk-container-center white-bg uk-margin-large-top has-shadow">
      <div class="uk-panel uk-text-center">
         <h2>Melde dich bei uns.</h2>
         <h4>Wir freuen uns über deine Bewerbung. Ein kurzer Lebenslauf genügt.</h4>
         <a href="mailto:kontakt@idalabs.de"
            class="uk-button uk-button-danger uk-margin-large-top">Kontakt.</a>
      </div>
   </section>
<?php } ?>
<?php include("section-jobs.inc.php"); ?>

<?php include("../_templates/footer.inc.php"); ?>
