<?php include($_SERVER['DOCUMENT_ROOT'] . "/_templates/jobs-list.inc.php");

if (empty($pageTitle)) {
   $pageTitle = "Software ist unser Handwerk";
}

if (empty($metaDescription)) {
   $metaDescription = "Wir unterstützen Handwerker und deren Lieferanten bei der Digitalisierung. Unsere Mission ist es, mit allen Beteiligten mehr zu erreichen: Produktivität, Umsatz und Freude bei der Arbeit.";
}

$defaultKeywords = "Handwerkersoftware, Smarthandwerk, Handwerk Digitalisierung, Software für Handwerker, ERP für kleine Unternehmen, ERP System Mittelstand, Software Handwerk, Software Fensterbau, Software Zimmermann";

if (empty($metaKeywords)) {
   $metaKeywords = $defaultKeywords . ", " . $defaultKeywords;
} else {
   $metaKeywords = $defaultKeywords;
}


?>

<!DOCTYPE html>
<html id="top" lang="de-DE">
<head>
    <meta charset="utf-8">
    <title>IDALABS - <?php echo $pageTitle; ?></title>
    <meta name="description"
          content="<?php echo $metaDescription; ?>">
    <meta name="keywords" content="<?php echo $metaKeywords; ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1" user-scalable="no">
    <meta name="robots" content="follow, index, max-snippet:-1, max-video-preview:-1, max-image-preview:large">
    <link rel="stylesheet" href="/assets/css/app.css">
    <link rel="stylesheet" href="/assets/css/custom.css?v<=1">
    <!-- <script src="https://unpkg.com/vue@next"></script>  -->
    <script src="/playground/vue.global.js"></script>


   <?php if (!isset($_COOKIE['ida_opt_out'])) { ?>
       <!-- Piwik -->
       <script type="text/javascript">
           var _paq = _paq || [];
           /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
           _paq.push(['trackPageView']);
           _paq.push(['enableLinkTracking']);
           (function () {
               var u = "//hama.idalabs.de/piwik/";
               _paq.push(['setTrackerUrl', u + 'piwik.php']);
               _paq.push(['setSiteId', '3']);
               var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
               g.type = 'text/javascript';
               g.async = true;
               g.defer = true;
               g.src = u + 'piwik.js';
               s.parentNode.insertBefore(g, s);
           })();
       </script>
       <noscript><p><img src="//hama.idalabs.de/piwik/piwik.php?idsite=3&rec=1" style="border:0;" alt=""/></p>
       </noscript>
       <!-- End Piwik Code -->
   <?php } ?>
</head>
<body>
<header class="uk-container uk-container-center uk-padding-remove has-shadow">
    <div style="height:50px; font-size:13px; letter-spacing: 1px;"
         class="uk-flex uk-flex-middle uk-flex-right uk-panel-box-secondary uk-hidden-small">
        <div class="uk-margin-right uk-text-muted">
            <!--<li class="uk-display-inline-block uk-margin-right"><i class="uk-icon-phone"></i><a class="uk-text-muted"> +49 (0) 431 888 555 8</a></li>-->
            <li class="uk-display-inline-block uk-margin-right"><i class="uk-icon-phone"></i><a class="uk-text-muted"
                                                                                                href="tel:+4943155698159">
                    +49 (0) 431 - 556 981 59</a></li>
            <li class="uk-display-inline-block"><i class="uk-icon-envelope"></i><a href="mailto:info@idalabs.de"
                                                                                   class="uk-text-muted">
                    info@idalabs.de</a></li>
        </div>
    </div>
    <nav data-uk-sticky class="uk-navbar">
        <div class="uk-navbar-brand"><a href="/"><img src="/assets/img/ida-logo.svg" data-uk-svg
                                                      class="logo uk-hidden-small" alt="IDALABS"><img
                        src="/assets/img/ida-logo-small.svg" data-uk-svg class="logo uk-visible-small"
                        alt="IDALABS"></a>
        </div>
        <div class="uk-navbar-flip">
            <ul class="uk-navbar-nav uk-visible-large">
                <li><a href="/">Start</a></li>
                <li data-uk-dropdown="{justify:'.uk-navbar', mode: 'hover'}" class="uk-parent"><a href="#none">Produkte
                        <i
                                class="uk-icon-caret-down"></i></a>
                    <div class="uk-dropdown uk-dropdown-navbar uk-dropdown-width-4">
                        <div class="uk-grid uk-dropdown-grid">
                            <div class="uk-panel uk-width-3-6 uk-panel-space">
                                <h4>Erfahre mehr über unser Angebot.</h4>
                                <p>Wir bilden mit unseren Produkten einen ganzheitlichen und durchgängigen
                                    Werkzeugkasten für
                                    die Digitalisierung von Handwerksbetrieben. Mit der Daisychain integrieren wir Ihre
                                    vorhandenen Softwaretools, mit der IDA ergänzen wir digitale Fähigkeitslücken, mit
                                    dem IDADWH
                                    binden wir externe Datenquellen an und in der IDA Service Cloud betreiben wir all
                                    das
                                    hochverfügbar und sicher!</p>
                            </div>
                            <div class="uk-panel uk-width-2-6">
                                <div class="uk-grid">
                                    <ul class="uk-nav uk-nav-dropdown">
                                        <li class="uk-nav-header">Lösungen für Handwerker</li>
                                        <li><a href="/produkte/daisychain.php">Integrieren: Daisy</a></li>
                                        <li><a href="/produkte/ida.php">Ergänzen: IDA</a></li>
                                        <li><a href="/produkte/idadwh.php">Anbinden: IDADWH</a></li>
                                        <li><a href="/produkte/idaservicecloud.php">Betreiben: IDA Service Cloud</a>
                                        <li><a href="/produkte/unterstützen.php">Unterstützen: IDA Hypercare</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="uk-panel uk-width-1-6">
                                <div class="uk-grid">
                                    <ul class="uk-nav uk-nav-dropdown">
                                        <li class="uk-nav-header">Infos</li>
                                        <li><a href="/produkte/projekte.php">Projekte</a>
                                        <li>
                                        <li><a href="/produkte/beratung.php">Beratung</a>
                                        <li>
                                        <li><a href="/produkte/preis.php">Preise</a>
                                        <li>
                                        <li><a href="/produkte/begleitung.php">Begleitung</a>
                                        <li>
                                    </ul>
                                </div>
                            </div>


                        </div>
                    </div>
                </li>
                <li data-uk-dropdown="{justify:'.uk-navbar', mode: 'hover'}" class="uk-parent"><a href="#none">Jobs <i
                                class="uk-icon-caret-down"></i></a>
                    <div class="uk-dropdown uk-dropdown-navbar uk-dropdown-width-4">
                        <div class="uk-grid uk-dropdown-grid">
                            <div class="uk-panel uk-width-1-2 uk-panel-space">
                                <h4>Jobs & Studium bei IDALABS.</h4>
                                <p><strong>Software ist unser Handwerk:</strong> Wir unterstützen Handwerker und
                                    Lieferanten bei
                                    der Digitalisierung. Wir wollen mit allen Beteiligten mehr erreichen: Produktivität,
                                    Umsatz
                                    und Freude bei der Arbeit. Die Nachfrage nach Features ist sensationell - deswegen
                                    brauchen
                                    wir deine Unterstützung.</p>
                            </div>
                            <div class="uk-panel uk-width-1-2">
                                <div class="uk-grid">
                                    <!--<div class="uk-width-1-2">-->
                                    <ul class="uk-nav uk-nav-dropdown">
                                        <li class="uk-nav-header">Aktuelle Angebote (m/w/d)</li>
                                       <?php foreach ($jobs as $haederJob) {
                                          if (!$haederJob["deaktiviert"]) { ?>
                                              <li>
                                                  <a href="/jobs/<?php echo $haederJob["url"]; ?>"><?php echo $haederJob["kurztitel"]; ?></a>
                                              </li>
                                          <?php }
                                       } ?>
                                    </ul>
                                    <!--</div>-->
                                    <!--
                                    <div class="uk-width-1-2">
                                      <ul class="uk-nav uk-nav-dropdown">
                                        <li class="uk-nav-header">Beratung</li>
                                        <li><a href="/produkte/digitalisierung.php">Digitalisierungsberatung</a></li>
                                        <li><a href="/produkte/sam.php">Software Asset Management</a></li>
                                        <li><a href="/produkte/itsm.php">IT Service Management</a></li>
                                        <li><a href="/produkte/pm.php">Produktmanagement</a></li>
                                      </ul>
                                    </div>
                                    -->
                                </div>
                            </div>
                        </div>
                    </div>
                </li>

                <li data-uk-dropdown="{justify:'.uk-navbar', mode: 'hover'}" class="uk-parent"><a href="#none">Kontakt
                        <i
                                class="uk-icon-caret-down"></i></a>
                    <div class="uk-dropdown uk-dropdown-navbar uk-dropdown-width-4">
                        <div class="uk-grid uk-dropdown-grid">
                            <div class="uk-panel uk-width-3-6 uk-panel-space">
                                <h4>Wir freuen uns auf Deine Kontaktanfrage</h4>
                                <p>Du kannst uns via Telefon und Email jederzeit erreichen. Hier findest du unsere
                                    Kontaktadressen
                                </p>
                            </div>
                            <div class="uk-panel uk-width-2-6">
                                <div class="uk-grid">
                                    <ul class="uk-nav uk-nav-dropdown">
                                        <li class="uk-nav-header">Kontaktadressen</li>
                                        <li><a href="../kontakt/vertrieb.php">Vertrieb</a></li>
                                        <li><a href="../kontakt/support.php">Support</a></li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>

                    <!--
                    <li data-uk-dropdown="{justify:'.uk-navbar', mode: 'hover'}" class="uk-parent"><a href="#none">Über <i class="uk-icon-caret-down"></i></a>
                      <div class="uk-dropdown uk-dropdown-navbar uk-dropdown-width-4">
                        <div class="uk-grid uk-dropdown-grid">
                          <div class="uk-panel uk-width-1-2 uk-panel-space">
                            <h4>Erfahren Sie mehr über IDALABS.</h4>
                            <p>Wer wir sind, wie wir uns organisieren und mit welchen Werkzeugen und Methoden wir arbeiten, haben wir auf diesen Seiten für Sie zusammengestellt.</p>
                          </div>
                          <div class="uk-panel uk-width-1-2">
                            <div class="uk-grid">
                              <div class="uk-width-1-2">
                                <ul class="uk-nav uk-nav-dropdown">
                                  <li class="uk-nav-header">Idalabs</li>
                                  <li><a href="/ueber/unser-team">Unser Team</a></li>
                                  <li><a href="/ueber/genossenschaft">Genossenschaft</a></li>
                                  <li><a href="/ueber/philosophie">Philosophie</a></li>
                                </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </li>
                    -->
                <li><a href="https://ida.idalabs.de">Login</a></li>
                <li class="uk-active"><a href="/demo.php">Jetzt testen!</a></li>
            </ul>
            <ul class="uk-navbar-nav uk-hidden-large">
                <li><a href="#offcanvas" data-uk-offcanvas style="width:60px;" class="uk-navbar-toggle"></a></li>
            </ul>
        </div>
    </nav>
    <div id="offcanvas" class="uk-offcanvas">
        <div class="uk-offcanvas-bar uk-offcanvas-bar-flip">
            <div class="uk-panel uk-panel-space"><a
                        class="uk-position-top-right uk-close uk-close-alt uk-offcanvas-close"></a></div>
            <div class="uk-panel uk-margin-top">
                <ul data-uk-nav data-uk-scrollspy-nav="{smoothscroll:true}"
                    class="uk-nav uk-nav-parent-icon uk-nav-offcanvas">
                    <li><a href="/">Start</a></li>
                    <li><a href="/demo.php">Jetzt testen!</a></li>

                    <li class="uk-nav-header">Lösungen für Handwerker</li>
                    <li><a href="/produkte/alt%20/daisychain.php">Integrieren: Daisy</a></li>
                    <li><a href="/produkte/ida">Ergänzen: IDA Software</a></li>
                    <li><a href="/produkte/idadwh.php">Anbinden: IDADWH</a></li>
                    <li><a href="/produkte/idaservicecloud.php">Betreiben: IDA Service Cloud</a></li>
                    <li><a href="/produkte/unterstützen.php">Unterstützen: IDA Hypercare</a></li>
                    <li><a href="/produkte/projekte.php">Projekte</a>
                    <li><a href="/produkte/beratung.php">Beratung</a>
                    <li><a href="/produkte/begleitung.php">Begleitung</a>
                    <li><a href="/produkte/preis.php">Preise</a></li>
                    <li class="uk-nav-header">IDALABS</li>

                    <li class="uk-parent">
                        <a href="#">Jobs</a>
                        <ul class="uk-nav-sub">
                           <?php foreach ($jobs as $navJob) {
                              if (!$navJob["deaktiviert"]) { ?>
                                  <li>
                                      <a href="/jobs/<?php echo $navJob["url"]; ?>"><?php echo $navJob["kurztitel"]; ?></a>
                                  </li>
                              <?php }
                           } ?>
                        </ul>
                    </li>
                    <li><a href="/kontakt/vertrieb.php">Vertrieb</a></li>
                    <li><a href="/kontakt/support.php">Support</a></li>

                    <!--
                    <li class="uk-nav-header">Beratung</li>
                       <li><a href="/produkte/digitalisierung.php">Digitalisierungsberatung</a></li>
                       <li><a href="/produkte/sam.php">Software Asset Management</a></li>
                       <li><a href="/produkte/itsm.php">IT Service Management</a></li>
                        <li><a href="/produkte/pm.php">Produktmanagement</a></li>
                     -->
                    <!--
                    <li class="uk-nav-header">Idalabs</li>
                    <li><a href="/ueber/unser-team">Unser Team</a></li>
                    <li><a href="/ueber/genossenschaft">Genossenschaft</a></li>
                    <li><a href="/ueber/philosophie">Philosophie</a></li>
                    -->

                </ul>
            </div>
        </div>
    </div>
</header>
