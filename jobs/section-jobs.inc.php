<?php include("../_templates/jobs-list.inc.php");

$activeJobs =[];

          foreach ($jobs as $job) {
            if(!$job["deaktiviert"]) {
               $activeJobs[] = $job;
            }
         } ?>

<section id="products" class="uk-container uk-container-center white-bg uk-margin-large-top has-shadow">
   <div class="uk-panel uk-text-center uk-text-left-small">
      <h2>Alle Jobangebote im Überblick.</h2>
      <h4>Nichts dabei? Bewirb Dich gerne initiativ!</h4>
   </div>
   <div class="uk-panel uk-margin-large-top uk-margin-left uk-margin-right">
      <ul data-uk-tab data-uk-switcher="{connect:'#tabbed-content', animation: 'uk-animation-fade'}"
          class="uk-tab uk-tab-grid uk-margin-top">
         <?php foreach ($activeJobs as $job) {
            if(!$job["deaktiviert"]) { ?>
               <li class="uk-width-1-<?php echo count($activeJobs) ?>"><a href="#"><?php echo $job["kurztitel"]; ?></a></li>
            <?php }
         } ?>
      </ul>
      <ul id="tabbed-content" class="uk-switcher uk-width-medium-4-5 uk-push-1-10 uk-margin-large-top">
         <?php foreach ($activeJobs as $job) {
            if(!$job["deaktiviert"]) { ?>
               <li>
                  <div class="uk-panel">
                     <div data-uk-grid-margin class="uk-grid uk-margin-top">
                        <div class="uk-width-large-1-2">
                           <h6><?php echo $job["titel"]; ?></h6>
                           <ul>
                              <?php foreach ($job["suchen"] as $text) { ?>
                                 <li><?php echo $text; ?></li>
                                 <?php
                              } ?>
                           </ul>
                        </div>
                        <div class="uk-width-large-1-2">
                           <h6>Wir bieten</h6>
                           <ul>
                              <?php foreach ($job["bieten"] as $text) { ?>
                                 <li><?php echo $text; ?></li>
                              <?php } ?>
                           </ul>
                        </div>
                     </div>
                     <div class="uk-margin-large-top">
                        <a href="<?php echo $job["url"] ?>" class="uk-button uk-button-small uk-button-primary">Mehr
                           erfahren.</a>
                     </div>

                  </div>
               </li>
            <?php }
         } ?>

      </ul>
   </div>
</section>
