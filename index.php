<?php include("_templates/header.inc.php"); ?>

<section id="teaser" class="uk-container uk-container-center uk-margin-large-top uk-padding-remove ">
    <div class="uk-grid uk-grid-collapse sec has-shadow white-bg ">
        <div class="uk-width-large-1-2 uk-position- uk-panel uk-panel-box">
            <div style="padding-bottom: 50px!important;"
                 class="uk-panel uk-panel-box  uk-flex uk-flex-middle uk-flex-center uk-padding-remove">
                <div class="uk-panel-space">
                    <h1 class="text-ida-red">
                        Mit
                        Digitalisierung die Produktivität steigern</h1>
                    <h3>
                        Bis zu 30% Prozesskosten einsparen!</h3>
                    <p>IDALABS sorgt für eine ganzheitliche Digitalisierung der Geschäftsprozesse in deinem
                        Betrieb. Durchgängig vom Lieferanten bis zum Kunden. </p>
                </div>
            </div>

            <div class="uk-panel uk-panel-space uk-position-bottom-left">
                <div class="uk-panel-box">
                    <a href="/kontakt/vertrieb.php" class="uk-button uk-button-danger uk-margin-top"><span>Kontakt
                     aufnehmen</span><span class="uk-visible-medium"> & durchstarten.</span></a>
                </div>
            </div>
        </div>
        <div class="uk-width-large-1-2">
            <div
                    class="uk-panel uk-panel-box uk-panel-box-primary uk-flex uk-flex-middle uk-flex-center uk-padding-remove">
                <div class="uk-position-relative">
                    <img src="/assets/img/teaser/handwerker.jpg" alt="IDALABS Handwerker">
                    <!--div class="uk-overlay uk-position-bottom-left uk-text-right">
                       <div class="uk-panel-space">
                          <div>
                             <a href="#funktionen" data-uk-smooth-scroll=""
                                class="uk-button uk-button-primary uk-margin-top">> Mehr erfahren</a></div>
                       </div>
                    </div-->
                </div>

            </div>
        </div>
    </div>
</section>

<section id="konzept" class="uk-container uk-container-center uk-margin-large-top uk-padding-vertical-remove">

    <div class="uk-panel uk-space">
        <div class="uk-grid uk-grid-collapse has-shadow">
            <div class="uk-width-large-1-2">
                <div style="height:720px;"
                     class="uk-panel uk-panel-box uk-panel-box-primary uk-flex uk-flex-middle uk-flex-center uk-padding-remove hero-panel">
                    <div class="uk-panel uk-panel-space">
                        <div class="uk-panel uk-panel-space">
                            <h1 class="uk-contrast">Geschäftsprozesse im Baugewerbe digital vernetzen.</h1>
                            <h4 class="uk-margin-top uk-contrast">Unser Ansatz zur Digitalisierung ist ganzheitlich und
                                perfekt auf das Bauhauptgewerbe und die Baunebengewerbe abgestimmt.
                                Wir entwickeln unsere Lösungen mit dem Handwerk, für das Handwerk.</h4><a
                                    href="#products" data-uk-smooth-scroll class="uk-button uk-margin-top">Mehr
                                erfahren.</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-width-large-1-2">
                <div class="uk-grid hero">
                    <div class="uk-width-1-1">
                        <div style="height:180px;"
                             class="uk-panel uk-panel-box uk-flex uk-flex-middle uk-padding-remove hero-panel-small">
                            <div class="uk-width-2-5 uk-text-center"><img src="/assets/img/daisy.png"
                                                                          width="120" alt="IDALABS">
                            </div>
                            <div class="uk-width-3-5 uk-container">
                                <h3>Integrieren</h3>
                                <p class="uk-text-small"><!--Mit der Daisychain integrieren wir-->Wir integrieren
                                    Software,
                                    Datentöpfe und Infrastrukturkomponenten von Handwerksbetrieben in durchgängige
                                    Prozesse, die
                                    sich medienbruchfrei steuern lassen.</p>
                                <!--<a href="produkte/daisychain.php" class="uk-button uk-button-small uk-button-primary">Mehr.</a>-->
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-1-1">
                        <div style="height:180px; background-color: #F8F8F8"
                             class="uk-panel uk-panel-box uk-flex uk-flex-middle uk-padding-remove hero-panel-small">
                            <div class="uk-width-2-5 uk-text-center"><img src="/assets/img/ida.png" width="120"
                                                                          alt="IDALABS"></div>
                            <div class="uk-width-3-5 uk-container">
                                <h3>Ergänzen</h3>
                                <p class="uk-text-small">Die IDA-Softwarefamilie ist modular aufgebaut und schließt die
                                    Fähigkeitslücken, die verhindern, dass sich deine Geschäftsprozesse durchgängig
                                    digitalisieren
                                    lassen.</p>
                                <!--<a href="produkte/ida.php" class="uk-button uk-button-small uk-button-primary">Mehr.</a>-->
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-1-1">
                        <div style="height:180px;"
                             class="uk-panel uk-panel-box uk-flex uk-flex-middle uk-padding-remove hero-panel-small">
                            <div class="uk-width-2-5 uk-text-center"><img src="/assets/img/idadwh.png"
                                                                          width="140" alt="IDALABS">
                            </div>
                            <div class="uk-width-3-5 uk-container">
                                <h3>Anbinden</h3>
                                <p class="uk-text-small"><!--Mit dem IDA Datawarehouse binden wir -->Wir binden externe
                                    Datenquellen etwa der Lieferanten an deine IT/EDV an und binden Geschäftspartner
                                    damit
                                    direkt und digital in deine Geschäftsprozesse ein</p>
                                <!--<a href="produkte/idadwh.php" class="uk-button uk-button-small uk-button-primary">Mehr.</a>-->
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-1-1">
                        <div style="height:180px; background-color: #F8F8F8"
                             class="uk-panel uk-panel-box uk-flex uk-flex-middle uk-padding-remove hero-panel-small">
                            <div class="uk-width-2-5 uk-text-center"><img src="assets/img/idaservicecloud.png"
                                                                          width="120" alt="IDALABS"></div>
                            <div class="uk-width-3-5 uk-container">
                                <h3>Betreiben</h3>
                                <p class="uk-text-small">In der IDA Servicecloud betreiben wir IDA, Daisy und das IDADWH
                                    für
                                    unsere Kunden nach strengen Sicherheitsvorgaben und in höchster Verfügbarkeit 24
                                    Stunden am
                                    Tag, 7 Tage die Woche.</p>
                                <!--<a href="produkte/idaservicecloud.php" class="uk-button uk-button-small uk-button-primary">Mehr.</a>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--section id="teaser" class="uk-container uk-container-center uk-margin-large-top uk-padding-vertical-remove">
   <div class="uk-space">
      <div class="uk-position-relative">
         <img src="/assets/img/teaser/handwerker.jpg" alt="IDALABS Handwerker">
         <div class="uk-overlay uk-position-bottom-right uk-text-right">
            <div class="uk-panel-space">
               <h4 class="uk-contrast uk-panel-box-secondary uk-margin-remove uk-panel-box uk-display-inline-block">
                  Gewinne
                  Arbeitskraft von 3 - 4 Mitarbeitern</h4>
               <h2 class="uk-panel-box-primary uk-contrast uk-display-inline-block uk-panel-box uk-panel-box-primary">
                  Mit
                  Digitalisierung die Produktivität steigern</h2>
               <div>
                  <a href="/kontakt.php#kontakt" class="uk-button uk-button-danger uk-margin-top">> Kontakt
                     aufnehmen & durchstarten</a>
                  <a href="#funktionen" data-uk-smooth-scroll=""
                     class="uk-button uk-button-primary uk-margin-top">Mehr erfahren</a></div>
            </div>
         </div>
      </div>
   </div>
</section-->

<!--section id="teaser" class="uk-container uk-container-center uk-margin-large-top uk-padding-vertical-remove">
   <div class="uk-space">
      <div class="uk-position-relative">
         <img src="/assets/img/teaser/handwerker.jpg" alt="IDALABS Handwerker">
         <div class="uk-overlay uk-position-bottom-right uk-text-right">
            <div class="uk-panel-space">
               <h4 class="uk-contrast uk-panel-box-secondary uk-margin-remove uk-panel-box uk-display-inline-block">
                  Gewinne
                  Arbeitskraft von 3 - 4 Mitarbeitern.</h4>
               <h2 class="uk-panel-box-primary uk-contrast uk-display-inline-block uk-panel-box uk-panel-box-primary">
                  Mit
                  Digitalisierung die Produktivität steigern</h2>
               <div>
                  <a href="#kontakt" data-uk-smooth-scroll="" class="uk-button uk-button-danger uk-margin-top">> Kontakt
                     aufnehmen & durchstarten.</a>
                  <a href="#funktionen" data-uk-smooth-scroll=""
                     class="uk-button uk-button-primary uk-margin-top">Mehr erfahren</a></div>
            </div>
         </div>
      </div>
   </div>
</section-->

<section id="funktionen"
         class="uk-container uk-container-center white-bg uk-margin-large-top has-shadow">
    <div class="uk-panel uk-panel-space uk-text-center uk-text-left-small uk-padding-top-remove">
        <h2>Software ist unser Handwerk.</h2>
        <h4>Wir bereiten den Weg zur Digitalisierung.</h4>
    </div>

    <div class="uk-grid uk-grid-collapse uk-margin-large-bottom">
        <div class="uk-width-large-2-4 uk-space">
            <div>
                <div class="uk-flex">
                    <div class="uk-text-center uk-margin-large-right">
                        <i class="uk-icon-cloud uk-icon-large uk-icon-justify "></i>
                    </div>
                    <div>
                        <h6>Uneingeschränkte Flexibilität.</h6>
                        <p>Du hast alle Informationen überall verfügbar! Egal ob im Büro oder auf der Baustelle.
                            Mit der IDA sind Informationen zu Aufträgen, Kontakten usw. immer mit dabei.</p>
                    </div>
                </div>

            </div>
            <div>
                <div class="uk-flex">
                    <div class="uk-text-center uk-margin-large-right">
                        <i class="uk-icon-cubes uk-icon-large uk-icon-justify "></i>
                    </div>
                    <div>
                        <h6>Umfassende Funktionsvielfalt</h6>
                        <p>Wir digitalisieren jeden einzelnen deiner Geschäftsprozesse: Auftragsverwaltung,
                            Warenwirtschaft,
                            Kalkulation, Personalmanagement uvm. warten auf dich.</p>
                    </div>
                </div>
            </div>
            <div>
                <div class="uk-flex">
                    <div class="uk-text-center uk-margin-large-right">
                        <i class="uk-icon-desktop uk-icon-large uk-icon-justify "></i>
                    </div>
                    <div>
                        <h6>Einfache Bedienbarkeit</h6>
                        <p>Verabschiede Dich von komplizierter Software! Wir bieten leicht verständliche und gut
                            bedienbare
                            Software. So macht Digitalisierung Spaß!</p>
                    </div>
                </div>
            </div>
            <div>
                <div class="uk-flex">
                    <div class="uk-margin-large-right">
                        <i class="uk-icon-graduation-cap uk-icon-large uk-icon-justify "></i>
                    </div>
                    <div>
                        <h6>Schulung und Beratung</h6>
                        <p>Wir denken ganzheitlich. Wir liefern nicht nur die Software, sondern sorgen mit Schulungen
                            und individuelle Beratung, dass die Software im gesamten Betrieb erfolgreich läuft.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="uk-width-large-2-4 uk-space">
            <img src="/assets/img/teaser/teaser.png" alt="IDALABS - Software ist unser Handwerk">
        </div>
    </div>

</section>

<section id="video" class="uk-container uk-container-center uk-margin-large-top uk-padding-vertical-remove">
    <div class="uk-panel uk-space ">
        <div class="uk-panel uk-panel-box ">

            <div class="uk-panel uk-panel-box ">
                <iframe style="width: 100%; height: 400px;" class="uk-align-center uk-responsive-width  " width="600" height="400"
                        src="https://www.youtube.com/embed/3delJEteFo0" frameborder="0" -->
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen uk-video="autoplay: false; automute: false">
                </iframe>
            </div>
            <div>
                <p>In diesem kurzen Video stellen wir dir das Konzept unserer Software vor und geben dir am Beispiel der
                    Materialwirtschaft einen Einblick, wie
                    das aussieht, sich anfühlt - und wie leistungsfähig IDA ist. Am besten Ton anmachen :-)</p>
            </div>
        </div>
    </div>
</section>


<section id="getit"
         class="uk-container uk-container-center uk-margin-large-top uk-padding-vertical-remove">
    <div class="uk-panel uk-space">
        <div class="uk-panel uk-panel-box">
            <div class="uk-panel uk-panel-space uk-text-center uk-text-left-small uk-margin-large-top">
                <h2>Schnelle Einführung, frühe Erfolge.</h2>
                <h4>Erfolgreiche Einführung in wenigen Schritten am Beispiel der Materialwirtschaft.</h4>
            </div>

            <div class="uk-panel uk-panel-space uk-padding-top-remove">
                <div class="uk-grid uk-text-center">
                    <div class="uk-width-1-2 uk-width-medium-1-3 uk-width-large-1-6 uk-margin-top">
                        <h6 class="text-ida-red uk-margin-remove">1. Schritt >></h6>
                        <h5 class="uk-margin-remove">Bestandsaufnahme</h5>
                    </div>
                    <div class="uk-width-1-2 uk-width-medium-1-3 uk-width-large-1-6 uk-margin-top">
                        <h6 class="text-ida-red uk-margin-remove">2. Schritt >></h6>
                        <h5 class="uk-margin-remove">Abstimmung</h5>
                    </div>
                    <div class="uk-width-1-2 uk-width-medium-1-3 uk-width-large-1-6 uk-margin-top">
                        <h6 class="text-ida-red uk-margin-remove">3. Schritt >></h6>
                        <h5 class="uk-margin-remove">Daten erfassen</h5>
                    </div>
                    <div class="uk-width-1-2 uk-width-medium-1-3 uk-width-large-1-6 uk-margin-top">
                        <h6 class="text-ida-red uk-margin-remove">4. Schritt >></h6>
                        <h5 class="uk-margin-remove">Schulung</h5>
                    </div>
                    <div class="uk-width-1-2 uk-width-medium-1-3 uk-width-large-1-6 uk-margin-top">
                        <h6 class="text-ida-red uk-margin-remove">5. Schritt >></h6>
                        <h5 class="uk-margin-remove">Einführung</h5>
                    </div>
                    <div class="uk-width-1-2 uk-width-medium-1-3 uk-width-large-1-6 uk-margin-top">
                        <h6 class="text-ida-red uk-margin-remove">Bis zu 30%</h6>
                        <h5 class="uk-margin-remove">Sparen</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="products" class="uk-container uk-container-center white-bg uk-margin-large-top has-shadow">
    <div class="uk-panel uk-text-center uk-text-left-small">
        <h2>Innovationswerkstatt für Handwerker.</h2>
        <h4>Mit IDALABS von allem mehr haben.</h4>
    </div>
    <div class="uk-panel uk-margin-large-top">
        <ul data-uk-tab data-uk-switcher="{connect:'#tabbed-content', animation: 'uk-animation-fade'}"
            class="uk-tab uk-tab-grid uk-width-medium-2-3 uk-push-1-6 uk-margin-large-top">
            <li class="uk-active uk-width-1-4"><a href="#">Mehr Zeit</a></li>
            <li class="uk-width-1-4"><a href="#">Mehr Gewinn</a></li>
            <li class="uk-width-1-4"><a href="#">Mehr Transparenz</a></li>
            <li class="uk-width-1-4"><a href="#">Mehr Spaß</a></li>
        </ul>
        <ul id="tabbed-content" class="uk-switcher uk-width-medium-4-5 uk-push-1-10 uk-margin-large-top">
            <li>
                <div class="uk-panel">
                    <div data-uk-grid-margin class="uk-grid uk-margin-top">
                        <div class="uk-width-large-1-3">
                            <h6>Alles sofort finden</h6>
                            <p>Kontakte, Termine, Bestellungen, Dokumente, uvm. alles in einer Software, durchsuchbar
                                und
                                schnell auffindbar.</p>
                        </div>
                        <div class="uk-width-large-1-3">
                            <h6>Keine Zeitverschwendung</h6>
                            <p>Importiere die Artikeldaten deiner Lieferanten, versende elektronische Bestellungen oder
                                tausche
                                AVA Daten digital aus.</p>
                        </div>
                        <div class="uk-width-large-1-3">
                            <h6>Alles nur einmal erfassen</h6>
                            <p>Spare dir die doppelte Erfassung von Daten. Mit der IDA und den umfangreichen
                                Schnittstellen
                                stehen dir in jedem Prozess alle relevanten und bereits erfassten Daten zur
                                Verfügung.</p>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="uk-panel">
                    <div data-uk-grid-margin class="uk-grid uk-margin-top">
                        <div class="uk-width-large-1-3">
                            <h6>Mehr Umsatz</h6>
                            <p>Wir bringen Struktur und Transparenz in die Vertriebs- und Vergabe-Prozesse und
                                ermöglichen es
                                dadurch schneller und passgenauer auf Ausschreibungen und Anfragen zu reagieren.</p>
                        </div>
                        <div class="uk-width-large-1-3">
                            <h6>Weniger Kosten</h6>
                            <p>Die Digitalisierung sorgt für weniger Verschwendung, verkürzte Prozesse, eine geringere
                                Fehlerquote und weniger Abstimmungsaufwände. Das spart am Ende bares Geld.
                        </div>
                        <div class="uk-width-large-1-3">
                            <h6>Bessere Steuerung</h6>
                            <p>Die IDA bringt nicht nur Transparenz in den Betrieb, sondern bietet eine Vielzahl an
                                Steuerungsmöglichkeiten. von der Auftrags- und Personaleinsatzplanung über das
                                Fuhrparkmanagement
                                bis hin zum Liquiditäts- und Investitionsmanagement.</p>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="uk-panel">
                    <div data-uk-grid-margin class="uk-grid uk-margin-top">
                        <div class="uk-width-large-1-3">
                            <h6>Bessere Entscheidungen</h6>
                            <p>Transparenz und Übersicht ermöglichen Dir und deinen Mitarbeitern schnelle und
                                faktenbasierte
                                Entscheidungen zu treffen.</p>
                        </div>
                        <div class="uk-width-large-1-3">
                            <h6>Mehr Planbarkeit</h6>
                            <p>Wer ist wann und wo eingesetzt? Wann haben wir die Kapazität für einen großen Auftrag?
                                Wie ist
                                die Auslastung in der Fertigung? All die Fragen gehören der Vergangenheit an.</p>
                        </div>
                        <div class="uk-width-large-1-3">
                            <h6>Schnellere Antworten</h6>
                            <p>Egal ob Du deinen Mitarbeitern oder deinen Kunden antwort geben möchtest: Mit der IDA
                                findest Du
                                sofort
                                die richtige Antwort.</p>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="uk-panel">
                    <div data-uk-grid-margin class="uk-grid uk-margin-top">
                        <div class="uk-width-large-1-3">
                            <h6>Geile Software</h6>
                            <p>Wir sind von den vielfältigen Einsatzmöglichkeiten unser Software überzeugt und
                                versprechen
                                nicht nur viel Nutzen, sondern auch viel Spaß damit!</p>
                        </div>
                        <div class="uk-width-large-1-3">
                            <h6>Moderne Erscheinung</h6>
                            <p>Wir legen nicht nur Wert auf Benutzerfreundlichkeit, sondern auch auf simples, aber
                                modernes
                                Design.</p>
                        </div>
                        <div class="uk-width-large-1-3 ">
                            <h6>Glückliche Mitarbeiter</h6>
                            <p>Weniger Medienbrüche, einfacher Zugang zu relevanten Informationen. Deine Mitarbeiter
                                werden es
                                danken.</p>


                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>

    <div class="uk-panel uk-panel-space uk-padding-top-remove uk-padding-bottom-remove">
        <div class="uk-panel uk-panel-space uk-padding-top-remove uk-padding-bottom-remove">
            <div class="uk-panel uk-panel-space uk-padding-bottom-remove">
                <div class="uk-grid ">
                    <div class="uk-width-large-1-3 uk-padding-remove">
                        <div class="uk-panel uk-panel-box uk-panel-space uk-flex uk-flex-center uk-padding-bottom-remove"
                        ">
                        <a href="/produkte/projekte.php"
                           class="uk-button uk-button-danger uk-margin-top   ">Projekte</a>
                    </div>
                </div>

                <div class="uk-width-large-1-3">
                </div>

                <div class="uk-width-large-1-3 uk-padding-remove">
                    <div class="uk-panel uk-panel-box uk-panel-space uk-flex uk-flex-center uk-padding-bottom-remove"
                    ">
                    <a href="/produkte/beratung.php" class="uk-button uk-button-danger uk-margin-top  ">Beratungsangebote</a>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>


</section>

<section class="uk-container uk-container-center uk-margin-large-top uk-padding-vertical-remove">
    <div class="uk-panel uk-space">
        <div style="height: 300px;"
             class="uk-panel uk-panel-box uk-panel-box-secondary uk-flex uk-flex-middle uk-flex-center has-shadow">
            <div class="uk-panel uk-text-center uk-width-medium-2-3">
                <h2 class="uk-contrast">Newsletter bestellen.</h2>
                <h4 class="uk-contrast">Bestellen Sie unseren Newsletter und erhalten Sie aktuelle Informationen rund um
                    IDALABS und neue Produkte.</h4>
                <form id="newsletter-form" action="//formspree.io/info@idalabs.de" method="post"
                      class="uk-form uk-margin-large-top">
                    <fieldset>
                        <input type="email" name="Newsletteranmeldung" placeholder="Emailadresse"
                               style="border-radius:40px;padding-left:25px;" required class="uk-width-4-5">
                        <input type="hidden" name="_subject" value="[idalabs] Newsletteranmeldung">
                        <button type="submit" style="min-height:45px;line-height:45px;margin-left:-60px;" disabled
                                class="uk-button uk-button-danger"><i class="uk-icon-send uk-icon-small"></i></button>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</section>

<section id="after_nl" class="uk-container uk-container-center white-bg uk-margin-large-top has-shadow">
    <div class="uk-panel uk-text-center">
        <h2>Unser Leistungsversprechen.</h2>
        <h4>Unsere Kernthesen für die optimale Digitalisierung von Handwerksbetrieben.</h4>
    </div>
    <div class="uk-panel uk-margin-large-top uk-width-medium-4-5 uk-push-1-10">
        <div data-uk-grid-margin class="uk-grid uk-margin-large-top">
            <div class="uk-width-large-1-2">
                <div class="uk-grid">
                    <div class="uk-width-1-5"><i class="uk-icon-star-o uk-icon-large uk-icon-justify "></i></div>
                    <div class="uk-width-4-5">
                        <h6>Durchgängige Digitalisierung.</h6>
                        <p>Das permanente manuelle Verknüpfen von Wissensinseln aus unterschiedlichen Softwareprodukten
                            ist so
                            aufwendig, dass sich die Einführung einer ganzheitlichen Software nach kurzer Zeit
                            auszahlt.</p>
                    </div>
                </div>
            </div>
            <div class="uk-width-large-1-2">
                <div class="uk-grid">
                    <div class="uk-width-1-5"><i class="uk-icon-cogs uk-icon-large uk-icon-justify "></i></div>
                    <div class="uk-width-4-5">
                        <h6>Denken in Wertschöpfungsketten.</h6>
                        <p>Wir betreiben den Betrieb nicht isoliert, sondern in seinem gesamten Umfeld.
                            Kunden, Lieferanten, Informationen, Dienstleister, Netzwerke, Kooperationen werden in
                            unseren
                            Lösungen berücksichtigt.</p>
                    </div>
                </div>
            </div>
        </div>
        <div data-uk-grid-margin class="uk-grid">
            <div class="uk-width-large-1-2">
                <div class="uk-grid">
                    <div class="uk-width-1-5"><i class="uk-icon-thumbs-o-up uk-icon-large uk-icon-justify "></i></div>
                    <div class="uk-width-4-5">
                        <h6>Handwerkliche gute Software.</h6>
                        <p>Gute Qualität und handwerklich gute Software zahlt sich aus. Wir sorgen für einen
                            reibungslosen und
                            störungsfreien Betrieb.</p>
                    </div>
                </div>
            </div>
            <div class="uk-width-large-1-2">
                <div class="uk-grid">
                    <div class="uk-width-1-5"><i class="uk-icon-heart-o uk-icon-large uk-icon-justify "></i></div>
                    <div class="uk-width-4-5">
                        <h6>Hürden der Digitalisierung überwinden.</h6>
                        <p>Nicht jeder ist von Anfang an von der Digitalisierung überzeugt und auch nicht jeder
                            Lieferant oder
                            Betrieb besitzt zu Beginn die erforderlichen Fähigkeiten dafür. Unser erfahrenes Team hilft
                            dabei, die
                            Hürden zu
                            überwinden und die Potenziale der Digitalisierung freizusetzen.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="cites" class="uk-container uk-container-center uk-margin-large-top uk-padding-vertical-remove">
    <div class="uk-panel uk-space">
        <div class="uk-grid uk-grid-collapse has-shadow">
            <div class="uk-width-large-1-2">
                <div style="height:400px;"
                     class="uk-panel uk-panel-box uk-panel-box-secondary uk-flex uk-flex-middle uk-flex-center uk-padding-remove">
                    <div class="uk-panel uk-panel-space">
                        <div class="uk-panel uk-panel-space">
                            <p class="uk-text-large uk-contrast">"Dank der IDA haben wir endlich unser Lager im Griff
                                und
                                vertrödeln hier keine wertvolle Zeit mehr. Unseren Lieferanten lassen wir nichts mehr
                                durchgehen, weil wir stets den Status von Bestellungen wissen und die Lieferungen in
                                kürzester
                                Zeit kontrollieren können."</p>
                            <div class="uk-margin-large-top">
                                <div class="uk-panel">
                                    <h6 class="uk-contrast uk-margin-bottom-remove">Ulf Fornefett </h6>
                                    <p class="uk-text-small uk-contrast uk-margin-top-remove">Geschäftsführer Tischlerei
                                        Kurt
                                        Fornefett GmbH</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="uk-width-large-1-2">
                <div style="height:400px;"
                     class="uk-panel uk-panel-box uk-panel-box-primary uk-flex uk-flex-middle uk-flex-center uk-padding-remove">
                    <div class="uk-panel uk-panel-space">
                        <div class="uk-panel uk-panel-space">
                            <p class="uk-text-large uk-contrast">"Als unser Partner hat IDALABS unsere EDV übernommen
                                und
                                kümmert sich darum. Wir müssen seitdem keine Mitarbeiter mehr abstellen, die alles mehr
                                schlecht
                                als recht am Laufen halten. Es läuft stabiler, es spart Geld und es macht einfach
                                Spaß."</p>
                            <div class="uk-margin-large-top">
                                <div class="uk-panel">
                                    <h6 class="uk-contrast uk-margin-bottom-remove">Steffen Fricke</h6>
                                    <p class="uk-text-small uk-contrast uk-margin-top-remove">Geschäftsführer Elektro
                                        Fricke
                                        GmbH</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="smoothscroll-bar uk-flex uk-flex-middle uk-visible-large">
    <ul data-uk-scrollspy-nav="{closest:'li', smoothscroll:true}" data-no-instant class="uk-dotnav uk-flex-column">
        <li><a href="#top"> </a></li>
        <li><a href="#konzept"></a></li>
        <li><a href="#funktionen"></a></li>
        <li><a href="#video"></a></li>
        <li><a href="#getit"></a></li>
        <li><a href="#products"></a></li>
        <li><a href="#after_nl"> </a></li>
        <li><a href="#cites"></a></li>
    </ul>
</div>
<?php include("_templates/kontakt.inc.php"); ?>
<?php include("_templates/footer.inc.php"); ?>
